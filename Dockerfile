FROM openjdk:17-alpine
EXPOSE 8888
ADD target/governess.jar governess.jar
ENTRYPOINT ["java","-jar","governess.jar"]
