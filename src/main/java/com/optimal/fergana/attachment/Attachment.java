package com.optimal.fergana.attachment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String path;  // papkani ichidan topish un

    //CLIENTGA KO'RINADIGAN NOM
    private String fileOriginalName;

    //PAPKADA KO'RINADIGAN NOMI UNIQUE BO'LADI
    private String name;  //izlaganda shu nom bilan izlaniladi
    private Long size;
    private String contentType;
}
