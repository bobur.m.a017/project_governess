package com.optimal.fergana.product;

import com.optimal.fergana.product.product_category.ProductCategory;
import com.optimal.fergana.product.product_price.ProductPrice;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategory;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String measurementType;
    private boolean delete = false;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @Column(precision = 19, scale = 6)
    private BigDecimal protein;
    @Column(precision = 19, scale = 6)
    private BigDecimal kcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal oil;
    @Column(precision = 19, scale = 6)
    private BigDecimal carbohydrates;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ProductCategory productCategory;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private SanpinCategory sanpinCategory;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private RegionalDepartment regionalDepartment;

    @OneToMany(mappedBy = "product")
    private List<ProductPrice> productPrice;

    public Product(String name, String measurementType, BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates, ProductCategory productCategory, SanpinCategory sanpinCategory, RegionalDepartment regionalDepartment, boolean delete) {
        this.name = name;
        this.measurementType = measurementType;
        this.protein = protein;
        this.kcal = kcal;
        this.oil = oil;
        this.carbohydrates = carbohydrates;
        this.productCategory = productCategory;
        this.sanpinCategory = sanpinCategory;
        this.regionalDepartment = regionalDepartment;
        this.delete = delete;
    }
}
