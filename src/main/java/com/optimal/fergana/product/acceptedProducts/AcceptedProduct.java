package com.optimal.fergana.product.acceptedProducts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.supplier.contract.product.ProductContract;
import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AcceptedProduct {

    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal pack;

    @Column(precision = 19, scale = 6)
    private BigDecimal price;

    private boolean delete = false;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    private Date date;

    private BigDecimal restByDate;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Users recipient;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Supplier supplier;

    private String status;
    @JsonIgnore
    @ManyToOne
    private Product product;
    @JsonIgnore
    @ManyToOne
    private Department department;

    public AcceptedProduct(BigDecimal weight, BigDecimal packWeight, BigDecimal pack, BigDecimal price, Date date, Users recipient, Supplier supplier, String status, Product product, Department department,BigDecimal restByDate) {
        this.weight = weight;
        this.packWeight = packWeight;
        this.pack = pack;
        this.price = price;
        this.date = date;
        this.recipient = recipient;
        this.supplier = supplier;
        this.status = status;
        this.product = product;
        this.department = department;
        this.restByDate = restByDate;
    }
}
