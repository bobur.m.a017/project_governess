package com.optimal.fergana.product.acceptedProducts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AcceptedProductDTO {

    private UUID id;


    @NotNull
    private BigDecimal receivedWeight;

    @NotNull
    private Integer productId;

    private BigDecimal price;
}
