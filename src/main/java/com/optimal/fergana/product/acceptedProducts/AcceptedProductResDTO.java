package com.optimal.fergana.product.acceptedProducts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AcceptedProductResDTO {

    private UUID id;
    private BigDecimal weight;
    private boolean delete = false;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String recipient;
    private Date date;
    private BigDecimal packWeight;
    private BigDecimal pack;
    private BigDecimal restByDate;
    private BigDecimal price;
    private String sender;
    private String productName;

    public AcceptedProductResDTO(UUID id, BigDecimal weight, boolean delete, Timestamp createDate, Timestamp updateDate, String recipient, Date date, BigDecimal packWeight, BigDecimal pack, BigDecimal price, String sender, String productName) {
        this.id = id;
        this.weight = weight;
        this.delete = delete;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.recipient = recipient;
        this.date = date;
        this.packWeight = packWeight;
        this.pack = pack;
        this.price = price;
        this.sender = sender;
        this.productName = productName;
    }
}
