package com.optimal.fergana.product.acceptedProducts;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.productPack.ProductPack;
import com.optimal.fergana.product.productPack.ProductPackRepo;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AcceptedProductService {

    private final AcceptedProductRepo acceptedProductRepo;
    private final CustomConverter converter;
    private final UserService userService;
    private final WarehouseRepo warehouseRepo;

    public void add(Supplier supplier, Product product, BigDecimal price, BigDecimal receivedWeight, BigDecimal receivedWeightPack,BigDecimal pack, Users users) {

        if ((receivedWeight.compareTo(BigDecimal.valueOf(0)) > 0) && users.getDepartment() != null) {

            Optional<Warehouse> optionalWarehouse = warehouseRepo.findByDepartment_IdAndProduct_IdAndDeleteIsFalse(users.getDepartment().getId(), product.getId());

            if (optionalWarehouse.isPresent()) {

                Warehouse warehouse = optionalWarehouse.get();
                BigDecimal restByDate = warehouse.getTotalWeight().add(receivedWeight);

                acceptedProductRepo.save(new AcceptedProduct(receivedWeight, receivedWeightPack, pack, price, new Date(System.currentTimeMillis()), users, supplier, Status.NEW.getName(), product, users.getDepartment(), restByDate));
            }
        }
    }

    public CustomPageable getAll(Integer page, Integer pageSize, Users user) {

        Page<AcceptedProduct> productPage;

        if (pageSize == null)
            pageSize = 20;
        if (page == null)
            page = 0;

        Pageable pageable = PageRequest.of(page, pageSize);

        if (user.getKindergarten() != null)
            productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_id(user.getKindergarten().getId(), pageable);
        else if (user.getDepartment() != null)
            productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_id(user.getDepartment().getId(), pageable);
        else
            productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_id(user.getRegionalDepartment().getId(), pageable);

        List<AcceptedProductResDTO> list = converter.acceptedProductToDTO(productPage.stream().toList());

        return new CustomPageable(productPage.getSize(), pageable.getPageNumber(), list, 0);
    }

    public CustomPageable getAllByDate(Integer productId, Integer supplierId, Integer departmentId, Long startDate, Long endDate, Integer page, Integer pageSize, HttpServletRequest request) {

        Users user = userService.parseToken(request);
        if (departmentId == null) {
            departmentId = user.getDepartment().getId();
        }

        if (pageSize == null) {
            pageSize = 20;
        }
        if (page == null) {
            page = 0;
        }


        Page<AcceptedProduct> productPage;
        Pageable pageable = PageRequest.of(page, pageSize);

        List<AcceptedProductResDTO> acceptedProductResDTOList = new ArrayList<>();


        if (productId == null) {


            if (startDate != null && endDate != null) {
                Date date1 = new Date(startDate);
                Date date2 = new Date(endDate);

                if (supplierId != null) {
                    productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndSupplier_IdAndDateBetween(departmentId, supplierId, date1, date2, pageable);
                } else {
                    productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndDateBetween(departmentId, date1, date2, pageable);
                }
            } else {
                productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_Id(departmentId, pageable);
            }
        } else {

            if (startDate != null && endDate != null) {
                Date date1 = new Date(startDate);
                Date date2 = new Date(endDate);

                if (supplierId != null) {
                    productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndSupplier_IdAndDateBetweenAndProduct_Id(departmentId, supplierId, date1, date2, productId, pageable);
                } else {
                    productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndDateBetweenAndProduct_Id(departmentId, date1, date2, productId, pageable);
                }
            } else {
                productPage = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndProduct_Id(departmentId, productId, pageable);
            }

        }


        List<AcceptedProduct> productList = productPage.stream().toList();


        for (AcceptedProduct acceptedProduct : productList) {
            AcceptedProductResDTO acceptedProductResDTO = new AcceptedProductResDTO(
                    acceptedProduct.getId(),
                    acceptedProduct.getWeight(),
                    acceptedProduct.isDelete(),
                    acceptedProduct.getCreateDate(),
                    acceptedProduct.getUpdateDate(),
                    acceptedProduct.getRecipient().getName(),
                    acceptedProduct.getDate(),
                    acceptedProduct.getPackWeight(),
                    acceptedProduct.getPack(),
                    acceptedProduct.getRestByDate(),
                    acceptedProduct.getPrice(),
                    acceptedProduct.getSupplier().getName(),
                    acceptedProduct.getProduct().getName()
            );
            acceptedProductResDTOList.add(acceptedProductResDTO);

        }

        CustomPageable customPageable = new CustomPageable(productPage.getSize(), pageable.getPageNumber(), acceptedProductResDTOList);
        customPageable.setAllPageSize(productPage.getTotalPages());
        return customPageable;
    }
}
