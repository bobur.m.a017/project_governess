package com.optimal.fergana.product.acceptedProducts;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface AcceptedProductRepo extends JpaRepository<AcceptedProduct, UUID> {
    Page<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_id(Integer id, Pageable pageable);
    Page<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndSupplier_IdAndDateBetween(Integer department_id, Integer supplier_id, Date date1, Date date2, Pageable pageable);
    List<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndSupplier_IdAndDateBetween(Integer department_id, Integer supplier_id, Date date1, Date date2);
    Page<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndSupplier_IdAndDateBetweenAndProduct_Id(Integer department_id, Integer supplier_id, Date date, Date date2, Integer product_id, Pageable pageable);
    List<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndSupplier_IdAndDateBetweenAndProduct_Id(Integer department_id, Integer supplier_id, Date date, Date date2, Integer product_id);
    Page<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndDateBetween(Integer department_id, Date date1, Date date2, Pageable pageable);
    List<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndDateBetween(Integer department_id, Date date1, Date date2);
    Page<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndDateBetweenAndProduct_Id(Integer department_id, Date date, Date date2, Integer product_id, Pageable pageable);
    List<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndDateBetweenAndProduct_Id(Integer department_id, Date date, Date date2, Integer product_id);
    Page<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_Id(Integer department_id, Pageable pageable);
    List<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_Id(Integer department_id);
    Page<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndProduct_Id(Integer department_id, Integer product_id, Pageable pageable);
    List<AcceptedProduct> findAllByDeleteIsFalseAndDepartment_IdAndProduct_Id(Integer department_id, Integer product_id);



}
