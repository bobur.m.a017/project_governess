package com.optimal.fergana.product;


public interface ProductParseDTO {


    default ProductResDTO parse(Product product) {

        return new ProductResDTO(
                product.getId(),
                product.getName(),
                product.getMeasurementType(),
                product.getProtein(),
                product.getKcal(),
                product.getOil(),
                product.getCarbohydrates(),
                product.getProductCategory().getName(),
                product.getProductCategory().getId(),
                product.getSanpinCategory().getName(),
                product.getSanpinCategory().getId(),
                product.getRegionalDepartment().getName(),
                product.getRegionalDepartment().getId()
        );

    }
}
