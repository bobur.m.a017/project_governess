package com.optimal.fergana.product.product_price;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
public class ProductPriceDTO {
    @NotNull
    private int productId;

    @NotNull
    private int districtId;

    @NotNull
    private double price;

    @NotNull
    private int year = 2022;
    @NotNull
    private int month = 1;
}
