package com.optimal.fergana.product.product_price;

import com.optimal.fergana.product.ProductResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductPriceResDTO {

    private Integer id;
    private Integer productId;
    private String name;
    private BigDecimal price;
    private int year;
    private int month;
    private boolean edit;

}
