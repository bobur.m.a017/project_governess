package com.optimal.fergana.product.product_price;

import com.optimal.fergana.address.district.District;
import com.optimal.fergana.address.district.DistrictRepo;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.exception.ProductNotFoundException;
import com.optimal.fergana.exception.ProductPriceNotFoundException;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductPriceService {

    private final ProductPriceRepo productPriceRepo;
    private final UserService userService;
    private final ProductRepo productRepo;
    private final CustomConverter converter;
    private final DistrictRepo districtRepo;


    public StateMessage add(HttpServletRequest request, ProductPriceDTO dto) throws ProductNotFoundException {
        Users user = userService.parseToken(request);

        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());

        Optional<District> optionalDistrict = districtRepo.findById(dto.getDistrictId());

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;


        if (optionalProduct.isPresent() && optionalDistrict.isPresent()) {

            District district = optionalDistrict.get();
            Product product = optionalProduct.get();

            Optional<ProductPrice> optionalProductPrice = productPriceRepo.findByDepartment_IdAndDistrict_IdAndYearAndMonthAndProduct_Id(user.getDepartment().getId(), dto.getDistrictId(), dto.getYear(), dto.getMonth(), product.getId());

            if (optionalProductPrice.isEmpty()) {
                productPriceRepo.save(new ProductPrice(
                        product,
                        BigDecimal.valueOf(dto.getPrice()),
                        user.getRegionalDepartment(),
                        user.getDepartment(),
                        district, dto.getYear(), dto.getMonth()
                ));
                message = Message.SUCCESS_UZ;
                checkEdit(product, user.getRegionalDepartment());
            } else {
                ProductPrice productPrice = optionalProductPrice.get();
                productPrice.setPrice(BigDecimal.valueOf(dto.getPrice()));

                productPriceRepo.save(productPrice);
            }
        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(ProductPriceDTO dto, Integer productPriceId) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<ProductPrice> optionalProductPrice = productPriceRepo.findById(productPriceId);

        if (optionalProductPrice.isPresent()) {
            ProductPrice productPrice = optionalProductPrice.get();

            productPrice.setPrice(BigDecimal.valueOf(dto.getPrice()));
            productPriceRepo.save(productPrice);
            message = Message.EDIT_UZ;
        }

        return new StateMessage().parse(message);
    }

    public CustomPageable getAllPriceByProduct(HttpServletRequest request, Integer productId, Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);

        Users user = userService.parseToken(request);
        Integer departmentId = user.getRegionalDepartment().getId();

        Page<ProductPrice> page = productPriceRepo.findAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalse(departmentId, productId, pageable);

        return new CustomPageable(page.getSize(), page.getNumber(), converter.productPriceToDTO(page.stream().toList()), page.getTotalElements());
    }

    public CustomPageable getAll(HttpServletRequest request, Integer pageNumber, Integer pageSize, String search, Integer districtId, Integer year, Integer month) {

        Users user = userService.parseToken(request);
        if (year == null || month == null) {
            year = LocalDate.now().getYear();
            month = LocalDate.now().getMonthValue();
        }

        List<ProductPriceResDTO> list = new ArrayList<>();

        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);

        Integer departmentId = user.getRegionalDepartment().getId();
        productRepo.findAllByDeleteIsFalseAndRegionalDepartment_IdOrderByName(user.getRegionalDepartment().getId(), pageable);

        Page<Product> productPage;
        if (search == null) {
            productPage = productRepo.findAllByDeleteIsFalseAndRegionalDepartment_IdOrderByName(departmentId, pageable);
        } else {
            productPage = productRepo.findAllByDeleteIsFalseAndNameContainsAndRegionalDepartment_Id(search, departmentId, pageable);
        }

        List<Product> content = productPage.getContent();

        for (Product product : content) {
            Optional<ProductPrice> optionalProductPrice = productPriceRepo.findByDepartment_IdAndDistrict_IdAndYearAndMonthAndProduct_Id(user.getDepartment().getId(), districtId, year, month, product.getId());
            if (optionalProductPrice.isEmpty()) {
                list.add(new ProductPriceResDTO(null, product.getId(), product.getName(), BigDecimal.ZERO, year, month, true));
            } else {
                list.add(converter.productPriceToDTO(optionalProductPrice.get()));
            }
        }
        return new CustomPageable(productPage.getSize(), productPage.getNumber(), list, productPage.getTotalElements());
    }

    public StateMessage delete(Integer id) {
        Optional<ProductPrice> optionalProductPrice = productPriceRepo.findById(id);
        if (optionalProductPrice.isPresent()) {
            ProductPrice productPrice = optionalProductPrice.get();
            productPrice.setDelete(true);
            productPriceRepo.save(productPrice);
            return new StateMessage().parse(Message.SUCCESS_UZ);
        } else {
            throw new ProductPriceNotFoundException("bunday narx mavjud emas");
        }
    }

    private void checkEdit(Product product, RegionalDepartment regionalDepartment) {
        List<ProductPrice> list = productPriceRepo.findAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalseAndEditIsTrueOrderByCreateDateDesc(regionalDepartment.getId(), product.getId());

        for (int i = 1; i < list.size(); i++) {
            ProductPrice productPrice = list.get(i);
            productPrice.setEdit(false);
        }

        productPriceRepo.saveAll(list);
    }
}