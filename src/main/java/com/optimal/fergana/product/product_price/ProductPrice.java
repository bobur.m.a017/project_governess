package com.optimal.fergana.product.product_price;


import com.optimal.fergana.address.district.District;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ProductPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private Product product;

    private BigDecimal price;

    @ManyToOne
    private RegionalDepartment regionalDepartment;

    @ManyToOne
    private Department department;

    @ManyToOne
    private District district;

    private int year = 2022;

    private int month = 1;

    private boolean delete = false;

    private boolean edit = true;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public ProductPrice(Product product, BigDecimal price, RegionalDepartment regionalDepartment, Department department, District district, int year, int month) {
        this.product = product;
        this.price = price;
        this.regionalDepartment = regionalDepartment;
        this.department = department;
        this.district = district;
        this.year = year;
        this.month = month;
    }
}
