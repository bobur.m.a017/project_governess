package com.optimal.fergana.product.product_price;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface ProductPriceRepo extends JpaRepository<ProductPrice, Integer> {

    Page<ProductPrice> findAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalse(Integer regionalDepartment_id, Integer product_id, Pageable pageable);

    List<ProductPrice> findAllByRegionalDepartment_IdAndProduct_IdAndDeleteIsFalseAndEditIsTrueOrderByCreateDateDesc(Integer regionalDepartment_id, Integer product_id);


    Optional<ProductPrice> findByDepartment_IdAndDistrict_IdAndYearAndMonthAndProduct_Id(Integer department_id, Integer district_id, int year, int month, Integer productId);
}
