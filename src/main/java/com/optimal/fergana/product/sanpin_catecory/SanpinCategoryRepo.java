package com.optimal.fergana.product.sanpin_catecory;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface SanpinCategoryRepo extends JpaRepository<SanpinCategory,Integer> {
    boolean existsByNameAndRegionalDepartment(String name, RegionalDepartment regionalDepartment);
    List<SanpinCategory> findAllByRegionalDepartmentName(String name);
}
