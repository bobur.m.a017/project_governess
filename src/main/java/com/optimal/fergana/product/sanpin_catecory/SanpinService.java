package com.optimal.fergana.product.sanpin_catecory;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductParseDTO;
import com.optimal.fergana.product.ProductResDTO;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNorm;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNormDTO;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNormRepo;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNormResDTO;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentRepo;
import com.optimal.fergana.users.Users;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class SanpinService implements SanpinCategoryParseDTO, ProductParseDTO,SanpinServiceInter {

    private final SanpinCategoryRepo sanpinCategoryRepo;
    private final AgeGroupRepo ageGroupRepo;
    private final DailyNormRepo dailyNormRepo;
    private final RegionalDepartmentRepo regionalDepartmentRepo;

    public SanpinService(SanpinCategoryRepo sanpinCategoryRepo, AgeGroupRepo ageGroupRepo, DailyNormRepo dailyNormRepo, RegionalDepartmentRepo regionalDepartmentRepo) {
        this.sanpinCategoryRepo = sanpinCategoryRepo;
        this.ageGroupRepo = ageGroupRepo;
        this.dailyNormRepo = dailyNormRepo;
        this.regionalDepartmentRepo = regionalDepartmentRepo;
    }

    public StateMessage add(SanpinCategoryDTO sanpinCategoryDTO, Users users) {
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        RegionalDepartment regionalDepartment = users.getRegionalDepartment();

        boolean res = sanpinCategoryRepo.existsByNameAndRegionalDepartment(sanpinCategoryDTO.getName(), regionalDepartment);

        if (!res) {
            SanpinCategory sanpinCategory = new SanpinCategory(sanpinCategoryDTO.getName(), regionalDepartment);
            List<DailyNorm> dailyNormList = addDailyNorm(sanpinCategoryDTO.getDailyNormDTOList(), sanpinCategory);
            sanpinCategory.setDailyNormList(dailyNormList);
            SanpinCategory save = sanpinCategoryRepo.save(sanpinCategory);
            List<SanpinCategory> sanpinCategoryList = regionalDepartment.getSanpinCategories();
            sanpinCategoryList.add(save);
            regionalDepartment.setSanpinCategories(sanpinCategoryList);
            regionalDepartmentRepo.save(regionalDepartment);

            message = Message.SUCCESS_UZ;
        }
        return new StateMessage().parse(message);
    }

    private List<DailyNorm> addDailyNorm(List<DailyNormDTO> dailyNormDTOList, SanpinCategory sanpinCategory) {

        List<DailyNorm> dailyNormList = new ArrayList<>();

        for (DailyNormDTO dailyNormDTO : dailyNormDTOList) {

            DailyNorm dailyNorm = new DailyNorm();
            Optional<AgeGroup> ageGroupOptional = ageGroupRepo.findById(dailyNormDTO.getAgeGroupId());

            if (ageGroupOptional.isPresent()) {

                dailyNorm.setAgeGroup(ageGroupOptional.get());
                dailyNorm.setWeight(BigDecimal.valueOf(dailyNormDTO.getWeight()));
                dailyNorm.setSanpinCategory(sanpinCategory);
                dailyNormList.add(dailyNorm);

            }
        }
        return dailyNormList;
    }

    public StateMessage edit(SanpinCategoryDTO sanpinCategoryDTO, Integer id) {
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        Optional<SanpinCategory> optionalSanpinCategory = sanpinCategoryRepo.findById(id);


        if (optionalSanpinCategory.isPresent()) {

            SanpinCategory sanpinCategory = optionalSanpinCategory.get();

            List<DailyNorm> deleteDailyNormList = new ArrayList<>(sanpinCategory.getDailyNormList());

            sanpinCategory.setName(sanpinCategoryDTO.getName());

            List<DailyNorm> dailyNormList = new ArrayList<>();

            for (DailyNormDTO dailyNormDTO : sanpinCategoryDTO.getDailyNormDTOList()) {

                DailyNorm dailyNorm = new DailyNorm();
                dailyNorm.setAgeGroup(ageGroupRepo.getById(dailyNormDTO.getAgeGroupId()));
                dailyNorm.setWeight(BigDecimal.valueOf(dailyNormDTO.getWeight()));
                dailyNorm.setSanpinCategory(sanpinCategory);
                dailyNormList.add(dailyNorm);
            }
            sanpinCategory.setDailyNormList(dailyNormList);

            sanpinCategoryRepo.save(sanpinCategory);

            dailyNormRepo.deleteAll(deleteDailyNormList);

            message = Message.EDIT_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage delete(Integer id) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<SanpinCategory> optionalSanpinCategory = sanpinCategoryRepo.findById(id);


        if (optionalSanpinCategory.isPresent()) {
            if (optionalSanpinCategory.get().getProductList().size() == 0) {
                sanpinCategoryRepo.delete(optionalSanpinCategory.get());
                message = Message.DELETE_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public ResponseEntity<?> getAll(Users users) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();
        List<SanpinCategoryResDTO> list = new ArrayList<>();
        List<SanpinCategory> sanpinCategoryList = sanpinCategoryRepo.findAllByRegionalDepartmentName(regionalDepartment.getName());


        for (SanpinCategory sanpinCategory : sanpinCategoryList) {
            SanpinCategoryResDTO sanpinCategoryResDTO = parser(sanpinCategory);

            List<ProductResDTO> productResDTOList = new ArrayList<>();
            for (Product product : sanpinCategory.getProductList()) {
                productResDTOList.add(parse(product));
            }
            sanpinCategoryResDTO.setProduct(productResDTOList);
            List<DailyNormResDTO> dailyNormResDTOList = new ArrayList<>();
            for (DailyNorm dailyNorm : sanpinCategory.getDailyNormList()) {
                DailyNormResDTO dailyNormResDTO = new DailyNormResDTO();
                dailyNormResDTO.setId(dailyNorm.getId());
                dailyNormResDTO.setWeight(dailyNorm.getWeight());
                dailyNormResDTO.setAgeGroupId(dailyNorm.getAgeGroup().getId());
                dailyNormResDTO.setAgeGroupName(dailyNorm.getAgeGroup().getName());
                dailyNormResDTOList.add(dailyNormResDTO);
            }
            sanpinCategoryResDTO.setDailyNormDTOList(dailyNormResDTOList);

            list.add(sanpinCategoryResDTO);

        }
        list.sort(Comparator.comparing(SanpinCategoryResDTO::getName));
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    public ResponseEntity<?> getOne(Integer id) {
        Optional<SanpinCategory> optionalSanpinCategory = sanpinCategoryRepo.findById(id);
        Message message;
        if (optionalSanpinCategory.isEmpty()) {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
            return ResponseEntity.status(message.getCode()).body(new StateMessage().parse(message));
        } else {
            SanpinCategory sanpinCategory = optionalSanpinCategory.get();
            SanpinCategoryResDTO sanpinCategoryResDTO = parser(sanpinCategory);
            List<ProductResDTO> productResDTOList = new ArrayList<>();
            for (Product product : sanpinCategory.getProductList()) {
                productResDTOList.add(parse(product));
            }
            sanpinCategoryResDTO.setProduct(productResDTOList);
            List<DailyNormResDTO> dailyNormResDTOList = new ArrayList<>();
            for (DailyNorm dailyNorm : sanpinCategory.getDailyNormList()) {
                DailyNormResDTO dailyNormResDTO = new DailyNormResDTO();
                dailyNormResDTO.setId(dailyNorm.getId());
                dailyNormResDTO.setWeight(dailyNorm.getWeight());
                dailyNormResDTO.setAgeGroupId(dailyNorm.getAgeGroup().getId());
                dailyNormResDTO.setAgeGroupName(dailyNorm.getAgeGroup().getName());
                dailyNormResDTOList.add(dailyNormResDTO);
            }
            sanpinCategoryResDTO.setDailyNormDTOList(dailyNormResDTOList);

            message = Message.SUCCESS_UZ;
            return ResponseEntity.status(message.getCode()).body(sanpinCategoryResDTO);
        }
    }
}
