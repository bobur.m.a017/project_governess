package com.optimal.fergana.product.sanpin_catecory.dailyNorm;


import com.sun.istack.NotNull;

import java.math.BigDecimal;

public class DailyNormDTO {

    @NotNull
    private Integer ageGroupId;
    @NotNull
    private Double weight;

    public Integer getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
