package com.optimal.fergana.product.sanpin_catecory.dailyNorm;


import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategory;
import com.optimal.fergana.stayTime.StayTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DailyNorm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private AgeGroup ageGroup;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;
    @Column(precision = 19, scale = 6)
    private BigDecimal protein;
    @Column(precision = 19, scale = 6)
    private BigDecimal kcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal oil;
    @Column(precision = 19, scale = 6)
    private BigDecimal carbohydrates;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private SanpinCategory sanpinCategory;

    @ManyToOne
    private StayTime stayTime;


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;
}
