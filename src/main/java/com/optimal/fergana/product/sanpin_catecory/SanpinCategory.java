package com.optimal.fergana.product.sanpin_catecory;

import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNorm;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SanpinCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "productCategory")
    private List<Product> productList;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private RegionalDepartment regionalDepartment;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    @OneToMany(mappedBy = "sanpinCategory", cascade = CascadeType.ALL)
    private List<DailyNorm> dailyNormList;


    public SanpinCategory(String name) {
        this.name = name;
    }

    public SanpinCategory(String name, RegionalDepartment regionalDepartment) {
        this.name = name;
        this.regionalDepartment = regionalDepartment;
    }
}
