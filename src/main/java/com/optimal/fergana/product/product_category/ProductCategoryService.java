package com.optimal.fergana.product.product_category;

import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.Users;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductCategoryService implements ProductCategoryServiceInter {
    private final ProductCategoryRepository productCategoryRepository;

    public ProductCategoryService(ProductCategoryRepository productCategoryRepository) {
        this.productCategoryRepository = productCategoryRepository;
    }


    public StateMessage add(ProductCategoryDTO productCategoryDTO, Users users) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();
        boolean res = productCategoryRepository.existsByNameAndRegionalDepartment(productCategoryDTO.getName(), regionalDepartment);
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        if (!res) {
            productCategoryRepository.save(new ProductCategory(productCategoryDTO.getName(), users.getRegionalDepartment()));
            message = Message.SUCCESS_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(ProductCategoryDTO productCategoryDTO, Integer id) {
        Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(id);

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        if (optionalProductCategory.isPresent()) {
            ProductCategory productCategory = optionalProductCategory.get();
            boolean res = productCategoryRepository.existsByNameAndRegionalDepartment(productCategoryDTO.getName(), productCategory.getRegionalDepartment());
            if (!res) {
                productCategory.setName(productCategoryDTO.getName());
                productCategoryRepository.save(productCategory);
                message = Message.EDIT_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public List<ProductCategoryDTO> getAll(Users users) {

        List<ProductCategoryDTO> list = new ArrayList<>();
        List<ProductCategory> productCategoryList = productCategoryRepository.findAllByRegionalDepartment(users.getRegionalDepartment());

        for (ProductCategory productCategory : productCategoryList) {
            list.add(new ProductCategoryDTO(productCategory.getId(), productCategory.getName()));
        }
        return list;
    }

    public StateMessage delete(Integer id) {
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(id);

        if (optionalProductCategory.isPresent()) {
            ProductCategory productCategory = optionalProductCategory.get();
            if (productCategory.getProduct().size() == 0) {
                productCategoryRepository.deleteById(id);
                message = Message.EDIT_UZ;
            }
        }
        return new StateMessage().parse(message);
    }
}
