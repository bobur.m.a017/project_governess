package com.optimal.fergana.product.product_category;

import com.optimal.fergana.product.Product;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "productCategory")
    private List<Product> product;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private RegionalDepartment regionalDepartment;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public ProductCategory(String name, RegionalDepartment regionalDepartment) {
        this.name = name;
        this.regionalDepartment = regionalDepartment;
    }
}
