package com.optimal.fergana.product;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductServiceInter {
    StateMessage add(ProductDTO productDTO, Users users);
    StateMessage edit(ProductDTO productDTO, Integer id);
    List<ProductResDTO> getAll(Users users);
    ResponseEntity<?> getOne(Integer id);
    ResponseEntity<?> delete(Integer id);
}
