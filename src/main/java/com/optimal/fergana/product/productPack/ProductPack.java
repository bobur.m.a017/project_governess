package com.optimal.fergana.product.productPack;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ProductPack {


    @Id
    private UUID id = UUID.randomUUID();


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private Product product;

    @ManyToOne
    private Department department;

    @Column(precision = 19,scale = 6)
    private BigDecimal pack;

    public ProductPack(Product product, Department department, BigDecimal pack) {
        this.product = product;
        this.department = department;
        this.pack = pack;
    }
}
