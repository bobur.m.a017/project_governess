package com.optimal.fergana.product.productPack;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductPackResDTO {

    private UUID id;
    private Timestamp createDate;
    private Timestamp updateDate;
    private Integer productId;
    private String productName;
    private BigDecimal pack;
}
