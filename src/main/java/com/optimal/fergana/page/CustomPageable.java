package com.optimal.fergana.page;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@NoArgsConstructor
@Setter
public class CustomPageable {
    private int getPageSize;
    private int getPageNumber;
    private List<?> list;
    private long allPageSize;

    public CustomPageable(int getPageSize, int getPageNumber, List<?> list, long allPageSize) {
        this.getPageSize = getPageSize;
        this.getPageNumber = getPageNumber;
        this.list = list;
        this.allPageSize = (long) Math.ceil((double) allPageSize/getPageSize);
    }
    public CustomPageable(int getPageSize, int getPageNumber, List<?> list) {
        this.getPageSize = getPageSize;
        this.getPageNumber = getPageNumber;
        this.list = list;
    }
}
