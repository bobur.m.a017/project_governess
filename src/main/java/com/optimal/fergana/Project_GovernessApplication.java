package com.optimal.fergana;


import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.reporter.excel.OrderEcxelReporter;
import com.optimal.fergana.reporter.excel.ReportPDF;
import com.optimal.fergana.reporter.pdf.OrderPDFReporter;
import com.optimal.fergana.reporter.pdf.YukXati;
import com.optimal.fergana.role.Role;
import com.optimal.fergana.role.RoleRepo;
import com.optimal.fergana.supplier.STIRParser.STIRParser;
import com.optimal.fergana.supplier.SupplierDTO;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import com.optimal.fergana.users.UsersSave;
import com.optimal.fergana.users.UsersSaveRepo;
import com.optimal.fergana.warehouse.delivery.DeliveryRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

@RequiredArgsConstructor
@SpringBootApplication
public class
Project_GovernessApplication implements CommandLineRunner {


    private final KindergartenRepo kindergartenRepo;
    private final UsersSaveRepo usersSaveRepo;
    private final RoleRepo roleRepo;
    private final PasswordEncoder encoder;
    private final STIRParser stirParser;
    private final UsersRepo usersRepo;
    private final DepartmentRepo departmentRepo;
    private final ReportPDF reportPDF;
    private final DeliveryRepo deliveryRepo;
    private final OrderPDFReporter orderPDFReporter;
    private final OrderEcxelReporter orderEcxelReporter;


    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+5:00"));
        SpringApplication.run(Project_GovernessApplication.class, args);
    }

    @Override
    public void run(String... args) throws IOException {

//        orderEcxelReporter.createExcel(79);

//        test();


//        LocalDate start = LocalDate.of(2023, 3, 16);
//        LocalDate end = LocalDate.of(2023, 3, 17);
//
////       List<Kindergarten> all = kindergartenRepo.findAll();
//        List<Kindergarten> all = List.of(kindergartenRepo.findById(1554).get());
//        reportPDF.getReportFile(start, end, all, 8, 163, start.getYear(), start.getMonthValue(), 19);
////
//
        System.out.println("TUGADI");
    }

    public void test() {

    }
}
