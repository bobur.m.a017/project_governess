package com.optimal.fergana.kindergarten;


import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class KindergartenDTO {

    @NotNull
    private Integer number;

    @NotNull
    private Integer departmentId;

    @NotNull
    private Integer districtId;

    @NotNull
    private String stir;
}
