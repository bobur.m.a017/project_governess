package com.optimal.fergana.kindergarten;


import com.optimal.fergana.address.district.District;
import com.optimal.fergana.address.district.DistrictRepo;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.kindergarten.dto.KindergartenByAddressDTO;
import com.optimal.fergana.kindergarten.dto.KindergartenBySentProductDTO;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.order.toSendProduct.ProductWeight;
import com.optimal.fergana.order.toSendProduct.ProductWeightRepo;
import com.optimal.fergana.order.toSendProduct.ProductWeightResDTO;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.supplier.STIRParser.STIRParser;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.supplier.SupplierDTO;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@Service
@RequiredArgsConstructor
public class KindergartenService implements KindergartenServiceInter {

    private final KindergartenRepo kindergartenRepo;
    private final DepartmentRepo departmentRepo;
    private final CustomConverter converter;
    private final UserService userService;
    private final ProductWeightRepo productWeightRepo;
    private final DistrictRepo districtRepo;
    private final STIRParser stirParser;




    public StateMessage add(KindergartenDTO dto, Users users) {

        Optional<Department> optionalDepartment = departmentRepo.findById(dto.getDepartmentId());
        Message message;
        RegionalDepartment regionalDepartment = users.getRegionalDepartment();

        Optional<District> optionalDistrict = districtRepo.findById(dto.getDistrictId());

        if (optionalDepartment.isPresent() && optionalDistrict.isPresent()) {
            Department department = optionalDepartment.get();
            District district = optionalDistrict.get();

            boolean res = kindergartenRepo.existsAllByDistrict_IdAndNumberAndDeleteIsFalse(dto.getDistrictId(), dto.getNumber());

            if (res && regionalDepartment.getId().equals(department.getRegionalDepartment().getId()) && dto.getStir().length() != 9) {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            } else {

                Kindergarten kindergarten = (new Kindergarten(
                        "-DMTT",
                        dto.getNumber(),
                        Status.ACTIVE.getName(),
                        department,
                        regionalDepartment,
                        district
                ));


                SupplierDTO dtoSup = stirParser.parserLiveSTIR(dto.getStir());

                kindergarten.setFullName(dtoSup.getName());
                kindergarten.setDate(dtoSup.getDate());
                kindergarten.setStatus(dtoSup.getStatus());
                kindergarten.setTHSHT(dtoSup.getTHSHT());
                kindergarten.setDirector(dtoSup.getDirector());
                kindergarten.setDBIBT(dtoSup.getDBIBT());
                kindergarten.setIFUT(dtoSup.getIFUT());
                kindergarten.setFond(dtoSup.getFond());
                kindergarten.setAddress(dtoSup.getAddress());
                kindergarten.setPhoneNumber(dtoSup.getPhoneNumber());


                List<Kindergarten> kindergartenList = department.getKindergartenList() != null ? department.getKindergartenList() : new ArrayList<>();
                kindergartenList.add(kindergarten);
                department.setKindergartenList(kindergartenList);


                Set<District> addressList = department.getAddressList();
                addressList.add(district);
                department.setAddressList(addressList);


                departmentRepo.save(department);
                message = Message.SUCCESS_UZ;
            }
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage().parse(message);
    }


    public StateMessage edit(KindergartenDTO dto, Users users, Integer id) {

        Optional<Department> optionalDepartment = departmentRepo.findById(dto.getDepartmentId());
        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(id);
        Optional<District> optionalDistrict = districtRepo.findById(dto.getDistrictId());

        Message message;

        if (optionalDepartment.isPresent() && optionalKindergarten.isPresent() && optionalDistrict.isPresent()) {
            Department department = optionalDepartment.get();
            Kindergarten kindergarten = optionalKindergarten.get();
            District district = optionalDistrict.get();

            boolean res = false;
            if (!(dto.getNumber().equals(kindergarten.getNumber()))) {
                res = kindergartenRepo.existsAllByDistrict_IdAndNumberAndDeleteIsFalse(dto.getDistrictId(), dto.getNumber());
            }

            if (res && users.getRegionalDepartment().getId().equals(department.getRegionalDepartment().getId())) {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            } else {

                if (!(dto.getNumber().equals(kindergarten.getNumber()))) {
                    kindergarten.setNumber(dto.getNumber());
                }
                if (!(kindergarten.getDepartment().getId().equals(department.getId()))) {
                    Department oldDepartment = kindergarten.getDepartment();
                    List<Kindergarten> list = oldDepartment.getKindergartenList();
                    list.remove(kindergarten);
                    oldDepartment.setKindergartenList(list);

                    kindergarten.setDepartment(department);
                    List<Kindergarten> kindergartenList = department.getKindergartenList() != null ? department.getKindergartenList() : new ArrayList<>();
                    kindergartenList.add(kindergarten);
                    department.setKindergartenList(kindergartenList);
                    departmentRepo.save(department);
                    departmentRepo.save(oldDepartment);
                }

                if (!(kindergarten.getDistrict().getId().equals(district.getId()))) {
                    kindergarten.setDistrict(district);
                }

                kindergartenRepo.save(kindergarten);
                message = Message.EDIT_UZ;
            }
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage().parse(message);
    }


    @Override
    public ResponseEntity<?> getOne(Integer id) {

        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(id);

        if (optionalKindergarten.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();
            KindergartenResDTO dto = converter.kindergartenToDTO(kindergarten);
            dto.setUsersList(converter.userToDTO(kindergarten.getUsersList()));
            return ResponseEntity.status(200).body(dto);
        }
        StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
        return ResponseEntity.status(message.getCode()).body(message);
    }


    public CustomPageable getAll(Users users, Integer departmentId, Integer number, Integer pageNumber, Integer pageSize) {

        Page<Kindergarten> page;
        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20, Sort.by("number").ascending());

        long count;


        if (departmentId != null && number != null) {
            page = kindergartenRepo.findAllByRegionalDepartment_IdAndDepartment_IdAndNumberAndDeleteIsFalse(users.getRegionalDepartment().getId(), departmentId, number, pageable);
            count = kindergartenRepo.countAllByRegionalDepartment_IdAndDepartment_IdAndNumberAndDeleteIsFalse(users.getRegionalDepartment().getId(), departmentId, number);
        } else if (departmentId != null) {
            page = (kindergartenRepo.findAllByRegionalDepartment_IdAndDepartment_IdAndDeleteIsFalse(users.getRegionalDepartment().getId(), departmentId, pageable));
            count = kindergartenRepo.countAllByRegionalDepartment_IdAndDepartment_IdAndDeleteIsFalse(users.getRegionalDepartment().getId(), departmentId);
        } else if (number != null) {
            page = (kindergartenRepo.findAllByRegionalDepartment_IdAndNumberAndDeleteIsFalse(users.getRegionalDepartment().getId(), number, pageable));
            count = kindergartenRepo.countAllByRegionalDepartment_IdAndNumberAndDeleteIsFalse(users.getRegionalDepartment().getId(), number);
        } else {
            page = (kindergartenRepo.findAllByRegionalDepartment_IdAndDeleteIsFalse(users.getRegionalDepartment().getId(), pageable));
            count = kindergartenRepo.count();
        }


        List<Kindergarten> list = new ArrayList<>(page.stream().toList());

        list.sort(Comparator.comparing(Kindergarten::getNumber));

        List<KindergartenResDTO> dtoList = converter.kindergartenToDTO(list);
        dtoList.sort(Comparator.comparing(KindergartenResDTO::getNumber));

        return new CustomPageable(page.getSize(), page.getNumber(), dtoList, count);
    }


    public List<KindergartenByAddressDTO> get(Users users) {

        List<Kindergarten> list = kindergartenRepo.findAll();
        List<KindergartenByAddressDTO> dtoList = new ArrayList<>();

        for (Kindergarten kindergarten : list) {
            dtoList.add(new KindergartenByAddressDTO(kindergarten.getId(), kindergarten.getNumber(), kindergarten.getName(), false, kindergarten.getDepartment().getId()));
        }
        return dtoList;
    }


    @Override
    public List<KindergartenResDTO> getByDepartmentId(Users users) {

        List<Kindergarten> kindergartenList = users.getDepartment().getKindergartenList();
        return converter.kindergartenToDTO(kindergartenList);
    }


    public List<KindergartenResDTO> getByDepartmentId(Integer id, Users users) {
        List<Kindergarten> list;
        if (users.getDepartment() != null) {

            list = kindergartenRepo.findAllByDistrict_IdAndDepartment_IdAndDeleteIsFalse(id, users.getDepartment().getId());

        } else {
            list = kindergartenRepo.findAllByDistrict_IdAndDeleteIsFalse(id);
        }


        list.sort(Comparator.comparing(Kindergarten::getNumber));
        return converter.kindergartenToDTO(list);
    }


    public StateMessage delete(Users users, Integer id) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();
        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(id);

        Message message;

        if (optionalKindergarten.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();
            if (kindergarten.getRegionalDepartment().getId().equals(regionalDepartment.getId())) {
                kindergarten.setDelete(true);
                kindergartenRepo.save(kindergarten);
                message = Message.DELETE_UZ;
            } else {
                message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
            }
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage().parse(message);
    }


    public List<KindergartenBySentProductDTO> getKindergartenByProduct(HttpServletRequest request) {
        Users user = userService.parseToken(request);
        Integer departmentId = user.getDepartment().getId();
        List<KindergartenBySentProductDTO> kindergartenBySentProductDTOList = new ArrayList<>();
        List<Kindergarten> kindergartenList = kindergartenRepo.findAllByDepartment_IdAndDeleteIsFalse(departmentId);
        for (Kindergarten kindergarten : kindergartenList) {
            List<ProductWeight> productWeightList = productWeightRepo.findAllByToSendProduct_Kindergarten_Id(kindergarten.getId());
            int sum = 0;
            for (ProductWeight productWeight : productWeightList) {
                if (productWeight.getPackWeight().compareTo(BigDecimal.ZERO) > 0) {
                    sum++;
                }
            }
            KindergartenBySentProductDTO kindergartenBySentProductDTO = new KindergartenBySentProductDTO(
                    kindergarten.getId(),
                    sum,
                    kindergarten.getNumber(),
                    kindergarten.getName()
            );
            kindergartenBySentProductDTOList.add(kindergartenBySentProductDTO);
        }

        return kindergartenBySentProductDTOList;
    }


    public List<ProductWeightResDTO> getProductByKindergarten(Integer kindergartenId) {
        List<ProductWeight> productWeightList = productWeightRepo.findAllByToSendProduct_Kindergarten_Id(kindergartenId);
        List<ProductWeightResDTO> productWeights = new ArrayList<>();
        for (ProductWeight productWeight : productWeightList) {
            if (productWeight.getWeight().compareTo(BigDecimal.ZERO) > 0) {

                ProductWeightResDTO productWeightResDTO = new ProductWeightResDTO(
                        productWeight.getId(),
                        productWeight.getWeight(),
                        productWeight.getPack(),
                        productWeight.getPackWeight(),
                        productWeight.getProduct().getId(),
                        productWeight.getProduct().getName(),
                        productWeight.getPackWeight()
                );
                productWeights.add(productWeightResDTO);
            }

        }
        return productWeights;
    }
}
