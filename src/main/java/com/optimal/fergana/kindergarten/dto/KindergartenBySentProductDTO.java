package com.optimal.fergana.kindergarten.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KindergartenBySentProductDTO {
    private int id;
    private int sentProductSum;
    private int number;
    private String name;
}
