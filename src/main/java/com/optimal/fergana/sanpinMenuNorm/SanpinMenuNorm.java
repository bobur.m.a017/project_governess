package com.optimal.fergana.sanpinMenuNorm;

import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategory;
import com.optimal.fergana.sanpinMenuNorm.ageGroupSanpinNorm.AgeGroupSanpinNorm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class SanpinMenuNorm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private SanpinCategory sanpinCategory;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sanpinMenuNorm")
    private List<AgeGroupSanpinNorm> ageGroupSanpinNormList;

    @Column(precision = 19, scale = 6)
    private BigDecimal doneProtein;
    @Column(precision = 19, scale = 6)
    private BigDecimal planProtein;

    @Column(precision = 19, scale = 6)
    private BigDecimal doneKcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal planKcal;

    @Column(precision = 19, scale = 6)
    private BigDecimal doneOil;
    @Column(precision = 19, scale = 6)
    private BigDecimal planOil;

    @Column(precision = 19, scale = 6)
    private BigDecimal doneCarbohydrates;
    @Column(precision = 19, scale = 6)
    private BigDecimal planCarbohydrates;


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private MultiMenu multiMenu;

    public SanpinMenuNorm(SanpinCategory sanpinCategory, BigDecimal doneProtein, BigDecimal planProtein, BigDecimal doneKcal, BigDecimal planKcal, BigDecimal doneOil, BigDecimal planOil, BigDecimal doneCarbohydrates, BigDecimal planCarbohydrates, MultiMenu multiMenu) {
        this.sanpinCategory = sanpinCategory;
        this.doneProtein = doneProtein;
        this.planProtein = planProtein;
        this.doneKcal = doneKcal;
        this.planKcal = planKcal;
        this.doneOil = doneOil;
        this.planOil = planOil;
        this.doneCarbohydrates = doneCarbohydrates;
        this.planCarbohydrates = planCarbohydrates;
        this.multiMenu = multiMenu;
    }
}
