package com.optimal.fergana.sanpinMenuNorm;

import com.optimal.fergana.multiMenu.MultiMenu;

import java.util.List;

public interface SanpinMenuNormInter {
    List<SanpinMenuNorm> add(MultiMenu multiMenu, Integer day);
    List<SanpinMenuNormResDTO> get(MultiMenu multiMenu);
}
