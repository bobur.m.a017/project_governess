package com.optimal.fergana.sanpinMenuNorm;

import java.util.ArrayList;
import java.util.List;

public interface SanpinMenuNormInterface {

    default SanpinMenuNormResDTO parser(SanpinMenuNorm sanpinMenuNorm) {
        return new SanpinMenuNormResDTO(
                sanpinMenuNorm.getId(),
                sanpinMenuNorm.getSanpinCategory().getName(),
                sanpinMenuNorm.getSanpinCategory().getId(),
                sanpinMenuNorm.getDoneProtein(),
                sanpinMenuNorm.getPlanProtein(),
                sanpinMenuNorm.getDoneKcal(),
                sanpinMenuNorm.getPlanKcal(),
                sanpinMenuNorm.getDoneOil(),
                sanpinMenuNorm.getPlanOil(),
                sanpinMenuNorm.getDoneCarbohydrates(),
                sanpinMenuNorm.getPlanCarbohydrates(),
                sanpinMenuNorm.getMultiMenu().getName(),
                sanpinMenuNorm.getMultiMenu().getId()
        );
    }

    default List<SanpinMenuNormResDTO> parserListSanPin(List<SanpinMenuNorm> list) {

        List<SanpinMenuNormResDTO> dtoList = new ArrayList<>();

        for (SanpinMenuNorm sanpinMenuNorm : list) {
            dtoList.add(parser(sanpinMenuNorm));
        }

        return dtoList;
    }
}
