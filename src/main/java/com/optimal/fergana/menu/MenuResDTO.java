package com.optimal.fergana.menu;

import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandardResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MenuResDTO {
    private UUID id;
    private String name;
    private Timestamp createDate;
    private Timestamp updateDate;
    private List<MealTimeStandardResDTO> mealTimeStandardList;
    private String multiMenuName;
    private UUID multiMenuId;


    public MenuResDTO(UUID id, String name, Timestamp createDate, Timestamp updateDate, String multiMenuName, UUID multiMenuId) {
        this.id = id;
        this.name = name;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.multiMenuName = multiMenuName;
        this.multiMenuId = multiMenuId;
    }
}
