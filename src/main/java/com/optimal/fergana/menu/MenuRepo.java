package com.optimal.fergana.menu;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MenuRepo extends JpaRepository<Menu, UUID> {
}
