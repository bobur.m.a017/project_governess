package com.optimal.fergana.menu.ageStandard;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AgeStandardService implements AgeStandardInterface, AgeStandardServiceInter {

    private final AgeGroupRepo ageGroupRepo;


    public List<AgeStandard> add(List<AgeStandardDTO> dtoList, MealAgeStandard mealAgeStandard) {

        List<AgeStandard> list = new ArrayList<>();

        Meal meal = mealAgeStandard.getMeal();
        BigDecimal protein = meal.getProtein().divide(meal.getWeight(), 6, RoundingMode.HALF_UP);
        BigDecimal oil = meal.getOil().divide(meal.getWeight(), 6, RoundingMode.HALF_UP);
        BigDecimal carbohydrates = meal.getCarbohydrates().divide(meal.getWeight(), 6, RoundingMode.HALF_UP);
        BigDecimal kcal = meal.getKcal().divide(meal.getWeight(), 6, RoundingMode.HALF_UP);

        for (AgeStandardDTO ageStandardDTO : dtoList) {
            Optional<AgeGroup> optionalAgeGroup = ageGroupRepo.findById(ageStandardDTO.getAgeGroupId());

            BigDecimal weight = BigDecimal.valueOf(ageStandardDTO.getWeight());

            optionalAgeGroup.ifPresent(ageGroup -> list.add(new AgeStandard(
                    weight,
                    protein.multiply(weight),
                    kcal.multiply(weight),
                    oil.multiply(weight),
                    carbohydrates.multiply(weight),
                    ageGroup, mealAgeStandard)));
        }
        return list;
    }
}
