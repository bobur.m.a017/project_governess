package com.optimal.fergana.menu.ageStandard;

import java.util.ArrayList;
import java.util.List;

public interface AgeStandardInterface {

    default AgeStandardResDTO parser(AgeStandard ageStandard) {
        return new AgeStandardResDTO(
                ageStandard.getId(),
                (ageStandard.getWeight()),
                ageStandard.getProtein(),
                ageStandard.getKcal(),
                ageStandard.getOil(),
                ageStandard.getCarbohydrates(),
                ageStandard.getCreateDate(),
                ageStandard.getUpdateDate(),
                ageStandard.getAgeGroup().getName(),
                ageStandard.getAgeGroup().getId()
        );
    }

    default List<AgeStandardResDTO> parserList(List<AgeStandard> list) {

        List<AgeStandardResDTO> dtoList = new ArrayList<>();

        for (AgeStandard ageStandard : list) {
            dtoList.add(parser(ageStandard));
        }
        return dtoList;
    }
}
