package com.optimal.fergana.menu.ageStandard;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgeStandard {

    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;
    @Column(precision = 19, scale = 6)
    private BigDecimal protein;
    @Column(precision = 19, scale = 6)
    private BigDecimal kcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal oil;
    @Column(precision = 19, scale = 6)
    private BigDecimal carbohydrates;

    @CreationTimestamp
    private Timestamp createDate;
    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private AgeGroup ageGroup;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private MealAgeStandard mealAgeStandard;


    public AgeStandard(BigDecimal weight, BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates, AgeGroup ageGroup, MealAgeStandard mealAgeStandard) {
        this.weight = weight;
        this.protein = protein;
        this.kcal = kcal;
        this.oil = oil;
        this.carbohydrates = carbohydrates;
        this.ageGroup = ageGroup;
        this.mealAgeStandard = mealAgeStandard;
    }
}
