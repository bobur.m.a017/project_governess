package com.optimal.fergana.menu.ageStandard;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgeStandardResDTO {

    private UUID id;
    private BigDecimal weight;
    private BigDecimal protein;
    private BigDecimal kcal;
    private BigDecimal oil;
    private BigDecimal carbohydrates;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String ageGroupName;
    private Integer ageGroupId;
}
