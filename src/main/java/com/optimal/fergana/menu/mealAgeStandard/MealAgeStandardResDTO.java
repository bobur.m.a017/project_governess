package com.optimal.fergana.menu.mealAgeStandard;

import com.optimal.fergana.ageGroup.AgeGroupResDTO;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardResDTO;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealAgeStandardResDTO {
    private UUID id;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String mealName;
    private Integer mealId;
    private List<AgeStandardResDTO> ageStandardList;


    public MealAgeStandardResDTO(UUID id, Timestamp createDate, Timestamp updateDate, String mealName, Integer mealId) {
        this.id = id;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.mealName = mealName;
        this.mealId = mealId;
    }
}
