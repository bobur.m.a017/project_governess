package com.optimal.fergana.menu.mealAgeStandard;

import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealAgeStandard {

    @Id
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    private Timestamp createDate;
    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private Meal meal;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mealAgeStandard")
    private List<AgeStandard> ageStandardList;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private MealTimeStandard mealTimeStandard;



    public MealAgeStandard(Meal meal, MealTimeStandard mealTimeStandard) {
        this.meal = meal;
        this.mealTimeStandard = mealTimeStandard;
    }
}
