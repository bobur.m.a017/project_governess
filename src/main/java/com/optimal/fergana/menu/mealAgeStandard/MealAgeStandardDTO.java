package com.optimal.fergana.menu.mealAgeStandard;

import com.optimal.fergana.menu.ageStandard.AgeStandardDTO;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.List;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealAgeStandardDTO {
    @NotNull
    private Integer mealId;
    @NotNull
    private List<AgeStandardDTO> ageStandardList;
}
