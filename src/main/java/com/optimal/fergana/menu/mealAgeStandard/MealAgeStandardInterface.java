package com.optimal.fergana.menu.mealAgeStandard;

public interface MealAgeStandardInterface {

    default MealAgeStandardResDTO parser(MealAgeStandard mealAgeStandard){

        return new MealAgeStandardResDTO(
                mealAgeStandard.getId(),
                mealAgeStandard.getCreateDate(),
                mealAgeStandard.getUpdateDate(),
                mealAgeStandard.getMeal().getName(),
                mealAgeStandard.getMeal().getId()
        );
    }
}
