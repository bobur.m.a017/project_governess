package com.optimal.fergana.menu.mealAgeStandard;

import com.optimal.fergana.menu.mealTimeStandard.MealTimeStandard;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;

import java.util.List;
import java.util.UUID;

public interface MealAgeStandardServiceInter {

    StateMessage add(MealAgeStandardDTO dto, UUID mealTimeStandardId, Users users);

    StateMessage edit(MealAgeStandardDTO dto, UUID mealTimeStandardId, UUID id);

    StateMessage delete(UUID id, Users users);

    List<MealAgeStandardResDTO> getAll(MealTimeStandard mealTimeStandard);
}
