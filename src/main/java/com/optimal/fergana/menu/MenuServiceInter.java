package com.optimal.fergana.menu;

import com.optimal.fergana.mealTime.MealTime;
import com.optimal.fergana.multiMenu.MultiMenu;

import java.util.List;

public interface MenuServiceInter {
    List<Menu> add(MultiMenu multiMenu, Integer day, List<MealTime> mealTimeList);
    List<MenuResDTO> getAll(MultiMenu multiMenu);
}
