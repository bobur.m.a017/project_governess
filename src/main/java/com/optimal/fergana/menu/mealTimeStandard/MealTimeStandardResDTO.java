package com.optimal.fergana.menu.mealTimeStandard;


import com.optimal.fergana.ageGroup.AgeGroupResDTO;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandardResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealTimeStandardResDTO {
    private UUID id;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String mealTimeName;
    private Integer mealTimeId;
    private List<MealAgeStandardResDTO> mealAgeStandardList;
    private String menuName;
    private UUID menuId;
    private List<AgeGroupResDTO> ageStandardList;



    public MealTimeStandardResDTO(UUID id, Timestamp createDate, Timestamp updateDate, String mealTimeName, Integer mealTimeId, String menuName, UUID menuId) {
        this.id = id;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.mealTimeName = mealTimeName;
        this.mealTimeId = mealTimeId;
        this.menuName = menuName;
        this.menuId = menuId;
    }
}
