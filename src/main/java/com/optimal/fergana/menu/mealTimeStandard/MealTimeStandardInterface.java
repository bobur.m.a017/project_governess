package com.optimal.fergana.menu.mealTimeStandard;

public interface MealTimeStandardInterface {

    default MealTimeStandardResDTO parser(MealTimeStandard mealTimeStandard){
        return new MealTimeStandardResDTO(
                mealTimeStandard.getId(),
                mealTimeStandard.getCreateDate(),
                mealTimeStandard.getUpdateDate(),
                mealTimeStandard.getMealTime().getName(),
                mealTimeStandard.getMealTime().getId(),
                mealTimeStandard.getMenu().getName(),
                mealTimeStandard.getMenu().getId()
        );
    }
}
