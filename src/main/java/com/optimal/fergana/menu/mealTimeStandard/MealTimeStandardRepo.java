package com.optimal.fergana.menu.mealTimeStandard;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MealTimeStandardRepo extends JpaRepository<MealTimeStandard, UUID> {
}
