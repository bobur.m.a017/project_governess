package com.optimal.fergana.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersSaveRepo extends JpaRepository<UsersSave, Integer> {

}
