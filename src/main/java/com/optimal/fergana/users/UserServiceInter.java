package com.optimal.fergana.users;

import com.optimal.fergana.message.StateMessage;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserServiceInter {
    StateMessage addUserDepartment(Integer departmentId, UsersDTO dto, HttpServletRequest request);

    StateMessage edit(UsersDTO usersDTO, HttpServletRequest request, Integer id);

    StateMessage delete(Integer id);

    UsersResDTO getOne(Integer id);

    List<UsersResDTO> getAll(String type, HttpServletRequest request, Integer id);
}
