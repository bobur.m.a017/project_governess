package com.optimal.fergana.users.permission;


import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.role.RoleType;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PermissionService {

    private final KindergartenRepo kindergartenRepo;
    private final DepartmentRepo departmentRepo;
    private final UsersRepo usersRepo;


    public List<PermissionDTO> get(Users users) {

        List<PermissionDTO> dtoList = new ArrayList<>();

        if (users.getPermissionState() || users.getRoles().get(0).getName().equals(RoleType.ADMIN.getName())) {
            if (users.getDepartment() != null) {
                Department department = users.getDepartment();
                List<Kindergarten> kindergartenList = department.getKindergartenList();

                kindergartenList.sort(Comparator.comparing(Kindergarten::getNumber));

                for (Kindergarten kindergarten : kindergartenList) {
                    PermissionDTO dto = new PermissionDTO();
                    dto.setName(kindergarten.getNumber() + kindergarten.getName());
                    dto.setId(kindergarten.getId());
                    boolean state = false;
                    for (Users user : kindergarten.getUsersList()) {
                        if (user.getPermissionState()) {
                            state = true;
                            break;
                        }
                    }
                    dto.setState(state);
                    dto.setStatus(state ? "RUXSAT ETILGAN" : "RUXSAT ETILMAGAN");
                    dtoList.add(dto);
                }
            } else if (users.getDepartment() == null && users.getRegionalDepartment() != null) {

                RegionalDepartment regionalDepartment = users.getRegionalDepartment();
                List<Department> departmentList = regionalDepartment.getDepartmentList();

                departmentList.sort(Comparator.comparing(Department::getName));
                for (Department department : departmentList) {
                    PermissionDTO dto = new PermissionDTO();
                    dto.setName(department.getName());
                    dto.setId(department.getId());
                    boolean state = false;
                    for (Users user : department.getUsersList()) {
                        if (user.getPermissionState()) {
                            state = true;
                            break;
                        }
                    }
                    dto.setState(state);
                    dto.setStatus(state ? "RUXSAT ETILGAN" : "RUXSAT ETILMAGAN");
                    dtoList.add(dto);
                }
            }
        }
        return dtoList;
    }

    public boolean checkDepartment(Department department) {
        for (Users user : department.getUsersList()) {
            if (user.getPermissionState()) {
                return true;
            }
        }
        return false;
    }

    public boolean checkKindergarten(Kindergarten kindergarten) {


        boolean state = false;
        for (Users user : kindergarten.getUsersList()) {
            if (user.getPermissionState()) {
                state = true;
                break;
            }
        }

        return state;
    }

    public StateMessage changeStatus(Users users, Integer id) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        boolean res = true;

        if (users.getRoles().get(0).getName().equals(RoleType.HUMAN_RESOURCES.getName()) && users.getPermissionState()) {
            Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(id);
            if (optionalKindergarten.isPresent()) {
                Kindergarten kindergarten = optionalKindergarten.get();
                for (Users user : kindergarten.getUsersList()) {
                    if (user.getRoles().get(0).getName().equals(RoleType.NURSE.getName())) {
                        user.setPermissionState(!user.getPermissionState());
                        user.setPermissionStateEditDate(new Timestamp(System.currentTimeMillis()));
                        usersRepo.save(user);
                        message = Message.EDIT_UZ;
                        res = false;
                    }
                }
            }
        } else if (users.getRoles().get(0).getName().equals(RoleType.ADMIN.getName())) {
            Optional<Department> optionalDepartment = departmentRepo.findById(id);
            if (optionalDepartment.isPresent()) {

                boolean state = false;
                Department department = optionalDepartment.get();
                for (Users user : department.getUsersList()) {
                    if (user.getRoles().get(0).getName().equals(RoleType.HUMAN_RESOURCES.getName())) {
                        user.setPermissionState(!user.getPermissionState());
                        user.setPermissionStateEditDate(new Timestamp(System.currentTimeMillis()));

                        state = user.getPermissionState();

                        usersRepo.save(user);
                        message = Message.EDIT_UZ;
                        res = false;
                    }
                }

                if (!state) {
                    for (Kindergarten kindergarten : department.getKindergartenList()) {
                        for (Users user : kindergarten.getUsersList()) {
                            if (user.getRoles().get(0).getName().equals(RoleType.NURSE.getName())) {
                                user.setPermissionState(false);
                                user.setPermissionStateEditDate(new Timestamp(System.currentTimeMillis()));
                                usersRepo.save(user);
                            }
                        }
                    }
                }
            }
        }

        StateMessage stateMessage = new StateMessage().parse(message);

        if (res)
            stateMessage.setText("Ushbu bo`limda xodimlar mavjud emas");

        return stateMessage;
    }
}
