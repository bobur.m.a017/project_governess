package com.optimal.fergana.users.permission;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PermissionDTO {

    private Integer id;
    private String name;
    private Timestamp date;
    private String status;
    private boolean state;
}
