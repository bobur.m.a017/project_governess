package com.optimal.fergana.users.loginParol;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.optimal.fergana.accountant.Accountant;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.notification.Notification;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.role.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class LoginParol {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String fatherName;
    private String surname;
    private String username;
    private String password;
    private String phoneNumber;
    private String role;
    private String department;
    private String kindergarten;

    public LoginParol(String name, String fatherName, String surname, String username, String password, String phoneNumber, String role, String department, String kindergarten) {
        this.name = name;
        this.fatherName = fatherName;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.department = department;
        this.kindergarten = kindergarten;
    }
}