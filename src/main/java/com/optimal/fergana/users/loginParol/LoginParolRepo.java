package com.optimal.fergana.users.loginParol;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginParolRepo extends JpaRepository<LoginParol,Integer> {
}
