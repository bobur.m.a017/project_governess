package com.optimal.fergana.users;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Getter
@Setter
@Entity
public class UsersSave {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String fatherName;
    private String surname;
    private String username;
    private String password;
    private String phoneNumber;
    private Integer roleId;
    private Integer departmentId;
    private Integer kindergartenSTIR;
}
