package com.optimal.fergana.productMeal;

import com.optimal.fergana.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashMap;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MealByProductResDTO {
    private Integer attachmentId;
    private String productName;
    private Integer productId;
    private BigDecimal multiply;

}
