package com.optimal.fergana.productMeal;

import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.meal.MealRepo;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandard;
import com.optimal.fergana.menu.mealAgeStandard.MealAgeStandardRepo;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class ProductMealService implements ProductMealServiceInter {

    private final ProductRepo productRepo;
    private final UserService userService;
    private final ReportRepo reportRepo;
    private final ProductMealRepo productMealRepo;
    private final MealAgeStandardRepo mealAgeStandardRepo;

    public ProductMealService(ProductRepo productRepo, UserService userService, KindergartenRepo kindergartenRepo, ReportRepo reportRepo, ProductMealRepo productMealRepo, MealRepo mealRepo, AgeStandardRepo ageStandardRepo, MealAgeStandardRepo mealAgeStandardRepo) {
        this.productRepo = productRepo;
        this.userService = userService;
        this.reportRepo = reportRepo;
        this.productMealRepo = productMealRepo;
        this.mealAgeStandardRepo = mealAgeStandardRepo;
    }

    public List<ProductMeal> add(List<ProductMealDTO> list, Meal meal) {

        List<ProductMeal> productMealList = new ArrayList<>();

        for (ProductMealDTO productMealDTO : list) {
            Optional<Product> optionalProduct = productRepo.findById(productMealDTO.getProductId());
            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();
                productMealList.add(new ProductMeal(
                        BigDecimal.valueOf(productMealDTO.getWeight()),
                        BigDecimal.valueOf(productMealDTO.getWaste()),
                        getProtein(product, productMealDTO.getWeight()),
                        getKcal(product, productMealDTO.getWeight()),
                        getOil(product, productMealDTO.getWeight()),
                        getCarbohydrates(product, productMealDTO.getWeight()),
                        product,
                        meal
                ));
            }
        }
        return productMealList;
    }

    private BigDecimal getProtein(Product product, Double weight) {

        BigDecimal protein = product.getProtein();
        BigDecimal protein100 = protein.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        return protein100.multiply(BigDecimal.valueOf(weight));
    }

    private BigDecimal getOil(Product product, Double weight) {
        BigDecimal oil = product.getOil();
        BigDecimal oil100 = oil.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        return oil100.multiply(BigDecimal.valueOf(weight));
    }

    private BigDecimal getCarbohydrates(Product product, Double weight) {
        BigDecimal carbohydrates = product.getCarbohydrates();
        BigDecimal carbohydrates100 = carbohydrates.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        return carbohydrates100.multiply(BigDecimal.valueOf(weight));
    }

    private BigDecimal getKcal(Product product, Double weight) {
        BigDecimal kcal = product.getKcal();
        BigDecimal kcal100 = kcal.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        return kcal100.multiply(BigDecimal.valueOf(weight));
    }


//    public List<MealByProductResDTO> getProductMeal(long date, UUID mealAgeStandardId, HttpServletRequest request) {
//
//        Users user = userService.parseToken(request);
//        Integer kindergartenId = user.getKindergarten().getId();
//
//        List<MealByProductResDTO> productResDTOList = new ArrayList<>();
//
//        LocalDateTime localDateTime = new Timestamp(date).toLocalDateTime();
//
//        Optional<MealAgeStandard> optionalMealAgeStandard = mealAgeStandardRepo.findById(mealAgeStandardId);
//        if (optionalMealAgeStandard.isPresent()) {
//            MealAgeStandard mealAgeStandard = optionalMealAgeStandard.get();
//            List<ProductMeal> productMealList = productMealRepo.findAllByMeal_Id(mealAgeStandard.getMeal().getId());
//
//            int attachmentId = mealAgeStandard.getMeal().getAttachment().getId();
//
//            BigDecimal totalMealWeight = null;
//
//
//            for (AgeStandard ageStandard : mealAgeStandard.getAgeStandardList()) {
//
//                for (ProductMeal productMeal : productMealList) {
//
//                    BigDecimal productMealWeight = productMeal.getWeight();
//
//                    BigDecimal sum = ageStandard.getWeight().multiply(productMealWeight).divide(BigDecimal.valueOf(100));
//                    totalMealWeight = sum.add(sum);
//                }
//            }
//
//
//
//            BigDecimal totalWeight = null;
//
//
//            Optional<Report> optionalReport = reportRepo.findByYearAndMonthAndDayAndKindergarten_Id(localDateTime.getYear(), localDateTime.getMonthValue(), localDateTime.getDayOfMonth(), kindergartenId);
//            if (optionalReport.isPresent()) {
//                Report report = optionalReport.get();
//                Set<KidsNumberSub> kidsNumberSubList = report.getKidsNumber().getKidsNumberSubList();
//
//                List<AgeStandard> ageStandardList = mealAgeStandard.getAgeStandardList();
//                for (AgeStandard ageStandard : ageStandardList) {
//                    for (KidsNumberSub kidsNumberSub : kidsNumberSubList) {
//                        if (Objects.equals(ageStandard.getAgeGroup().getId(), kidsNumberSub.getAgeGroup().getId())){
//                            BigDecimal sum=ageStandard.getWeight().multiply(BigDecimal.valueOf(kidsNumberSub.getNumber()));
//                            totalWeight=sum.add(sum);
//                        }
//                    }
//                }
//            }
//
//            assert totalMealWeight != null;
//            BigDecimal multiply=totalMealWeight.multiply(totalWeight);
//        }
//
//
//
//
//
//
//        return productResDTOList;
//    }

}
