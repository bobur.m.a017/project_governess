package com.optimal.fergana.productMeal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductMealDTOAddMenu {

    private String name;
    private BigDecimal weight;
    private BigDecimal mealWeight;
    private String mealName;
}
