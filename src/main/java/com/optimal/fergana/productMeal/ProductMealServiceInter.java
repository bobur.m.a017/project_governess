package com.optimal.fergana.productMeal;

import com.optimal.fergana.meal.Meal;

import java.util.List;

public interface ProductMealServiceInter {
    List<ProductMeal> add(List<ProductMealDTO> list, Meal meal);
}
