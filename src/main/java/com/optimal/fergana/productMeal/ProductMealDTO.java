package com.optimal.fergana.productMeal;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductMealDTO {

    @NotNull
    private Double weight;

    @NotNull
    private Double waste;

    @NotNull
    private Integer productId;
}
