package com.optimal.fergana.productMeal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductMealResDTO {

    private Integer id;
    private BigDecimal weight;
    private BigDecimal waste;
    private BigDecimal protein;
    private BigDecimal kcal;
    private BigDecimal oil;
    private BigDecimal carbohydrates;
    private String name;
    private Integer productId;
    private String mealName;
    private Integer mealId;
}
