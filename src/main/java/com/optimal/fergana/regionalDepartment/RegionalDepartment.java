package com.optimal.fergana.regionalDepartment;

import com.optimal.fergana.address.region.Region;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.mealCategory.MealCategory;
import com.optimal.fergana.mealTime.MealTime;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.product_category.ProductCategory;
import com.optimal.fergana.product.sanpin_catecory.SanpinCategory;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegionalDepartment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @OneToMany(mappedBy = "regionalDepartment",cascade = CascadeType.ALL)
    private List<Department> departmentList;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne
    private Region region;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "regionalDepartment",cascade = CascadeType.ALL)
    private List<Users> usersList;


    @OneToMany(mappedBy = "regionalDepartment")
    private List<MealTime> mealTimeList;

    @OneToMany(mappedBy = "regionalDepartment")
    private List<ProductCategory> productCategories;

    @OneToMany(mappedBy = "regionalDepartment")
    private List<SanpinCategory> sanpinCategories;

    @OneToMany(mappedBy = "regionalDepartment")
    private List<Product> products;

    @OneToMany(mappedBy = "regionalDepartment")
    private List<Meal> mealList;

    @OneToMany(mappedBy = "regionalDepartment")
    private List<MealCategory> MealCategoryList;

    @OneToMany(mappedBy = "regionalDepartment")
    private List<MultiMenu> multiMenuList;

    @ManyToMany(mappedBy = "regionalDepartments")
    private List<Supplier> suppliers;

    public RegionalDepartment(String name, Region region) {
        this.name = name;
        this.region = region;
    }

}
