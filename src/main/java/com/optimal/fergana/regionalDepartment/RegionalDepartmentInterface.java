package com.optimal.fergana.regionalDepartment;

public interface RegionalDepartmentInterface {

    default RegionalDepartmentResDTO parse(RegionalDepartment regionalDepartment) {
        return new RegionalDepartmentResDTO(
                regionalDepartment.getId(),
                regionalDepartment.getName(),
                regionalDepartment.getRegion().getName()
        );
    }
}
