package com.optimal.fergana.regionalDepartment;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersDTO;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface RegionalDepartmentServiseInter {
    StateMessage add(RegionalDepartmentDTO dto);

    StateMessage edit(RegionalDepartmentDTO dto, Integer id);

    StateMessage delete(Integer id);

    ResponseEntity<?> getOne(Integer id);

    ResponseEntity<?> getAll();


}
