package com.optimal.fergana.kids.dto;

import com.optimal.fergana.kids.kidsSub.SubResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class
KidsNumberResDTO {

    private Integer id;
    private long date;
    private List<SubResDTO> subDTO;
    private boolean verified;
    private String status;
    private String kindergartenName;
    private Long updateTime;
    private String updatedBy;

    public KidsNumberResDTO(Integer id, long date, List<SubResDTO> subDTO, boolean verified, String status, String kindergartenName, Timestamp updateTime) {
        this.id = id;
        this.date = date;
        this.subDTO = subDTO;
        this.verified = verified;
        this.status = status;
        this.kindergartenName = kindergartenName;
        this.updateTime = updateTime.getTime();
    }
}
