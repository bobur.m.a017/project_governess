package com.optimal.fergana.kids.dto;

import com.optimal.fergana.kids.dto.KidsNumberResDTO;
import com.optimal.fergana.menu.MenuResDTO;
import lombok.Getter;

@Getter
public class TotalResDTO {

    private MenuResDTO menuResDTO;
    private KidsNumberResDTO kidsNumberResDTO;
}
