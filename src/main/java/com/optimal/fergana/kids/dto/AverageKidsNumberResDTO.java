package com.optimal.fergana.kids.dto;

import com.optimal.fergana.kids.kidsSub.SubResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AverageKidsNumberResDTO {


    private String kindergartenName;

    private List<SubResDTO> subDTO;

}
