package com.optimal.fergana.kids;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.attachment.Attachment;
import com.optimal.fergana.attachment.AttachmentService;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.exception.AgeGroupNotFoundException;
import com.optimal.fergana.exception.KindergartenNotFoundException;
import com.optimal.fergana.exception.MenuNotFoundException;
import com.optimal.fergana.kids.dto.KidsNumberDTO;
import com.optimal.fergana.kids.dto.KidsNumberResDTO;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kids.kidsSub.SubDTO;
import com.optimal.fergana.kids.kidsSub.SubResDTO;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.report.ReportService;
import com.optimal.fergana.reporter.pdf.KidsNumberPDFReporterByDate;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.stayTime.StayTime;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class KidsNumberService {

    private final KidsNumberRepo kidsNumberRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final AgeGroupRepo ageGroupRepo;
    private final ReportRepo reportRepo;
    private final UserService userService;
    private final ReportService reportService;
    private final CustomConverter converter;
    private final KidsNumberPDFReporterByDate kidsNumberPDFReporterByDate;
    private final DepartmentRepo departmentRepo;
    private final AttachmentService attachmentService;


    public StateMessage add(KidsNumberResDTO dto, MultipartHttpServletRequest request) throws AgeGroupNotFoundException, KindergartenNotFoundException, MenuNotFoundException, IOException {

        Users user = userService.parseToken(request);
        MultipartFile file = request.getFile("file");
        Attachment attachment = attachmentService.add(file);

        StateMessage stateMessage = checkNumberOfKids(dto.getSubDTO());

        if (!stateMessage.isSuccess()) {
            return stateMessage;
        }

        Message message = Message.BU_KUNGA_MENYU_BIRIKTIRILMAGAN;

        Kindergarten kindergarten = user.getKindergarten();
        LocalDate localDate = new Timestamp(dto.getDate()).toLocalDateTime().toLocalDate();
        Optional<KidsNumber> optionalKidsNumber = kidsNumberRepo.findByDateAndReport_KidsNumber_IdAndDeleteIsFalse(localDate, kindergarten.getId());


        Optional<Report> optionalReport = reportRepo.findByYearAndMonthAndDayAndKindergarten_Id(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth(), kindergarten.getId());
        if (optionalReport.isPresent()) {
            Report report = optionalReport.get();

            Department department = report.getKindergarten().getDepartment();

            boolean resTime = true;

            if (department.getLocalTime() != null) {
                LocalTime localTime = department.getLocalTime();
                LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), localTime);
                LocalDate date = report.getDate();
                LocalDateTime localDateTime1 = LocalDateTime.of(date, LocalTime.now());

//            if ((date.getYear() == localDateTime.getYear()) && (date.getMonthValue() == localDateTime.getMonthValue()) && (date.getDayOfMonth() == localDateTime.getDayOfMonth()) && ()) {
                resTime = (localDateTime.isBefore(localDateTime1));
            }

            if (resTime) {

                if (report.getMenu() != null) {
                    if (report.getKidsNumber() == null) {
                        KidsNumber kidsNumber = new KidsNumber();
                        kidsNumber.setDate(new Timestamp(dto.getDate()).toLocalDateTime().toLocalDate());

                        Set<KidsNumberSub> list = new HashSet<>();

                        for (AgeGroup ageGroup : ageGroupRepo.findAllByDeleteIsFalse()) {

                            boolean res = true;

                            for (SubResDTO subDTO : dto.getSubDTO()) {
                                if (subDTO.getAgeGroupId().equals(ageGroup.getId())) {
                                    KidsNumberSub kidsNumberSub = new KidsNumberSub();
                                    kidsNumberSub.setNumber(subDTO.getNumber());
                                    kidsNumberSub.setSortNumber(ageGroup.getSortNumber());
                                    kidsNumberSub.setKidsNumber(kidsNumber);
                                    kidsNumberSub.setAgeGroup(ageGroup);
                                    list.add(kidsNumberSub);
                                    res = false;
                                }
                            }
                            if (res) {
                                KidsNumberSub kidsNumberSub = new KidsNumberSub();
                                kidsNumberSub.setNumber(0);
                                kidsNumberSub.setSortNumber(ageGroup.getSortNumber());
                                kidsNumberSub.setKidsNumber(kidsNumber);
                                kidsNumberSub.setAgeGroup(ageGroup);
                                list.add(kidsNumberSub);
                            }
                        }


                        kidsNumber.setKidsNumberSubList(list);
                        kidsNumber.setReport(report);
                        kidsNumber.setStatus(Status.NEW.getName());
                        kidsNumber.setAttachment(attachment);
                        kidsNumberRepo.save(kidsNumber);
                        message = Message.SUCCESS_UZ;

                    }
                } else {
                    if (optionalKidsNumber.isPresent()) {
                        KidsNumber kidsNumber = optionalKidsNumber.get();
                        return edit(dto, kidsNumber.getId(), request);
                    } else {
                        throw new RuntimeException("bola soni topilmadi");
                    }
                }
            } else return new StateMessage("Bola sonini kiritish vaqti belgilangan vaqtdan o'tib ketdi.", false, 417);
        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(KidsNumberResDTO dto, Integer kidsNumberId, MultipartHttpServletRequest request) throws AgeGroupNotFoundException, KindergartenNotFoundException {

        MultipartFile file = request.getFile("file");


        StateMessage stateMessage = checkNumberOfKids(dto.getSubDTO());

        if (!stateMessage.isSuccess()) {
            return stateMessage;
        }


        Optional<KidsNumber> optionalKidsNumber = kidsNumberRepo.findById(kidsNumberId);
        Users user = userService.parseToken(request);
        Kindergarten kindergarten = user.getKindergarten();
        LocalDate localDate = new Timestamp(dto.getDate()).toLocalDateTime().toLocalDate();
        Optional<KidsNumber> optionalKidsNumberDate = kidsNumberRepo.findByDateAndReport_KidsNumber_IdAndDeleteIsFalse(localDate, kindergarten.getId());

        if (optionalKidsNumber.isPresent() && optionalKidsNumberDate.isEmpty()) {
            KidsNumber kidsNumber = optionalKidsNumber.get();
            kidsNumber.setDate(new Timestamp(dto.getDate()).toLocalDateTime().toLocalDate());

            List<KidsNumberSub> list = new ArrayList<>();

            for (SubResDTO subDTO : dto.getSubDTO()) {
                Optional<KidsNumberSub> optional = kidsNumberSubRepo.findById(subDTO.getId());
                if (optional.isPresent()) {
                    KidsNumberSub kidsNumberSub = optional.get();
                    kidsNumberSub.setNumber(subDTO.getNumber());


                    list.add(kidsNumberSub);
                }
            }

            Attachment kidsNumberAttachment = kidsNumber.getAttachment();
            Attachment attachment;

            if (kidsNumberAttachment != null) {
                attachment = attachmentService.edit(file, kidsNumberAttachment.getId());
            } else {
                attachment = attachmentService.add(file);
            }

            kidsNumber.setAttachment(attachment);
            kidsNumber.setUpdateDate(new Timestamp(System.currentTimeMillis()));

            kidsNumberSubRepo.saveAll(list);
            kidsNumberRepo.save(kidsNumber);
            return new StateMessage().parse(Message.EDIT_UZ);

        } else {
            throw new KindergartenNotFoundException("notogri sana kiritdingiz");
        }
    }

    public StateMessage delete(Integer id) {
        Optional<KidsNumber> optionalKidsNumber = kidsNumberRepo.findById(id);
        if (optionalKidsNumber.isPresent()) {
            KidsNumber kidsNumber = optionalKidsNumber.get();
            kidsNumber.setDelete(true);
            kidsNumberRepo.save(kidsNumber);
            return new StateMessage(Message.DELETE_UZ.getName(), true, 200);
        } else {
            return new StateMessage(Message.THE_DATA_WAS_ENTERED_INCORRECTLY.getName(), true, 401);
        }
    }

    public StateMessage verified(Integer id, Users users) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<KidsNumber> optional = kidsNumberRepo.findById(id);
        if (optional.isPresent()) {
            KidsNumber kidsNumber = optional.get();
            if (!kidsNumber.isVerified() && users.getKindergarten() != null) {
                if (kidsNumber.getReport().getKindergarten().getId().equals(users.getKindergarten().getId())) {
                    kidsNumber.setVerified(true);
                    kidsNumber.setStatus(Status.TASDIQLANDI.getName());
                    kidsNumberRepo.save(kidsNumber);

                    message = Message.SUCCESS_VERIFIED;

                    reportService.calculate(kidsNumber.getReport());
                }
            }
        }

        return new StateMessage().parse(message);
    }

    public StateMessage add(KidsNumberDTO dto, Kindergarten kindergarten) throws AgeGroupNotFoundException, KindergartenNotFoundException, MenuNotFoundException {

        LocalDate localDate = dto.getDate().toLocalDateTime().toLocalDate();
        Optional<KidsNumber> optionalKidsNumber = kidsNumberRepo.findByDateAndReport_KidsNumber_IdAndDeleteIsFalse(localDate, kindergarten.getId());


        Optional<Report> optionalReport = reportRepo.findByYearAndMonthAndDayAndKindergarten_Id(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth(), kindergarten.getId());
        if (optionalReport.isPresent()) {
            Report report = optionalReport.get();
            Menu menu = report.getMenu();
            if (menu != null) {
                if (optionalKidsNumber.isEmpty()) {
                    KidsNumber kidsNumber = new KidsNumber();
                    kidsNumber.setDate(dto.getDate().toLocalDateTime().toLocalDate());


                    Set<KidsNumberSub> list = new HashSet<>();

                    for (SubDTO subDTO : dto.getSubDTO()) {

                        Optional<AgeGroup> optionalAgeGroup = ageGroupRepo.findById(subDTO.getAgeGroupId());

                        if (optionalAgeGroup.isPresent()) {
                            KidsNumberSub kidsNumberSub = new KidsNumberSub();
                            kidsNumberSub.setNumber(subDTO.getNumber());
                            kidsNumberSub.setKidsNumber(kidsNumber);
                            AgeGroup ageGroup = optionalAgeGroup.get();
                            kidsNumberSub.setAgeGroup(ageGroup);

                            list.add(kidsNumberSub);
                        } else {
                            throw new AgeGroupNotFoundException("bunday yosh toifasi mavjud emas");
                        }
                    }
                    kidsNumber.setKidsNumberSubList(list);

                    kidsNumber.setReport(report);

                    kidsNumber.setStatus(Status.NEW.getName());

                    kidsNumberRepo.save(kidsNumber);
                    return new StateMessage(Message.SUCCESS_UZ.getName(), true, 200);

                }
            } else {
                throw new MenuNotFoundException("bu kunga menyu biriktirilmagan");
            }
        }
        return null;
    }

    public KidsNumberResDTO getOne(HttpServletRequest request, LocalDate date) {

        Users users = userService.parseToken(request);
        if (users.getKindergarten() != null) {
            Optional<Report> optionalReport = reportRepo.findByKindergarten_IdAndYearAndMonthAndDay(users.getKindergarten().getId(), date.getYear(), date.getMonthValue(), date.getDayOfMonth());
            if (optionalReport.isPresent()) {
                Report report = optionalReport.get();
                if (report.getKidsNumber() != null) {

                    List<SubResDTO> list = new ArrayList<>();
                    KidsNumberResDTO kidsNumberResDTO = converter.kidsNumberToDTO(report.getKidsNumber());

                    for (StayTime stayTime : report.getKindergarten().getStayTimeList()) {

                        for (AgeGroup ageGroup : stayTime.getAgeGroupList()) {
                            for (SubResDTO subResDTO : kidsNumberResDTO.getSubDTO()) {
                                if (ageGroup.getId().equals(subResDTO.getAgeGroupId())) {
                                    list.add(subResDTO);
                                }
                            }
                        }
                    }

                    list.sort(Comparator.comparing(SubResDTO::getSortNumber));
                    kidsNumberResDTO.setSubDTO(list);
                    kidsNumberResDTO.setUpdatedBy(users.getName()+" "+users.getSurname());
                    return kidsNumberResDTO;
                }
            }
        }
        return new KidsNumberResDTO();
    }

    public List<KidsNumberResDTO> getAllByDepartmentId(Users users, Integer id, LocalDate date) {


        if (users.getDepartment() != null)
            id = users.getDepartment().getId();

        List<KidsNumber> list = kidsNumberRepo.findAllByReport_Kindergarten_Department_IdAndDateAndDeleteIsFalse(id, date);

        List<KidsNumberResDTO> dtoList = converter.kidsNumberToDTO(list);
        dtoList.sort(Comparator.comparing(KidsNumberResDTO::getKindergartenName));

        return dtoList;
    }

    public String getAllByDepartmentIdPDF(Users users, Integer id, LocalDate date) {
        if (users.getDepartment() != null)
            id = users.getDepartment().getId();


        List<KidsNumber> list = kidsNumberRepo.findAllByReport_Kindergarten_Department_IdAndDateAndDeleteIsFalse(id, date);
        Optional<Department> optionalDepartment = departmentRepo.findById(id);
        if (optionalDepartment.isPresent()) {

            String path = kidsNumberPDFReporterByDate.getAllByDepartmentIdPDF(list, date, optionalDepartment.get());
            return StaticWords.URI + path.substring(StaticWords.DEST.length());
        } else {
            throw new RuntimeException();
        }

    }

    public List<KidsNumberResDTO> getAllByDate(Users users, Integer id, LocalDate startDate, LocalDate endDate) {

        if (users.getKindergarten() != null) {
            id = users.getKindergarten().getId();
        }

        List<KidsNumber> list = kidsNumberRepo.findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(id, startDate, endDate);
        List<KidsNumberResDTO> dtoList = converter.kidsNumberToDTO(list);

        dtoList.sort(Comparator.comparing(KidsNumberResDTO::getDate));


        return dtoList;
    }

    public String getAllByDatePDF(Users users, Integer id, LocalDate startDate, LocalDate endDate) {
        if (users.getKindergarten() != null) {
            id = users.getKindergarten().getId();
        }

        List<KidsNumber> list = kidsNumberRepo.findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(id, startDate, endDate);

        String str = kidsNumberPDFReporterByDate.createPDFKindergarten(list, startDate, endDate);

        return StaticWords.URI + str.substring(StaticWords.DEST.length());

    }

    public StateMessage checkNumberOfKids(List<SubResDTO> list) {

        for (SubResDTO subDTO : list) {
            if (subDTO.getNumber() > 700 || subDTO.getNumber() < 0) {
                return new StateMessage("Kiritilgan bola soni xaqiqatdan yiroq. Iltimos tekshirib qaytadan kiriting", false, 417);
            }
        }

        return new StateMessage("", true, 200);
    }



    public void getAll(Long date,String status,Integer MTTNumber,Integer districtId){
        if (date==null) {
            date=System.currentTimeMillis();
        }

    }
}