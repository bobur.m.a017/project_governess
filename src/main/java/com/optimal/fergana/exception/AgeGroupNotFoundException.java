package com.optimal.fergana.exception;

public class AgeGroupNotFoundException extends RuntimeException{
    public AgeGroupNotFoundException(String message) {
        super(message);
    }
}
