package com.optimal.fergana.exception;

public class KindergartenNotFoundException extends RuntimeException {
    public KindergartenNotFoundException(String message) {
        super(message);
    }
}
