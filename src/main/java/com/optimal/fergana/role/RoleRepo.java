package com.optimal.fergana.role;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface RoleRepo extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(String name);
    boolean existsByName(String name);

    @Override
    List<Role> findAllById(Iterable<Integer> integers);
}
