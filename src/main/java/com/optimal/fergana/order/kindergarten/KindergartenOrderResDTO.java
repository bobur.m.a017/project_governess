package com.optimal.fergana.order.kindergarten;

import com.optimal.fergana.order.product.ProductOrderResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KindergartenOrderResDTO {

    private UUID id;
    private Integer kindergartenId;
    private Integer number;
    private String kindergartenName;
    private List<ProductOrderResDTO> productContracts;
}