package com.optimal.fergana.order.kindergarten;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface KindergartenOrderRepo extends JpaRepository<KindergartenOrder, UUID> {

    Optional<KindergartenOrder> findByKindergarten_Id(Integer kindergarten_id);
}
