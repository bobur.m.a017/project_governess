package com.optimal.fergana.order.kindergarten;

import com.optimal.fergana.order.MyOrder;
import com.optimal.fergana.order.product.ProductOrder;
import com.optimal.fergana.kindergarten.Kindergarten;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KindergartenOrder {

    @Id
    private UUID id = UUID.randomUUID();


    @ManyToOne(fetch = FetchType.LAZY)
    private Kindergarten kindergarten;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private MyOrder myOrder;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kindergartenOrder", cascade = CascadeType.ALL)
    private Set<ProductOrder> productOrders;


    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;


    public KindergartenOrder(Kindergarten kindergarten, MyOrder myOrder, Set<ProductOrder> productOrders) {
        this.kindergarten = kindergarten;
        this.myOrder = myOrder;
        this.productOrders = productOrders;
    }
}