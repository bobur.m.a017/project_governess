package com.optimal.fergana.order.kindergarten;

import com.optimal.fergana.order.MyOrder;
import com.optimal.fergana.order.product.ProductOrder;
import com.optimal.fergana.order.product.ProductOrderService;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RequiredArgsConstructor
@Service
public class KindergartenOrderService {


    private final KindergartenRepo kindergartenRepo;
    private final ProductOrderService productOrderService;
    private final KindergartenOrderRepo kindergartenOrderRepo;


    public KindergartenOrder add(KindergartenOrderDTO dto, MyOrder myOrder) {


        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(dto.getKindergartenId());

        if (optionalKindergarten.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();

            KindergartenOrder kindergartenOrder = new KindergartenOrder(
                    kindergarten,
                    myOrder,
                    null
            );

            Set<ProductOrder> productOrders = productOrderService.add(dto.getProductContracts(), kindergartenOrder);

            if (productOrders != null) {
                kindergartenOrder.setProductOrders(productOrders);
                return kindergartenOrder;
            }
        }
        return null;
    }

    public List<KindergartenOrder> add(List<KindergartenOrderDTO> dtoList, MyOrder myOrder) {
        List<KindergartenOrder> list = new ArrayList<>();

        for (KindergartenOrderDTO kindergartenOrderDTO : dtoList) {
            KindergartenOrder kindergartenOrder = add(kindergartenOrderDTO, myOrder);
            if (kindergartenOrder == null) {
                return null;
            }
            list.add(kindergartenOrder);
        }
        return list;
    }

    public List<KindergartenOrder> edit(List<KindergartenOrderDTO> dtoList, MyOrder myOrder) {

        List<KindergartenOrder> kindergartenOrderList = myOrder.getKindergartenOrderList();
        List<KindergartenOrder> returnList = new ArrayList<>();
        List<KindergartenOrder> deleteList = new ArrayList<>();


        for (KindergartenOrder kindergartenOrder : kindergartenOrderList) {
            boolean res = true;
            for (KindergartenOrderDTO kindergartenOrderDTO : dtoList) {
                if (kindergartenOrder.getId().equals(kindergartenOrderDTO.getId())) {
                    res = false;
                    break;
                }
            }
            if (res) {
                kindergartenOrder.setMyOrder(null);
                deleteList.add(kindergartenOrder);
            }
        }

        for (KindergartenOrderDTO kindergartenOrderDTO : dtoList) {
            if (kindergartenOrderDTO.getId() != null) {
                Optional<KindergartenOrder> optionalKindergartenContract = kindergartenOrderRepo.findById(kindergartenOrderDTO.getId());
                if (optionalKindergartenContract.isPresent()) {

                    KindergartenOrder kindergartenOrder = optionalKindergartenContract.get();

                    Set<ProductOrder> productOrderList = productOrderService.edit(kindergartenOrder, kindergartenOrderDTO.getProductContracts());
                    kindergartenOrder.setProductOrders(productOrderList);

                    returnList.add(kindergartenOrder);
                } else {
                    KindergartenOrder add = add(kindergartenOrderDTO, myOrder);
                    if (add != null)
                        returnList.add(add);
                }
            }else {
                KindergartenOrder add = add(kindergartenOrderDTO, myOrder);
                if (add != null)
                    returnList.add(add);
            }
        }

        for (int i = 0; i < deleteList.size(); i++) {
            kindergartenOrderRepo.deleteById(deleteList.get(i).getId());
        }


        return kindergartenOrderRepo.saveAll(returnList);
    }
}
