package com.optimal.fergana.order;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MyOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String status;
    private boolean edit = true;

    private boolean delete = false;
    private LocalDate startDate;
    private LocalDate endDate;

    @OneToOne(fetch = FetchType.LAZY)
    private Department department;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "myOrder", cascade = CascadeType.ALL)
    private List<KindergartenOrder> kindergartenOrderList;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    @ManyToOne
    private Users createdBy;

    @ManyToOne
    private Users updatedBy;


    public MyOrder(String name, LocalDate startDate, LocalDate endDate, Department department, Users createdBy) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.department = department;
        this.createdBy = createdBy;
    }
}
