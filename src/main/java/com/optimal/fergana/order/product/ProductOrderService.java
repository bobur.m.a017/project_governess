package com.optimal.fergana.order.product;

import com.optimal.fergana.order.OrderRepo;
import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductOrderService {


    private final ProductRepo productRepo;
    private final ProductOrderRepo productOrderRepo;
    private final CustomConverter converter;
    private final ProductPackService productPackService;


    public ProductOrder add(ProductOrderDTO dto, KindergartenOrder kindergartenOrder) {
        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();

            BigDecimal weight = BigDecimal.valueOf(dto.getPackWeight());


            BigDecimal pack = productPackService.checkPack(kindergartenOrder.getKindergarten().getDepartment().getId(), dto.getProductId());

            BigDecimal packWeight = BigDecimal.valueOf(dto.getPackWeight());

            if (pack != null) {
                weight = (packWeight.multiply(pack)).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
            } else {
                pack = BigDecimal.ZERO;
            }
            return new ProductOrder(weight, pack, packWeight, product, kindergartenOrder);
        }
        return null;
    }

    public Set<ProductOrder> add(List<ProductOrderDTO> dtoList, KindergartenOrder kindergartenOrder) {

        Set<ProductOrder> list = new HashSet<>();

        for (ProductOrderDTO productOrderDTO : dtoList) {
            ProductOrder productOrder = add(productOrderDTO, kindergartenOrder);
            if (productOrder == null) {
                return null;
            }
            list.add(productOrder);
        }
        return list;
    }

    public Set<ProductOrder> edit(KindergartenOrder kindergartenOrder, List<ProductOrderDTO> productContracts) {

        Set<ProductOrder> productOrderSet = kindergartenOrder.getProductOrders();
        Set<ProductOrder> deleteSet = new HashSet<>();
        Set<ProductOrder> returnSet = new HashSet<>();


        for (ProductOrder productOrder : productOrderSet) {
            boolean res = true;
            for (ProductOrderDTO contract : productContracts) {
                if (contract.getId() != null) {
                    if (contract.getId().equals(productOrder.getId())) {
                        res = false;
                        break;
                    }
                }
            }
            if (res) {
                deleteSet.add(productOrder);
            }
        }

        for (ProductOrderDTO productOrder : productContracts) {
            if (productOrder.getId() != null) {

                Optional<ProductOrder> optionalProductContract = productOrderRepo.findById(productOrder.getId());
                if (optionalProductContract.isPresent()) {


                    BigDecimal pack = productPackService.checkPack(kindergartenOrder.getKindergarten().getDepartment().getId(), productOrder.getProductId());

                    BigDecimal packWeight = BigDecimal.valueOf(productOrder.getPackWeight());
                    BigDecimal weight = BigDecimal.valueOf(productOrder.getPackWeight());


                    if (pack != null) {
                        weight = (packWeight.multiply(pack)).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
                    } else {
                        pack = BigDecimal.ZERO;
                    }

                    ProductOrder contract = optionalProductContract.get();
                    contract.setWeight(weight);
                    contract.setPack(pack);
                    contract.setPackWeight(packWeight);

                    returnSet.add(contract);
                }
            } else {
                returnSet.add(add(productOrder, kindergartenOrder));
            }
        }
        productOrderRepo.deleteAll(deleteSet);

        return new HashSet<>(productOrderRepo.saveAll(returnSet));
    }

}