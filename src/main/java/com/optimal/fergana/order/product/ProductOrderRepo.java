package com.optimal.fergana.order.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProductOrderRepo extends JpaRepository<ProductOrder, UUID> {

}
