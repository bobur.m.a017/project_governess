package com.optimal.fergana.order.toSendProduct;

import com.optimal.fergana.kindergarten.Kindergarten;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ToSendProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private Kindergarten kindergarten;

    @OneToMany(mappedBy = "toSendProduct", cascade = CascadeType.ALL)
    private List<ProductWeight> productWeight;

}
