package com.optimal.fergana.order.toSendProduct;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductWeightResDTO {


    private int id;
    private BigDecimal weight;
    private BigDecimal pack;
    private BigDecimal packWeight;

    private Integer productId;
    private String productName;
    private BigDecimal maxWeight;

}
