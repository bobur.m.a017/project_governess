package com.optimal.fergana.order.toSendProduct;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ToSendProductRepo extends JpaRepository<ToSendProduct, Integer> {
    Optional<ToSendProduct> findByKindergarten_Id(Integer kindergarten_id);


}
