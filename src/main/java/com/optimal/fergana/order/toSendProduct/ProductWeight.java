package com.optimal.fergana.order.toSendProduct;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.optimal.fergana.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class ProductWeight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private Product product;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal pack = BigDecimal.valueOf(0);

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight = BigDecimal.valueOf(0);
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private ToSendProduct toSendProduct;

    public ProductWeight(Product product, BigDecimal weight, BigDecimal pack, BigDecimal packWeight, ToSendProduct toSendProduct) {
        this.product = product;
        this.weight = weight;
        this.pack = pack;
        this.packWeight = packWeight;
        this.toSendProduct = toSendProduct;
    }

}