package com.optimal.fergana.order.toSendProduct;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductWeightRepo extends JpaRepository<ProductWeight, Integer> {
    List<ProductWeight> findAllByToSendProduct_Kindergarten_Id(Integer kindergarten_id);
    Optional<ProductWeight> findByProduct_IdAndToSendProduct_Kindergarten_Id(Integer product_id, Integer kindergarten_id);
}
