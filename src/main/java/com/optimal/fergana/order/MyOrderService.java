package com.optimal.fergana.order;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.order.kindergarten.KindergartenOrderRepo;
import com.optimal.fergana.order.kindergarten.KindergartenOrderService;
import com.optimal.fergana.order.product.ProductOrder;
import com.optimal.fergana.order.product.ProductOrderRepo;
import com.optimal.fergana.order.toSendProduct.ProductWeight;
import com.optimal.fergana.order.toSendProduct.ProductWeightRepo;
import com.optimal.fergana.order.toSendProduct.ToSendProduct;
import com.optimal.fergana.order.toSendProduct.ToSendProductRepo;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.productPack.ProductPack;
import com.optimal.fergana.product.productPack.ProductPackRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;


@RequiredArgsConstructor
@Service
public class MyOrderService implements OrderServiceInterface {


    private final OrderRepo orderRepo;
    private final KindergartenOrderService kindergartenOrderService;
    private final CustomConverter converter;
    private final ReportRepo reportRepo;
    private final AgeStandardRepo ageStandardRepo;
    private final KindergartenOrderRepo kindergartenOrderRepo;
    private final ProductPackRepo productPackRepo;
    private final ProductOrderRepo productOrderRepo;
    private final ToSendProductRepo toSendProductRepo;
    private final ProductWeightRepo productWeightRepo;
    private final ProductPackService productPackService;


    public StateMessage add(OrderDTO dto, Users user) {
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        Department department = user.getDepartment();

        boolean byName = orderRepo.existsAllByNameAndDeleteIsFalse(dto.getName());

        if (!byName) {
            MyOrder myOrder = new MyOrder(dto.getName(), dto.getStart().toLocalDateTime().toLocalDate(), dto.getEnd().toLocalDateTime().toLocalDate(), department, user);

            myOrder = orderRepo.save(myOrder);
            for (Kindergarten kindergarten : department.getKindergartenList()) {
                KidsNumber averageKidsNumber = kindergarten.getAverageKidsNumber();
                if (averageKidsNumber != null) {
                    KindergartenOrder kindergartenOrder = new KindergartenOrder(kindergarten, myOrder, new HashSet<>());
                    Set<ProductOrder> productOrderSet = new HashSet<>();
                    for (Report report : reportRepo.findAllByKindergarten_IdAndDateBetween(kindergarten.getId(), myOrder.getStartDate(), myOrder.getEndDate())) {
                        if (report.getMenu() != null) {
                            calculateProduct(averageKidsNumber, productOrderSet, report.getMenu(), kindergartenOrder);
                        }
                    }
                    if (productOrderSet.size() > 0) {
                        for (ProductOrder productOrder : productOrderSet) {
                            BigDecimal weight = productOrder.getWeight().divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);
                            productOrder.setWeight(weight);
                            productOrder.setPackWeight(weight);
                        }

                        List<KindergartenWarehouse> kindergartenWarehouses = kindergarten.getKindergartenWarehouses();
//                        calculateKindergartenWarehouse(dto.getStart().toLocalDateTime().toLocalDate(), kindergartenOrder.getKindergarten(), kindergartenWarehouses);   //BOBUR AKA BIR OYDAN BERI SHUNI QILISH KERAK DEB BONG URVOTUDI VA NIXOYAT QILDIM

                        calculateKW(productOrderSet, kindergartenWarehouses);

                        for (ProductOrder productOrder : productOrderSet) {

                            BigDecimal checkPack = productPackService.checkPack(department.getId(), productOrder.getProduct().getId());

                            if (checkPack != null) {

                                BigDecimal pack = checkPack.divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);

                                BigDecimal[] bigDecimals = productOrder.getWeight().divideAndRemainder(pack);

                                BigDecimal packWeight = bigDecimals[0];

                                if (bigDecimals[1].compareTo(BigDecimal.valueOf(0.3)) > 0) {
                                    packWeight = packWeight.add(BigDecimal.ONE);
                                }

                                productOrder.setPackWeight(packWeight);
                                productOrder.setPack(checkPack);
                                productOrder.setWeight((packWeight.multiply(pack)));
                            } else {
                                productOrder.setPackWeight(productOrder.getWeight());
                            }
                        }

                        kindergartenOrder.setProductOrders(productOrderSet);
                        kindergartenOrderRepo.save(kindergartenOrder);
                    }
                }
            }
            message = Message.SUCCESS_UZ;
        }
        return new StateMessage().parse(message);
    }

    public void calculateKW(Set<ProductOrder> productOrderSet, List<KindergartenWarehouse> kindergartenWarehouseList) {
        for (ProductOrder productOrder : productOrderSet) {
            for (KindergartenWarehouse kindergartenWarehouse : kindergartenWarehouseList) {
                if (productOrder.getProduct().getId().equals(kindergartenWarehouse.getProduct().getId())) {
                    BigDecimal subtract = productOrder.getWeight().subtract(kindergartenWarehouse.getTotalWeight());
                    if (subtract.compareTo(BigDecimal.valueOf(0)) > 0){
                        productOrder.setWeight(subtract);
                    }else {
                        productOrder.setWeight(BigDecimal.ZERO);
                    }
                }
            }
        }
    }

    public void calculateKindergartenWarehouse(LocalDate start, Kindergarten kindergarten, List<KindergartenWarehouse> kindergartenWarehouses) {

        List<Report> reportList = reportRepo.findAllByKindergarten_IdAndDateBetween(kindergarten.getId(), start, LocalDate.now());
        for (Report report : reportList) {
            Menu menu = report.getMenu();
            if (menu != null) {
                KidsNumber averageKidsNumber = kindergarten.getAverageKidsNumber();
                if (averageKidsNumber != null) {
                    for (AgeStandard ageStandard : ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_Id(menu.getId())) {
                        Meal meal = ageStandard.getMealAgeStandard().getMeal();
                        BigDecimal mealWeight = meal.getWeight();
                        for (ProductMeal productMeal : meal.getProductMealList()) {
                            BigDecimal divide = productMeal.getWeight().divide(mealWeight, 6, RoundingMode.HALF_UP);
                            BigDecimal multiply = divide.multiply(ageStandard.getWeight());
                            for (KidsNumberSub kidsNumberSub : averageKidsNumber.getKidsNumberSubList()) {
                                if (kidsNumberSub.getAgeGroup().getId().equals(ageStandard.getAgeGroup().getId())) {
                                    BigDecimal weightMultiply = multiply.multiply(BigDecimal.valueOf(kidsNumberSub.getNumber()));

                                    for (KindergartenWarehouse kindergartenWarehouse : kindergartenWarehouses) {
                                        if (kindergartenWarehouse.getProduct().getId().equals(productMeal.getProduct().getId())) {
                                            kindergartenWarehouse.setTotalWeight(kindergartenWarehouse.getTotalWeight().subtract(weightMultiply));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void calculateProduct(KidsNumber kidsNumber, Set<ProductOrder> productOrderList, Menu menu, KindergartenOrder kindergartenOrder) {

        for (AgeStandard ageStandard : ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_Id(menu.getId())) {
            Meal meal = ageStandard.getMealAgeStandard().getMeal();
            BigDecimal mealWeight = meal.getWeight();
            for (ProductMeal productMeal : meal.getProductMealList()) {
                BigDecimal divide = productMeal.getWeight().divide(mealWeight, 6, RoundingMode.HALF_UP);
                BigDecimal multiply = divide.multiply(ageStandard.getWeight());
                for (KidsNumberSub kidsNumberSub : kidsNumber.getKidsNumberSubList()) {
                    if (kidsNumberSub.getAgeGroup().getId().equals(ageStandard.getAgeGroup().getId())) {
                        BigDecimal weightMultiply = multiply.multiply(BigDecimal.valueOf(kidsNumberSub.getNumber()));
                        addProductList(productOrderList, productMeal.getProduct(), weightMultiply, kindergartenOrder);
                    }
                }
            }
        }
    }

    public void addProductList(Set<ProductOrder> productOrderList, Product product, BigDecimal weight, KindergartenOrder kindergartenOrder) {
        boolean res = true;

        for (ProductOrder productOrder : productOrderList) {
            if (productOrder.getProduct().getId().equals(product.getId())) {
                productOrder.setWeight(productOrder.getWeight().add(weight));
                productOrder.setPackWeight(productOrder.getPackWeight().add(weight));
                res = false;
                break;
            }
        }
        if (res) {
            productOrderList.add(new ProductOrder(weight, weight, product, kindergartenOrder));
        }
    }

    public StateMessage edit(OrderDTO contractDTO, Integer id, Users users) {

        Optional<MyOrder> optionalOrder = orderRepo.findById(id);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optionalOrder.isPresent()) {
            MyOrder myOrder = optionalOrder.get();
            List<KindergartenOrder> edit = kindergartenOrderService.edit(contractDTO.getKindergartenOrderDTOList(), myOrder);
            myOrder.setUpdatedBy(users);
            orderRepo.save(myOrder);
            message = Message.EDIT_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage delete(Integer id) {
        Optional<MyOrder> optionalContract = orderRepo.findById(id);
        Message message = Message.THE_CONTRACT_CANNOT_BE_CANCELED;

        if (optionalContract.isPresent()) {
            MyOrder myOrder = optionalContract.get();
            myOrder.setDelete(true);
            orderRepo.save(myOrder);
            message = Message.DELETE_UZ;
        }
        return new StateMessage().parse(message);
    }

    public OrderResDTO getOne(Integer id) {
        Optional<MyOrder> optionalContract = orderRepo.findById(id);
        if (optionalContract.isPresent()) {
            MyOrder myOrder = optionalContract.get();

            OrderResDTO orderResDTO = converter.orderToDTO(myOrder);
            orderResDTO.setKindergartenContractList(converter.kindergartenOrderToDTO(myOrder.getKindergartenOrderList()));
            return orderResDTO;
        } else {
            throw new UsernameNotFoundException("this object not found in the database");
        }
    }

    public CustomPageable getAll(Users users, Integer pageSize, Integer page) {

        if (page == null) page = 0;
        if (pageSize == null) pageSize = 10;


        Pageable pageable = PageRequest.of(page, pageSize);
        Page<MyOrder> contractPage = null;

        contractPage = orderRepo.findAllByDeleteIsFalseAndDepartment_IdOrderByCreateDateDesc(users.getDepartment().getId(), pageable);

        if (contractPage != null) {

            return new CustomPageable(contractPage.getSize(), contractPage.getNumber(), converter.orderToDTO(contractPage.stream().toList()), contractPage.getTotalElements());
        } else {
            throw new UsernameNotFoundException("Siz kiritgan paramertlar bo`yicha ma‘lumot mavjud emas");
        }
    }

    public StateMessage toRoundOff(Integer id, BigDecimal weight) {

        Optional<MyOrder> optional = orderRepo.findById(id);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        List<ProductOrder> list = new ArrayList<>();

        if (optional.isPresent() && weight.compareTo(BigDecimal.ZERO) > 0) {
            MyOrder myOrder = optional.get();

            for (KindergartenOrder kindergartenOrder : myOrder.getKindergartenOrderList()) {
                for (ProductOrder productOrder : kindergartenOrder.getProductOrders()) {

                    BigDecimal pack = productOrder.getPack();

                    if (pack.compareTo(BigDecimal.valueOf(0)) == 0) {

                        BigDecimal orderWeight = productOrder.getWeight();
                        if (orderWeight.compareTo(BigDecimal.ZERO) > 0) {

                            BigDecimal divide = orderWeight.divide(weight, 0, RoundingMode.UP);
                            BigDecimal multiply = divide.multiply(weight);

                            productOrder.setWeight(multiply);
                            productOrder.setPackWeight(multiply);

                            list.add(productOrder);
                        }
                    }
                }
            }
            productOrderRepo.saveAll(list);
            message = Message.EDIT_UZ;
        }

        return new StateMessage().parse(message);

    }

    public StateMessage send(Integer id, Users users) {
        Optional<MyOrder> optional = orderRepo.findById(id);
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optional.isPresent()) {
            MyOrder myOrder = optional.get();

            myOrder.setEdit(false);
            myOrder.setStatus(Status.SEND.getName());
            myOrder.setUpdatedBy(users);
            MyOrder order = orderRepo.save(myOrder);

            List<KindergartenOrder> kindergartenOrderList = order.getKindergartenOrderList();
            for (KindergartenOrder kindergartenOrder : kindergartenOrderList) {

                Integer kindergartenId = kindergartenOrder.getKindergarten().getId();
                Optional<ToSendProduct> optionalToSendProduct = toSendProductRepo.findByKindergarten_Id(kindergartenId);

                if (optionalToSendProduct.isPresent()) {
                    ToSendProduct toSendProduct = optionalToSendProduct.get();

                    List<ProductWeight> productWeight = toSendProduct.getProductWeight();

                    for (ProductOrder productOrder : kindergartenOrder.getProductOrders()) {
                        boolean res = true;
                        for (ProductWeight pWeight : productWeight) {

                            if (productOrder.getProduct().getId().equals(pWeight.getProduct().getId())) {
                                pWeight.setWeight(pWeight.getWeight().add(productOrder.getWeight()));
                                pWeight.setPackWeight(pWeight.getPackWeight().add(productOrder.getPackWeight()));
//                                productWeightRepo.save(pWeight);
                                res = false;
                            }

                        }
                        if (res) {
                            ProductWeight productWeightNew = new ProductWeight(productOrder.getProduct(), productOrder.getWeight(), productOrder.getPack(), productOrder.getPackWeight(), toSendProduct);
                            productWeight.add(productWeightNew);
                        }
                    }

                    toSendProduct.setProductWeight(productWeightRepo.saveAll(productWeight));
                    toSendProductRepo.save(toSendProduct);

                } else {

                    ToSendProduct toSendProduct = new ToSendProduct();
                    Kindergarten kindergarten = kindergartenOrder.getKindergarten();
                    toSendProduct.setKindergarten(kindergarten);
                    List<ProductWeight> productWeightList = new ArrayList<>();

                    for (ProductOrder productOrder : kindergartenOrder.getProductOrders()) {
                        ProductWeight productWeight = new ProductWeight(productOrder.getProduct(), productOrder.getWeight(), productOrder.getPack(), productOrder.getPackWeight(), toSendProduct);
                        productWeightList.add(productWeight);
                    }
                    toSendProduct.setProductWeight(productWeightList);
                    toSendProductRepo.save(toSendProduct);
                }
            }

            message = Message.SUCCESS_SEND;

        }

        return new StateMessage().parse(message);

    }
}
