package com.optimal.fergana.notification;

import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String message;

    private String sender;

    private String header;

    private String sendBy;

    private boolean read = false;

    @ManyToOne
    private Users users;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public Notification(String message, String sender, String header, String sendBy,Users users) {
        this.message = message;
        this.sender = sender;
        this.header = header;
        this.sendBy = sendBy;
        this.users = users;
    }
}
