package com.optimal.fergana.notification.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class KindergartenDTO {
    private Integer kindergartenId;
    private List<Integer> roleId;
}
