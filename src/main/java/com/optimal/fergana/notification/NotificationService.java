package com.optimal.fergana.notification;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.exception.NotificationNotFoundException;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.notification.dto.DepartmentDTO;
import com.optimal.fergana.notification.dto.KindergartenDTO;
import com.optimal.fergana.notification.dto.NotificationDTO;
import com.optimal.fergana.notification.dto.NotificationOneResDTO;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.role.Role;
import com.optimal.fergana.role.RoleRepo;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NotificationService {


    private final UserService userService;
    private final CustomConverter converter;
    private final NotificationRepo notificationRepo;
    private final UsersRepo usersRepo;



    public StateMessage add(HttpServletRequest request, NotificationDTO dto) {
        Users user = userService.parseToken(request);
        Integer departmentId = user.getDepartment().getId();

        List<Users> list = new ArrayList<>();

        if (user.getDepartment() != null) {
            list.addAll(usersRepo.findAllByDepartment_IdAndDeleteIsFalse(departmentId));
        } else {
            list.addAll(usersRepo.findAllByRegionalDepartment_IdAndDeleteIsFalse(user.getRegionalDepartment().getId()));
        }

        List<Notification> notificationList = new ArrayList<>();
        String sender = user.getName() + " " + user.getSurname() + " " + user.getFatherName();
        String sendBy = user.getDepartment() != null ? user.getDepartment().getName() : user.getRegionalDepartment().getName();

        for (Users users : list) {
            boolean res = false;
            for (Integer id : dto.getRoleIdList()) {
                for (Role role : users.getRoles()) {
                    if (role.getId().equals(id) && !res && (!users.getId().equals(user.getId()))) {
                        res = true;
                        notificationList.add(new Notification(dto.getMessage(), sender, dto.getHeader(), sendBy, users));
                    }
                }
                if (res) break;
            }
        }
        notificationRepo.saveAll(notificationList);

        return new StateMessage().parse(Message.SUCCESS_SEND);
    }

    public CustomPageable getAll(HttpServletRequest request, Integer page, Integer pageSize) {

        if (pageSize == null)
            pageSize = 20;
        if (page == null)
            page = 0;
        Pageable pageable = PageRequest.of(page, pageSize);
        Users user = userService.parseToken(request);

        Page<Notification> pages = notificationRepo.findAllByUsers_IdOrderByRead(user.getId(), pageable);
        return new CustomPageable(pages.getSize(), pages.getNumber(), converter.notificationToDTO(pages.stream().toList()), pages.getTotalPages());
    }

    public NotificationOneResDTO getOne(Integer id) {
        Optional<Notification> optionalNotification = notificationRepo.findById(id);
        if (optionalNotification.isPresent()) {
            Notification notification = optionalNotification.get();
            notification.setRead(true);
            Notification savedNotification = notificationRepo.save(notification);
            return new NotificationOneResDTO(
                    savedNotification.getHeader(),
                    savedNotification.isRead(),
                    savedNotification.getCreateDate(),
                    savedNotification.getSender(),
                    savedNotification.getMessage()
            );
        }
        throw new NotificationNotFoundException("bunday xabar topilmadi");
    }
}
