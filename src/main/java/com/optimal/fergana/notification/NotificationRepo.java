package com.optimal.fergana.notification;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepo extends JpaRepository<Notification, Integer> {
    Page<Notification> findAllByUsers_IdOrderByRead(Integer id, Pageable pageable);

    int countAllByReadIsFalseAndUsers_Id(Integer users_id);

}
