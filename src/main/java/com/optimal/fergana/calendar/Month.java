package com.optimal.fergana.calendar;

import com.sun.jdi.event.StepEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Month {
    private Integer year;
    private Integer month;
    private String name;
    private List<List<Day>> dayList;


    public Month(Integer year, Integer month, String name) {
        this.year = year;
        this.month = month;
        this.name = name;
    }
}
