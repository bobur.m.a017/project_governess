package com.optimal.fergana.calendar;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Day {
    private Integer day;
    private LocalDate localDate;
    private Date date;
    private boolean state;
    private boolean attached;
    private int attachedNumber;
    private int notAttachedNumber;
    private UUID reportId;
    private String menuName;


    public Day(Integer day, LocalDate localDate, Date date) {
        this.day = day;
        this.localDate = localDate;
        this.date = date;
    }
}
