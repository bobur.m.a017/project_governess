package com.optimal.fergana.calendar;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.*;


@RequiredArgsConstructor
@Service
public class CalendarService {

    private final ReportRepo reportRepo;
    private final KindergartenRepo kindergartenRepo;

    public Month get(Integer y, Integer m, Users users) {
        Kindergarten kindergarten = users.getKindergarten();

        YearMonth ym;
        int dayWeek = 0;

        if (y == null || m == null) {

            Date date = new Date(System.currentTimeMillis());
            date.setDate(1);
            ym = YearMonth.of(date.getYear() + 1900, date.getMonth() + 1);

            dayWeek = date.getDay();

            if (dayWeek != 0) {
                dayWeek = dayWeek - 1;
            } else {
                dayWeek = 6;
            }

        } else {
            ym = YearMonth.of(y, m);
//            Date date = new Date();
//            date.setYear(y);
//            date.setMonth(m);
            Date date = new Date(System.currentTimeMillis());
            date.setMonth(m - 1);
            date.setYear(ym.getYear() - 1900);
            date.setDate(1);
            dayWeek = date.getDay();
            if (dayWeek != 0) {
                dayWeek = dayWeek - 1;
            } else {
                dayWeek = 6;
            }
        }

        LocalDate firstOfMonth = ym.atDay(1);
        LocalDate firstOfFollowingMonth = ym.plusMonths(1).atDay(1);
        List<LocalDate> localDates = firstOfMonth.datesUntil(firstOfFollowingMonth).toList();

        int monthValue = localDates.get(0).getMonth().getValue();

        Month month = new Month(localDates.get(0).getYear(), monthValue, getMonthName(monthValue));

        List<Day> dayList = new ArrayList<>();

        for (int i = 0; i < dayWeek; i++) {
            dayList.add(new Day(0, null, null));
        }

        for (LocalDate localDate : localDates) {

            Day day = new Day(localDate.getDayOfMonth(), localDate, Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));

            dayList.add(day);
        }


        List<List<Day>> monthByWeeks = new ArrayList<>();
        List<Day> week = new ArrayList<>();
        for (int i = 0; i < dayList.size(); i++) {
            week.add(dayList.get(i));
            if ((i + 1) % 7 == 0) {
                monthByWeeks.add(week);
                week = new ArrayList<>();
            }
        }
        monthByWeeks.add(week);

        month.setDayList(monthByWeeks);
        return month;
    }

    public Month getCalendarAddMenu(Integer y, Integer m, Users users) {
        Month month = get(y, m, users);

        for (List<Day> dayList : month.getDayList()) {
            for (Day day : dayList) {
                if (day.getLocalDate() != null) {

                    int attached;
                    int all;

                    if (users.getKindergarten() != null) {
                        attached = reportRepo.countAllByYearAndMonthAndDayAndKindergarten_Id(day.getLocalDate().getYear(), day.getLocalDate().getMonthValue(), day.getLocalDate().getDayOfMonth(), users.getKindergarten().getId());
                        all = 1;
                    } else if (users.getDepartment() != null) {
                        attached = reportRepo.countAllByYearAndMonthAndDayAndKindergarten_Department_Id(day.getLocalDate().getYear(), day.getLocalDate().getMonthValue(), day.getLocalDate().getDayOfMonth(), users.getDepartment().getId());
                        all = kindergartenRepo.countAllByDepartment_IdAndDeleteIsFalseAndDeleteIsFalse(users.getDepartment().getId());
                    } else {
                        attached = reportRepo.countAllByYearAndMonthAndDayAndKindergarten_RegionalDepartment_Id(day.getLocalDate().getYear(), day.getLocalDate().getMonthValue(), day.getLocalDate().getDayOfMonth(), users.getRegionalDepartment().getId());
                        all = kindergartenRepo.countAllByRegionalDepartment_IdAndDeleteIsFalse(users.getRegionalDepartment().getId());
                    }


                    day.setAttached(attached > 0);
                    day.setAttachedNumber(attached);
                    day.setNotAttachedNumber(all - attached);
                    day.setState(day.getDate().getTime() > System.currentTimeMillis());
                }
            }
        }
        return month;
    }

    public Month byCalendarKindergarten(Integer id, LocalDate date, Users user) {

        Month month = get(date.getYear(), date.getMonthValue(), user);

        for (List<Day> dayList : month.getDayList()) {
            for (Day day : dayList) {
                Optional<Report> optionalReport = reportRepo.findByYearAndMonthAndDayAndKindergarten_Id(date.getYear(), date.getMonthValue(), day.getDay(), id);
                if (optionalReport.isPresent()) {
                    Report report = optionalReport.get();
                    if (report.getMenu() != null) {
                        day.setAttached(true);
                        day.setMenuName(report.getMenu().getMultiMenu().getName() + " \n " + report.getMenu().getName());
                    }
                    day.setReportId(report.getId());
                }
            }
        }
        return month;
    }


    public String getMonthName(int month) {

        String[] monthNames = {"", "Yanvar", "Fevral", "Mart", "Aprel", "May", "Iyun", "Iyul", "Avgust", "Sentabr", "Oktabr", "Noyabr", "Dekabr"};
        return monthNames[month];
    }
}
