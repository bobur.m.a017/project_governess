package com.optimal.fergana.warehouse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface WarehouseRepo extends JpaRepository<Warehouse, UUID> {

    Optional<Warehouse> findByDepartment_IdAndProduct_IdAndDeleteIsFalse(Integer departmentId, Integer product_id);

    Optional<Warehouse> findByProduct_Id(Integer product_id);

    Page<Warehouse> findAllByDepartment_IdAndDeleteIsFalse(Integer departmentId, Pageable pageable);


    Warehouse findByDepartment_IdAndDeleteIsFalseOrderByTotalWeight(Integer departmentId);
}
