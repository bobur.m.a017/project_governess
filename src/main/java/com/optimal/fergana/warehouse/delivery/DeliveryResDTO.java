package com.optimal.fergana.warehouse.delivery;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.order.toSendProduct.ProductWeightResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeliveryResDTO {
    private UUID id = UUID.randomUUID();

    private String deliver;
    private String status;
    private Timestamp date;
    private Integer kindergartenId;
    private String kindergartenName;
    private Integer kindergartenNumber;

    private List<DeliverySubDTO> deliverySubDTOList;
}
