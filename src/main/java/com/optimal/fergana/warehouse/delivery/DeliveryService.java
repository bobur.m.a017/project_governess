package com.optimal.fergana.warehouse.delivery;

import com.optimal.fergana.inputOutput.InputOutputService;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.ResponseMessage;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.order.toSendProduct.ProductWeight;
import com.optimal.fergana.order.toSendProduct.ProductWeightRepo;
import com.optimal.fergana.order.toSendProduct.ProductWeightResDTO;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.reporter.pdf.YukXati;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseRepo;
import com.optimal.fergana.warehouse.WarehouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DeliveryService {


    private final DeliveryRepo deliveryRepo;
    private final KindergartenRepo kindergartenRepo;
    private final ProductWeightRepo productWeightRepo;
    private final WarehouseRepo warehouseRepo;
    private final UserService userService;
    private final WarehouseService warehouseService;
    private final YukXati yukXati;
    private final ProductPackService productPackService;
    private final ProductRepo productRepo;
    private final InputOutputService inputOutputService;


    public ResponseMessage add(DeliveryDTO deliveryDTO, HttpServletRequest request) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        ResponseMessage responseMessage = new ResponseMessage(message.getName(), message.isState(), message.getCode(), new ArrayList<>());
        Users user = userService.parseToken(request);
        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(deliveryDTO.getKindergartenId());

        List<ProductWeight> productWeightList = new ArrayList<>();

        if (optionalKindergarten.isPresent()) {

            Kindergarten kindergarten = optionalKindergarten.get();

            StateMessage stateMessage = checkPack(deliveryDTO, user.getDepartment().getId());

            if (!stateMessage.isSuccess()) {
                return new ResponseMessage(stateMessage.getText(), false, stateMessage.getCode(), null);
            }

            List<ProductWeightResDTO> checklist = new ArrayList<>();
            List<ProductWeightResDTO> productWeightResDTOList = deliveryDTO.getProductWeightResDTOList();

            for (ProductWeightResDTO productWeightResDTO : productWeightResDTOList) {

                Optional<Warehouse> optionalWarehouse = warehouseRepo.findByDepartment_IdAndProduct_IdAndDeleteIsFalse(kindergarten.getDepartment().getId(), productWeightResDTO.getProductId());


                BigDecimal weight = productWeightResDTO.getPackWeight();
                BigDecimal weightPack = productWeightResDTO.getPackWeight();

                BigDecimal checkPack = productPackService.checkPack(user.getDepartment().getId(), productWeightResDTO.getProductId());

                if (checkPack != null) {
                    weight = (weightPack.multiply(checkPack)).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
                }

                if (optionalWarehouse.isPresent()) {

                    Warehouse warehouse = optionalWarehouse.get();
                    BigDecimal subtract = warehouse.getTotalWeight().subtract(weight);
                    if (subtract.compareTo(BigDecimal.ZERO) < 0) {
                        productWeightResDTO.setWeight(weight.subtract(warehouse.getTotalWeight()));
                        productWeightResDTO.setPackWeight(weightPack.subtract(warehouse.getTotalPackWeight()));
                        checklist.add(productWeightResDTO);
                    }

                } else {
                    checklist.add(productWeightResDTO);
                }


                Optional<ProductWeight> optionalProductWeight = productWeightRepo.findByProduct_IdAndToSendProduct_Kindergarten_Id(productWeightResDTO.getProductId(), kindergarten.getId());

                if (optionalProductWeight.isPresent()) {
                    ProductWeight productWeight = optionalProductWeight.get();
                    BigDecimal subtract1 = productWeight.getWeight().subtract(productWeightResDTO.getWeight());

                    if (subtract1.compareTo(BigDecimal.ZERO) < 0) {
                        checklist.add(productWeightResDTO);
                    } else {
                        productWeight.setWeight(productWeight.getWeight().subtract(weight));
                        productWeight.setPackWeight(productWeight.getPackWeight().subtract(weightPack));
                        productWeightList.add(productWeight);
                    }
                } else {
                    checklist.add(productWeightResDTO);
                }
            }

            if (checklist.size() > 0) {
                return new ResponseMessage("Kiritilgan maxsulotlar ombordagi mahsulotlar bilan mutanosib emas", false, 417, checklist);
            }


            Delivery delivery = new Delivery(deliveryDTO.getDeliver(), new Timestamp(deliveryDTO.getDate()).toLocalDateTime(), kindergarten, user);

            delivery.setDepartment(user.getDepartment());

            List<ProductDelivery> productDeliveryList = new ArrayList<>();

            for (ProductWeightResDTO productWeightResDTO : deliveryDTO.getProductWeightResDTOList()) {

                Integer productId = productWeightResDTO.getProductId();
                Product product = productRepo.findById(productId).get();

                Warehouse warehouse = warehouseRepo.findByDepartment_IdAndProduct_IdAndDeleteIsFalse(kindergarten.getDepartment().getId(), productId).get();


                BigDecimal weight = productWeightResDTO.getPackWeight();
                BigDecimal weightPack = productWeightResDTO.getPackWeight();
                BigDecimal pack = BigDecimal.ZERO;

                BigDecimal checkPack = productPackService.checkPack(user.getDepartment().getId(), productId);

                if (checkPack != null) {
                    weight = (weightPack.multiply(checkPack.divide(BigDecimal.valueOf(1000),6,RoundingMode.HALF_UP)));
                    pack = checkPack;
                }

                BigDecimal price = warehouseService.subtract(productWeightResDTO.getWeight(), weightPack, warehouse);


                inputOutputService.addOutputDepartment(product, user.getDepartment(), LocalDate.now(), weight, weightPack, pack);

                ProductDelivery productDelivery = new ProductDelivery(product, weight, weightPack, pack, delivery, price);

                productDeliveryList.add(productDelivery);
            }
            delivery.setProductDeliveryList(productDeliveryList);
            deliveryRepo.save(delivery);
            productWeightRepo.saveAll(productWeightList);

            return new ResponseMessage(Message.SUCCESS_SEND.getName(), Message.SUCCESS_SEND.isState(), Message.SUCCESS_SEND.getCode(), new ArrayList<>());
        }
        return responseMessage;
    }


    public List<DeliveryResDTO> getDeliveriesNEW(String status, HttpServletRequest request) {
        Users user = userService.parseToken(request);
        List<Delivery> deliveryList = deliveryRepo.findAllByStatusAndKindergarten_Id(status, user.getKindergarten().getId());


        List<DeliveryResDTO> deliveryResDTOList = new ArrayList<>();


        deliveryToDTO(deliveryList, deliveryResDTOList);
        return deliveryResDTOList;
    }


    public CustomPageable getDeliveriesNOTNEW(String status, HttpServletRequest request, Integer pageSize, Integer pageNumber) {
        Users user = userService.parseToken(request);

        List<DeliveryResDTO> deliveryResDTOList = new ArrayList<>();

        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);
        Page<Delivery> deliveryPage = deliveryRepo.findAllByStatusNotAndKindergarten_Id(status, user.getKindergarten().getId(), pageable);

        List<Delivery> deliveryList = deliveryPage.stream().toList();


        deliveryToDTO(deliveryList, deliveryResDTOList);
        CustomPageable customPageable = new CustomPageable(deliveryPage.getSize(), deliveryPage.getNumber(), deliveryResDTOList);
        customPageable.setAllPageSize(deliveryPage.getTotalPages());
        return customPageable;
    }


    public CustomPageable getDeliveriesByDate(String status, HttpServletRequest request, Integer MTTNumber, long startDate, long endDate, Integer pageSize, Integer pageNumber) {

        Users user = userService.parseToken(request);
        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);
        LocalDateTime date1 = new Timestamp(startDate).toLocalDateTime();
        LocalDateTime date2 = new Timestamp(endDate).toLocalDateTime();
        Page<Delivery> deliveryPage;
        deliveryPage = deliveryRepo.findAllByStatusAndUser_IdAndKindergarten_NumberAndDateBetween(status, user.getId(), MTTNumber, date1, date2, pageable);
        if (MTTNumber == null) {
            deliveryPage = deliveryRepo.findAllByStatusAndUser_IdAndDateBetween(status, user.getId(), date1, date2, pageable);

        }

        List<DeliveryResDTO> deliveryResDTOList = new ArrayList<>();


        if (status == null) {
            deliveryPage = deliveryRepo.findAllByDepartment_IdOrderByDateDesc(user.getDepartment().getId(), pageable);
        }

        List<Delivery> deliveryList = deliveryPage.stream().toList();

        deliveryToDTO(deliveryList, deliveryResDTOList);
        CustomPageable customPageable = new CustomPageable(deliveryPage.getSize(), deliveryPage.getNumber(), deliveryResDTOList);
        customPageable.setAllPageSize(deliveryPage.getTotalPages());
        return customPageable;
    }


    private void deliveryToDTO(List<Delivery> deliveryList, List<DeliveryResDTO> deliveryResDTOList) {
        for (Delivery delivery : deliveryList) {
            List<DeliverySubDTO> deliverySubDTOList = new ArrayList<>();
            for (ProductDelivery productDelivery : delivery.getProductDeliveryList()) {
                DeliverySubDTO deliverySubDTO = new DeliverySubDTO(
                        productDelivery.getId(),
                        productDelivery.getProduct().getId(),
                        productDelivery.getProduct().getName(),
                        productDelivery.getWeight(),
                        productDelivery.getPackWeight(),
                        productDelivery.getPackWeight(),
                        productDelivery.getKinWeight(),
                        productDelivery.getKinPackWeight(),
                        productDelivery.getRestWeight(),
                        productDelivery.getRestPackWeight(),
                        productDelivery.getWarWeight(),
                        productDelivery.getWarPackWeight(),
                        productDelivery.getPack(),
                        productDelivery.getStatus()
                );
                deliverySubDTOList.add(deliverySubDTO);
            }

            DeliveryResDTO deliveryResDTO = new DeliveryResDTO(
                    delivery.getId(),
                    delivery.getDeliver(),
                    delivery.getStatus(),
                    Timestamp.valueOf(delivery.getDate()),
                    delivery.getKindergarten().getId(),
                    delivery.getKindergarten().getName(),
                    delivery.getKindergarten().getNumber(),
                    deliverySubDTOList
            );
            deliveryResDTOList.add(deliveryResDTO);
        }
    }


    public String getFile(UUID id) {

        Optional<Delivery> optional = deliveryRepo.findById(id);

        if (optional.isPresent()) {
            Delivery delivery = optional.get();

            return yukXati.getFileProduct(delivery);
        }
        return null;
    }


    public StateMessage checkPack(DeliveryDTO deliveryDTO, Integer departmentId) {

        StringBuilder str = new StringBuilder("");

        int count = 0;

        for (ProductWeightResDTO dto : deliveryDTO.getProductWeightResDTOList()) {
            BigDecimal checkPack = productPackService.checkPack(departmentId, dto.getProductId());
            if (checkPack != null) {

                Product product = productRepo.findById(dto.getProductId()).get();

                BigDecimal[] bigDecimals = dto.getPackWeight().divideAndRemainder(BigDecimal.valueOf(1));
                if (bigDecimals[1].compareTo(BigDecimal.valueOf(0)) > 0) {

                    str.append(count > 0 ? "" : ", " + product.getName());

                    count++;
                }
            }
        }

        if (count > 0) {
            return new StateMessage(str.append(" \n ushbu maxsulotlar miqdorini donada kiriting").toString(), false, 417);
        }
        return new StateMessage("", true, 200);
    }
}
