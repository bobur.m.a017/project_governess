package com.optimal.fergana.warehouse.delivery;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Delivery {

    @Id
    private UUID id = UUID.randomUUID();
    private String deliver;
    private boolean delete = false;
    private String status = Status.SEND.getName();

    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.LAZY)
    private Kindergarten kindergarten;


    @ManyToOne(fetch = FetchType.LAZY)
    private Department department;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "delivery", cascade = CascadeType.ALL)
    private List<ProductDelivery> productDeliveryList;

    @ManyToOne(fetch = FetchType.LAZY)
    private Users user;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public Delivery(String deliver, LocalDateTime date, Kindergarten kindergarten,Users user) {
        this.deliver = deliver;
        this.date = date;
        this.kindergarten = kindergarten;
        this.user=user;
    }

}
