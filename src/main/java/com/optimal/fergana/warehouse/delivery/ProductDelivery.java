package com.optimal.fergana.warehouse.delivery;

import com.optimal.fergana.product.Product;
import com.optimal.fergana.status.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductDelivery {
    @Id
    private UUID id = UUID.randomUUID();

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal pack;


    @Column(precision = 19, scale = 6)
    private BigDecimal kinWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal kinPackWeight;


    @Column(precision = 19, scale = 6)
    private BigDecimal warWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal warPackWeight;


    @Column(precision = 19, scale = 6)
    private BigDecimal restWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal restPackWeight;


    @Column(precision = 19, scale = 6)
    private BigDecimal price;


    private String status = Status.NEW.getName();
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private Delivery delivery;

    @OneToOne
    private Product product;

    public ProductDelivery(Product product, BigDecimal weight, BigDecimal packWeight, BigDecimal pack, Delivery delivery, BigDecimal price) {
        this.product = product;
        this.weight = weight;
        this.packWeight = packWeight;
        this.pack = pack;
        this.delivery = delivery;
        this.price = price;
    }
}
