package com.optimal.fergana.warehouse.delivery;

import com.optimal.fergana.order.toSendProduct.ProductWeightResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeliveryDTO {
    private String deliver;
    private long date;
    private int kindergartenId;
    private List<ProductWeightResDTO> productWeightResDTOList;

    public DeliveryDTO(String deliver, long date, int kindergartenId) {
        this.deliver = deliver;
        this.date = date;
        this.kindergartenId = kindergartenId;
    }
}
