package com.optimal.fergana.warehouse.delivery;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProductDeliveryRepo extends JpaRepository<ProductDelivery, UUID> {
    int countAllByDelivery_IdAndStatus(UUID delivery_id, String status);
}
