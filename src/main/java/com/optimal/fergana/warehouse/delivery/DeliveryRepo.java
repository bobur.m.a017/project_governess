package com.optimal.fergana.warehouse.delivery;

import com.optimal.fergana.status.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface DeliveryRepo extends JpaRepository<Delivery, UUID> {

    List<Delivery> findAllByStatusAndKindergarten_Id(String status, Integer kindergarten_id);
    Page<Delivery> findAllByStatusNotAndKindergarten_Id(String status, Integer kindergarten_id, Pageable pageable);
    Page<Delivery> findAllByStatusAndUser_IdAndKindergarten_NumberAndDateBetween(String status, Integer user_id, Integer MTTNumber, LocalDateTime date1, LocalDateTime date2, Pageable pageable);
    Page<Delivery> findAllByStatusAndUser_IdAndDateBetween(String status,  Integer user_id, LocalDateTime date1, LocalDateTime date2, Pageable pageable);
    Page<Delivery> findAllByDepartment_IdOrderByDateDesc(Integer id,Pageable pageable);

}
