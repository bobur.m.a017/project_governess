package com.optimal.fergana.warehouse.inOut;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InOutPriceResDTO {

    private UUID id = UUID.randomUUID();
    private BigDecimal price;
    private BigDecimal weight;
    private BigDecimal pack;
    private BigDecimal packWeight;
    private Timestamp createDate;
    private Timestamp updateDate;
}
