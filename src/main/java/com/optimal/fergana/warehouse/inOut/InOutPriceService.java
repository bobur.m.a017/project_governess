package com.optimal.fergana.warehouse.inOut;


import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RequiredArgsConstructor
@Service
public class InOutPriceService {

    private final InOutPriceRepo inOutPriceRepo;
    private final WarehouseRepo warehouseRepo;


    public InOutPrice add(BigDecimal price, BigDecimal weight, Warehouse warehouse, BigDecimal pack, BigDecimal packWeight) {
        return inOutPriceRepo.save(new InOutPrice(price, weight, pack, packWeight, warehouse));
    }

    public BigDecimal subtract(BigDecimal weight, BigDecimal weightPack, Warehouse warehouse) {


        BigDecimal price = BigDecimal.ZERO;
        BigDecimal totalSum = BigDecimal.ZERO;
        BigDecimal totalWeight = BigDecimal.ZERO;

        List<InOutPrice> inOutPriceList = warehouse.getInOutPriceList();
        List<InOutPrice> delete = new ArrayList<>();
        inOutPriceList.sort(Comparator.comparing(InOutPrice::getCreateDate));

        for (InOutPrice inOutPrice : inOutPriceList) {
            if (inOutPrice.getWeight().compareTo(weight) > 0) {
                inOutPrice.setWeight(inOutPrice.getWeight().subtract(weight));
                inOutPrice.setPackWeight(inOutPrice.getPackWeight().subtract(weightPack));
                inOutPriceRepo.save(inOutPrice);

                warehouse.setTotalWeight(warehouse.getTotalWeight().subtract(weight));
                warehouse.setTotalPackWeight(warehouse.getTotalPackWeight().subtract(weightPack));

                totalSum = totalSum.add(weight.multiply(inOutPrice.getPrice()));
                totalWeight = totalWeight.add(weight);

            } else if (inOutPrice.getWeight().compareTo(weight) < 0) {

                delete.add(inOutPrice);
                weight = weight.subtract(inOutPrice.getWeight());
                weightPack = weightPack.subtract(inOutPrice.getPackWeight());

                warehouse.setTotalWeight(warehouse.getTotalWeight().subtract(inOutPrice.getWeight()));
                warehouse.setTotalPackWeight(warehouse.getTotalPackWeight().subtract(inOutPrice.getPackWeight()));

                totalSum = totalSum.add(inOutPrice.getWeight().multiply(inOutPrice.getPrice()));
                totalWeight = totalWeight.add(inOutPrice.getWeight());

            } else {

                delete.add(inOutPrice);
                warehouse.setTotalWeight(warehouse.getTotalWeight().subtract(weight));
                warehouse.setTotalPackWeight(warehouse.getTotalPackWeight().subtract(weightPack));

                totalSum = totalSum.add(weight.multiply(inOutPrice.getPrice()));
                totalWeight = totalWeight.add(weight);
            }
        }

        warehouseRepo.save(warehouse);
        inOutPriceRepo.deleteAll(delete);

        return price;
    }
}
