package com.optimal.fergana.warehouse.inOut;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.UUID;

public interface InOutPriceRepo extends JpaRepository<InOutPrice, UUID> {

    Page<InOutPrice> findFirst20ByWarehouse_IdAndWarehouse_Department_IdAndWarehouse_Delete(UUID warehouse_id, Integer warehouse_department_id, boolean warehouse_delete, Pageable pageable);
    Page<InOutPrice> findAllByWarehouse_IdAndWarehouse_Department_IdAndWarehouse_DeleteAndCreateDateBetween(UUID warehouse_id, Integer warehouse_department_id, boolean warehouse_delete, Timestamp createDate1, Timestamp createDate2, Pageable pageable);

}
