package com.optimal.fergana.warehouse;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.warehouse.inOut.InOutPrice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Warehouse {

    @Id
    private UUID id = UUID.randomUUID();

    @ManyToOne(fetch = FetchType.LAZY)
    private Department department;

    private boolean delete = false;

    @Column(precision = 19,scale = 6)
    private BigDecimal totalWeight;

    @Column(precision = 19,scale = 6)
    private BigDecimal totalPackWeight;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @OneToMany(mappedBy = "warehouse", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InOutPrice> inOutPriceList;



    public Warehouse(Department department, BigDecimal totalWeight, BigDecimal totalPackWeight, Product product) {
        this.department = department;
        this.totalWeight = totalWeight;
        this.totalPackWeight = totalPackWeight;
        this.product = product;
    }

//    public BigDecimal getTotalWeight() {
//        BigDecimal weight = BigDecimal.valueOf(0);
//        for (InOutPrice inOutPrice : this.inOutPriceList) {
//            weight = weight.add(inOutPrice.getWeight());
//        }
//        return weight;
//    }
}
