package com.optimal.fergana.warehouse;


import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.inputOutput.InputOutputService;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.acceptedProducts.AcceptedProduct;
import com.optimal.fergana.product.acceptedProducts.AcceptedProductRepo;
import com.optimal.fergana.product.acceptedProducts.AcceptedProductService;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.reporter.excel.WarehouseExcel;
import com.optimal.fergana.reporter.pdf.ProductStoreReportByMTT;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.supplier.SupplierRepo;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.delivery.*;
import com.optimal.fergana.warehouse.inOut.InOutPrice;
import com.optimal.fergana.warehouse.inOut.InOutPriceRepo;
import com.optimal.fergana.warehouse.inOut.InOutPriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;

@RequiredArgsConstructor
@Service
public class WarehouseService {

    private final WarehouseRepo warehouseRepo;
    private final CustomConverter converter;
    private final InOutPriceRepo inOutPriceRepo;
    private final InOutPriceService inOutPriceService;
    private final ProductStoreReportByMTT productStoreReportByMTT;
    private final ProductRepo productRepo;
    private final AcceptedProductService acceptedProductService;
    private final SupplierRepo supplierRepo;
    private final InputOutputService inputOutputService;
    private final UserService userService;
    private final DeliveryRepo deliveryRepo;
    private final ProductPackService productPackService;
    private final AcceptedProductRepo acceptedProductRepo;
    private final ProductDeliveryRepo productDeliveryRepo;
    private final WarehouseExcel warehouseExcel;
    private DepartmentRepo departmentRepo;


    public StateMessage add(Integer productId, BigDecimal weightPack, BigDecimal price, Users users, Integer supplierId) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Department department = users.getDepartment();

        Optional<Product> optionalProduct = productRepo.findById(productId);

        Optional<Supplier> optionalSupplier = supplierRepo.findById(supplierId);


        Optional<Warehouse> optionalWarehouse = warehouseRepo.findByDepartment_IdAndProduct_IdAndDeleteIsFalse(department.getId(), productId);
        BigDecimal weight = weightPack;
        BigDecimal pack = BigDecimal.ZERO;

        if (optionalProduct.isPresent() && optionalSupplier.isPresent() && (weightPack.compareTo(BigDecimal.valueOf(0)) > 0)) {

            Product product = optionalProduct.get();
            Supplier supplier = optionalSupplier.get();

            BigDecimal checkPack = productPackService.checkPack(users.getDepartment().getId(), productId);

            if (checkPack != null) {
                BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.valueOf(1));
                if (bigDecimals[1].compareTo(BigDecimal.valueOf(0)) > 0) {
                    return new StateMessage("Ushbu maxsulot miqdorini donada kiriting", false, 417);
                }
                weight = (weightPack.multiply(checkPack)).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
                pack = checkPack;
            }


            if (optionalWarehouse.isPresent()) {

                Warehouse warehouse = optionalWarehouse.get();
                warehouse.setTotalPackWeight(warehouse.getTotalPackWeight().add(weightPack));
                warehouse.setTotalWeight(warehouse.getTotalWeight().add(weight));
                warehouseRepo.save(warehouse);

                inOutPriceService.add(price, weight, warehouse, pack, weightPack);
                acceptedProductService.add(supplier, product, price, weight, weightPack, pack, users);
                inputOutputService.addInputDepartment(product, users.getDepartment(), LocalDate.now(), weight, weightPack, pack);

                message = Message.SUCCESS_UZ;
            } else {

                Warehouse warehouse = warehouseRepo.save(new Warehouse(department, weight, weightPack, product));

                inOutPriceService.add(price, weight, warehouse, pack, weightPack);
                acceptedProductService.add(supplier, product, price, weight, weightPack, pack, users);
                inputOutputService.addInputDepartment(product, users.getDepartment(), LocalDate.now(), weight, weightPack, pack);

                message = Message.SUCCESS_UZ;

            }
        }
        return new StateMessage().parse(message);
    }

    public BigDecimal subtract(BigDecimal weight, BigDecimal weightPack, Warehouse warehouse) {
        return inOutPriceService.subtract(weight, weightPack, warehouse);
    }

    public CustomPageable getAll(Users user, Integer departmentId, Integer pageSize, Integer pageNumber) {

        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);

        if (user.getDepartment() != null) {
            departmentId = user.getDepartment().getId();
        }

        Page<Warehouse> page = warehouseRepo.findAllByDepartment_IdAndDeleteIsFalse(departmentId, pageable);

        List<WarehouseResDTO> list = converter.warehouseToDTO(page.stream().toList());
        list.sort(Comparator.comparing(WarehouseResDTO::getProductName));


        CustomPageable customPageable = new CustomPageable(page.getSize(), page.getNumber(), list);
        customPageable.setAllPageSize(page.getTotalPages());
        return customPageable;
    }

    public CustomPageable getAll(HttpServletRequest request, Long startDate, Long endDate, String id, Integer departmentId, Integer pageSize, Integer pageNumber) {
        Users user = userService.parseToken(request);
        UUID uuidId = UUID.fromString(id);
        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);
        Page<InOutPrice> inOutPricePage;

        if (user.getDepartment() != null) {
            departmentId = user.getDepartment().getId();
        }

        if (startDate == null || endDate == null) {

            inOutPricePage = inOutPriceRepo.findFirst20ByWarehouse_IdAndWarehouse_Department_IdAndWarehouse_Delete(uuidId, departmentId, false, pageable);
        } else {
            Timestamp date1 = new Timestamp(startDate);
            Timestamp date2 = new Timestamp(endDate);
            inOutPricePage = inOutPriceRepo.findAllByWarehouse_IdAndWarehouse_Department_IdAndWarehouse_DeleteAndCreateDateBetween(uuidId, departmentId, false, date1, date2, pageable);
        }

        CustomPageable customPageable = new CustomPageable(inOutPricePage.getSize(), inOutPricePage.getNumber(), inOutPricePage.stream().toList());
        customPageable.setAllPageSize(inOutPricePage.getTotalPages());
        return customPageable;
    }

    public String getFile(Users users) {

        if (users.getDepartment() != null) {
            return productStoreReportByMTT.createPDFOmbor(users.getDepartment());
        }
        return null;
    }

    public String getFileExcel(Users users) throws IOException {

        if (users.getDepartment() != null) {
            return warehouseExcel.getFile(users.getDepartment());
        }
        return null;
    }

    public StateMessage acceptedProducts(HttpServletRequest request, DeliverySubDTO deliverySubDTO, UUID deliveryId) {

        Users user = userService.parseToken(request);
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Delivery> optionalDelivery = deliveryRepo.findById(deliveryId);

        if (optionalDelivery.isPresent()) {

            Delivery delivery = optionalDelivery.get();
            List<ProductDelivery> productDeliveryList = delivery.getProductDeliveryList();

            for (ProductDelivery productDelivery : productDeliveryList) {

                BigDecimal subtract = productDelivery.getRestWeight().subtract(deliverySubDTO.getWeight());

                if (productDelivery.getRestWeight().compareTo(BigDecimal.ZERO) > 0 && subtract.compareTo(BigDecimal.ZERO) >= 0) {

                    BigDecimal checkPack = productPackService.checkPack(user.getDepartment().getId(), productDelivery.getProduct().getId());

                    BigDecimal weight = deliverySubDTO.getPackWeight();
                    BigDecimal weightPack = deliverySubDTO.getPackWeight();
                    BigDecimal pack = BigDecimal.ZERO;

                    if (checkPack != null) {
                        BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.valueOf(1));
                        if (bigDecimals[1].compareTo(BigDecimal.valueOf(0)) > 0) {
                            return new StateMessage("Ushbu maxsulot miqdorini donada kiriting", false, 417);
                        }
                        weight = (weightPack.multiply(checkPack)).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
                        pack = checkPack;
                    }


                    productDelivery.setWarWeight(weight);
                    productDelivery.setWarPackWeight(weightPack);
                    productDelivery.setRestWeight(subtract);
                    productDelivery.setRestPackWeight(productDelivery.getRestPackWeight().subtract(deliverySubDTO.getPackWeight()));
                    productDeliveryRepo.save(productDelivery);

                    Optional<Warehouse> optionalWarehouse = warehouseRepo.findByDepartment_IdAndProduct_IdAndDeleteIsFalse(user.getDepartment().getId(), deliverySubDTO.getProductId());

                    if (optionalWarehouse.isPresent()) {

                        Warehouse warehouse = optionalWarehouse.get();
                        BigDecimal restByDate = warehouse.getTotalWeight().add(deliverySubDTO.getWeight());

                        acceptedProductRepo.save(new AcceptedProduct(weight, weightPack, pack, productDelivery.getPrice(), new Date(System.currentTimeMillis()), user, null, Status.NEW.getName(), productDelivery.getProduct(), user.getDepartment(), restByDate));

//                        Warehouse warehouse = optionalWarehouse.get();
//                        warehouse.setTotalWeight(deliverySubDTO.getWeight());
//                        warehouse.setTotalPackWeight(deliverySubDTO.getPackWeight());
//                        warehouseRepo.save(warehouse);

                        message = Message.SUCCESS_UZ;

                    }
                }
            }
        }

        return new StateMessage().parse(message);

    }
}