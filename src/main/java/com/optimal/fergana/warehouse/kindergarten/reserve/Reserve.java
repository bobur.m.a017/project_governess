package com.optimal.fergana.warehouse.kindergarten.reserve;


import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Reserve {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Kindergarten kindergarten;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight = BigDecimal.ZERO;

    @Column(precision = 19, scale = 6)
    private BigDecimal pack= BigDecimal.ZERO;

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight= BigDecimal.ZERO;

    @OneToMany(mappedBy = "reserve", cascade = CascadeType.ALL)
    private List<AmountSpent> amountSpentList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    public Reserve(Kindergarten kindergarten, BigDecimal weight, BigDecimal pack, BigDecimal packWeight, Product product) {
        this.kindergarten = kindergarten;
        this.weight = weight;
        this.pack = pack;
        this.packWeight = packWeight;
        this.product = product;
    }
}
