package com.optimal.fergana.warehouse.kindergarten.reserve;


import com.optimal.fergana.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AmountSpent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    @Column(precision = 19, scale = 6)
    private BigDecimal pack;

    @Column(precision = 19, scale = 6)
    private BigDecimal packWeight;

    private LocalDate date;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private Reserve reserve;

    public AmountSpent(BigDecimal weight, BigDecimal pack, BigDecimal packWeight, LocalDate date, Product product, Reserve reserve) {
        this.weight = weight;
        this.pack = pack;
        this.packWeight = packWeight;
        this.date = date;
        this.product = product;
        this.reserve = reserve;
    }
}
