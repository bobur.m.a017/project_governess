package com.optimal.fergana.warehouse.kindergarten;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KindergartenWarehouse {

    @Id
    private UUID id = UUID.randomUUID();

    @ManyToOne
    private Kindergarten kindergarten;

    private boolean delete = false;

    @Column(precision = 19, scale = 6)
    private BigDecimal totalWeight;

    @Column(precision = 19, scale = 6)
    private BigDecimal pack;

    @Column(precision = 19, scale = 6)
    private BigDecimal totalPackWeight;

    private BigDecimal restByDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;


    public KindergartenWarehouse(Kindergarten kindergarten, BigDecimal totalWeight, BigDecimal pack, BigDecimal totalPackWeight, Product product,BigDecimal restByDate) {
        this.kindergarten = kindergarten;
        this.totalWeight = totalWeight;
        this.pack = pack;
        this.totalPackWeight = totalPackWeight;
        this.product = product;
        this.restByDate = restByDate;
    }
    public KindergartenWarehouse(Kindergarten kindergarten, BigDecimal totalWeight, BigDecimal pack, BigDecimal totalPackWeight, Product product) {
        this.kindergarten = kindergarten;
        this.totalWeight = totalWeight;
        this.pack = pack;
        this.totalPackWeight = totalPackWeight;
        this.product = product;
    }

    public KindergartenWarehouse(BigDecimal totalWeight, BigDecimal totalPackWeight) {
        this.totalWeight = totalWeight;
        this.totalPackWeight = totalPackWeight;
    }
}
