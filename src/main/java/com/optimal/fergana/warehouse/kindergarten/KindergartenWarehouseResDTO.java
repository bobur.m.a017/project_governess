package com.optimal.fergana.warehouse.kindergarten;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KindergartenWarehouseResDTO {

    private UUID id;

    private Integer productId;

    private String productName;

    private BigDecimal weight;

    private BigDecimal packWeight;

    private BigDecimal pack;
}
