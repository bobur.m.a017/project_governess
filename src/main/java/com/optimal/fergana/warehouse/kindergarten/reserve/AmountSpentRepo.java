package com.optimal.fergana.warehouse.kindergarten.reserve;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface AmountSpentRepo extends JpaRepository<AmountSpent,Integer> {

    Optional<AmountSpent> findAllByDateAndReserve_Id(LocalDate date, Integer reserve_id);
    List<AmountSpent> findAllByDateBetweenAndReserve_Product_IdAndReserve_Kindergarten_Id(LocalDate date, LocalDate date2, Integer product_id, Integer kindergarten_id);
    Optional<AmountSpent> findByDateAndReserve_Product_IdAndReserve_Kindergarten_Id(LocalDate date, Integer reserve_product_id, Integer reserve_kindergarten_id);
}
