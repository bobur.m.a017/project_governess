package com.optimal.fergana.warehouse.kindergarten;


import com.optimal.fergana.inputOutput.InputOutput;
import com.optimal.fergana.inputOutput.InputOutputRepo;
import com.optimal.fergana.inputOutput.InputOutputService;
import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.inputOutput.inOut.InOutRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.reporter.excel.KindergartenWarehouseExcel;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.delivery.*;
import com.optimal.fergana.warehouse.kindergarten.reserve.ReserveService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;


@Service
@RequiredArgsConstructor
public class KindergartenWarehouseService {


    private final KindergartenWarehouseRepo kindergartenWarehouseRepo;
    private final DeliveryRepo deliveryRepo;
    private final ProductDeliveryRepo productDeliveryRepo;
    private final ProductRepo productRepo;
    private final InputOutputService inputOutputService;
    private final ProductPackService productPackService;
    private final InputOutputRepo inputOutputRepo;
    private final InOutRepo inOutRepo;
    private final ReserveService reserveService;
    private final KindergartenWarehouseExcel kindergartenWarehouseExcel;
    private final KindergartenRepo kindergartenRepo;


    public StateMessage add(Integer productId, BigDecimal weightPack, Users users) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Product> optionalProduct = productRepo.findById(productId);

        if (optionalProduct.isPresent() && users.getKindergarten() != null && weightPack.compareTo(BigDecimal.ZERO) > 0) {

            Kindergarten kindergarten = users.getKindergarten();
            Product product = optionalProduct.get();

            BigDecimal weight = weightPack;

            BigDecimal pack = productPackService.checkPack(users.getDepartment().getId(), productId);

            if (pack != null) {
                BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.valueOf(1));
                if (bigDecimals[1].compareTo(BigDecimal.valueOf(0)) > 0) {
                    return new StateMessage("Ushbu maxsulot miqdorini donada kiriting", false, 417);
                }
                weight = weightPack.multiply(pack).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
            }

            Optional<KindergartenWarehouse> optionalKindergartenWarehouse = kindergartenWarehouseRepo.findByKindergarten_IdAndProduct_Id(users.getKindergarten().getId(), productId);

            KindergartenWarehouse kindergartenWarehouse;

            kindergartenWarehouse = optionalKindergartenWarehouse.orElseGet(() -> new KindergartenWarehouse(
                    users.getKindergarten(),
                    BigDecimal.ZERO,
                    pack,
                    BigDecimal.ZERO,
                    product,
                    BigDecimal.ZERO
            ));

            kindergartenWarehouse.setTotalWeight(kindergartenWarehouse.getTotalWeight().add(weight));
            kindergartenWarehouse.setTotalPackWeight(kindergartenWarehouse.getTotalPackWeight().add(weightPack));

            inputOutputService.addInputKindergarten(kindergartenWarehouse.getProduct(), kindergartenWarehouse.getKindergarten(), LocalDate.now(), weight, weightPack, pack);

            kindergartenWarehouseRepo.save(kindergartenWarehouse);

            reserveService.add(product, kindergarten, weight, weightPack, pack);

            message = Message.SUCCESS_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage addOutPut(Integer productId, BigDecimal weightPack, Users users) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Product> optionalProduct = productRepo.findById(productId);

        if (optionalProduct.isPresent() && users.getKindergarten() != null && weightPack.compareTo(BigDecimal.ZERO) > 0) {

            Kindergarten kindergarten = users.getKindergarten();
            Product product = optionalProduct.get();

            Optional<KindergartenWarehouse> optional = kindergartenWarehouseRepo.findByKindergarten_IdAndProduct_Id(kindergarten.getId(), product.getId());

            if (optional.isPresent()) {
                BigDecimal weight = weightPack;

                BigDecimal pack = productPackService.checkPack(users.getDepartment().getId(), productId);

                if (pack != null) {
                    BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.valueOf(1));
                    if (bigDecimals[1].compareTo(BigDecimal.valueOf(0)) > 0) {
                        return new StateMessage("Ushbu maxsulot miqdorini donada kiriting", false, 417);
                    }
                    weight = weightPack.multiply(pack).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
                }


                boolean res = true;
                Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByKindergarten_IdAndLocalDate(kindergarten.getId(), LocalDate.now());

                if (inputOutputOptional.isPresent()) {
                    InputOutput inputOutput = inputOutputOptional.get();
                    Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());
                    if (optionalInOut.isPresent()) {
                        InOut inOut = optionalInOut.get();
                        if (inOut.getOutputPackWeight().compareTo(weightPack) > 0) {

                            BigDecimal subtractWeight = inOut.getOutputWeight().subtract(weight);
                            BigDecimal subtractWeightPack = inOut.getOutputPackWeight().subtract(weightPack);

                            inOut.setOutputWeight(weight);
                            inOut.setOutputPackWeight(weightPack);

                            subtract(product, subtractWeight, subtractWeightPack, kindergarten, LocalDate.now(), false);

                            message = Message.SUCCESS_UZ;

                            res = false;
                        } else {
                            return new StateMessage("Siz menyu bo`yicha chiqim qilingan miqdordan ko`p miqdor kirita olmaysiz", false, 417);
                        }
                    }
                }
                if (res) {
                    return new StateMessage("Bugungi bolalar soni MTT mudirasi tomonidan xali tasdiqlanmadi.", false, 417);
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public void subtract(Product product, BigDecimal weight, BigDecimal weightPack, Kindergarten kindergarten, LocalDate date, boolean result) {

        Optional<KindergartenWarehouse> optional = kindergartenWarehouseRepo.findByKindergarten_IdAndProduct_Id(kindergarten.getId(), product.getId());

        if (optional.isPresent()) {
            KindergartenWarehouse kindergartenWarehouse = optional.get();
            kindergartenWarehouse.setTotalWeight(kindergartenWarehouse.getTotalWeight().subtract(weight));
            kindergartenWarehouse.setTotalPackWeight(kindergartenWarehouse.getTotalPackWeight().subtract(weightPack));

            kindergartenWarehouseRepo.save(kindergartenWarehouse);

            boolean res = reserveService.checkReserve(product, kindergarten);

            if (res) {
                reserveService.subtract(product, kindergarten, weight, weightPack, kindergartenWarehouse.getPack(), date, result);
            }
        }
    }

    public StateMessage acceptedProducts(DeliverySubDTO deliverySubDTO, Users user) {

        Integer kindergartenId = user.getKindergarten().getId();
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;


        Optional<ProductDelivery> optionalProductDelivery = productDeliveryRepo.findById(deliverySubDTO.getId());

        if (optionalProductDelivery.isPresent()) {

            ProductDelivery productDelivery = optionalProductDelivery.get();
            Delivery delivery = productDelivery.getDelivery();


            BigDecimal weightPack = deliverySubDTO.getPackWeight();
            BigDecimal weight = weightPack;
            BigDecimal pack = BigDecimal.ZERO;


            if (weightPack.compareTo(BigDecimal.ZERO) >= 0) {

                BigDecimal checkPack = productPackService.checkPack(user.getDepartment().getId(), productDelivery.getProduct().getId());

                if (checkPack != null) {
                    BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.valueOf(1));
                    if (bigDecimals[1].compareTo(BigDecimal.valueOf(0)) > 0) {
                        return new StateMessage("Ushbu maxsulot miqdorini donada kiriting", false, 417);
                    }
                    weight = weightPack.multiply(checkPack).divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
                    pack = checkPack;
                }

                if (productDelivery.getWeight().subtract(weight).compareTo(BigDecimal.ZERO) >= 0) {

                    productDelivery.setRestWeight(productDelivery.getWeight().subtract(weight));
                    productDelivery.setRestPackWeight(productDelivery.getPackWeight().subtract(weightPack));

                } else {
                    return new StateMessage("Yuqori miqdor kiritdingiz", false, 417);
                }


                Optional<KindergartenWarehouse> optionalKindergartenWarehouse = kindergartenWarehouseRepo.findByKindergarten_IdAndProduct_Id(kindergartenId, deliverySubDTO.getProductId());

                KindergartenWarehouse kindergartenWarehouse;

                if (optionalKindergartenWarehouse.isEmpty()) {

                    BigDecimal kinWeight = productDelivery.getKinWeight() != null ? productDelivery.getKinWeight() : BigDecimal.ZERO;

                    BigDecimal restByDate = kinWeight.add(weight);

                    kindergartenWarehouse = new KindergartenWarehouse(
                            user.getKindergarten(),
                            BigDecimal.ZERO,
                            pack,
                            BigDecimal.ZERO,
                            productDelivery.getProduct(),
                            restByDate
                    );
                } else {
                    kindergartenWarehouse = optionalKindergartenWarehouse.get();
                }

                productDelivery.setKinWeight(weight);
                productDelivery.setKinPackWeight(weightPack);


                kindergartenWarehouse.setTotalWeight(kindergartenWarehouse.getTotalWeight().add(weight));
                kindergartenWarehouse.setTotalPackWeight(kindergartenWarehouse.getTotalPackWeight().add(weightPack));

                inputOutputService.addInputKindergarten(kindergartenWarehouse.getProduct(), kindergartenWarehouse.getKindergarten(), LocalDate.now(), weight, weightPack, pack);

                kindergartenWarehouseRepo.save(kindergartenWarehouse);

                if (productDelivery.getWeight().subtract(weight).compareTo(BigDecimal.ZERO) == 0) {

                    productDelivery.setStatus(Status.TOLIQ_YETKAZIB_BERILDI.getName());
                    message = Message.FULLY_ACCEPTED;

                } else {

                    productDelivery.setStatus(Status.PARTIALLY_ACCEPTED.getName());
                    message = Message.PARTIALLY_ACCEPTED;

                }
                productDeliveryRepo.save(productDelivery);
                int countAll = productDeliveryRepo.countAllByDelivery_IdAndStatus(delivery.getId(), Status.NEW.getName());
                int count = productDeliveryRepo.countAllByDelivery_IdAndStatus(delivery.getId(), Status.PARTIALLY_ACCEPTED.getName());

                if (countAll == 0) {
                    if (count > 0) {
                        delivery.setStatus(Status.PARTIALLY_ACCEPTED.getName());
                    } else {
                        delivery.setStatus(Status.TOLIQ_YETKAZIB_BERILDI.getName());
                    }
                }
                deliveryRepo.save(delivery);
            }

        }
        return new StateMessage().parse(message);
    }

    public CustomPageable getAll(Integer kindergartenId, Integer pageSize, Integer pageNumber, Users user) {

        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);
        if (kindergartenId == null) {
            kindergartenId = user.getKindergarten().getId();
        }

        Page<KindergartenWarehouse> kindergartenWarehousePage = kindergartenWarehouseRepo.findAllByKindergarten_IdAndTotalPackWeightBetweenOrderByProduct_Name(kindergartenId, BigDecimal.valueOf(0.001), BigDecimal.valueOf(50000), pageable);
        List<KindergartenWarehouse> kindergartenWarehouseList = kindergartenWarehousePage.stream().toList();
        List<KindergartenWarehouseResDTO> kindergartenWarehouseResDTOList = new ArrayList<>();

        for (KindergartenWarehouse kindergartenWarehouse : kindergartenWarehouseList) {
//            if (kindergartenWarehouse.getTotalWeight().compareTo(BigDecimal.ZERO) > 0) {
            KindergartenWarehouseResDTO kindergartenWarehouseResDTO = new KindergartenWarehouseResDTO(
                    kindergartenWarehouse.getId(),
                    kindergartenWarehouse.getProduct().getId(),
                    kindergartenWarehouse.getProduct().getName(),
                    kindergartenWarehouse.getTotalWeight(),
                    kindergartenWarehouse.getTotalPackWeight(),
                    kindergartenWarehouse.getPack()
            );
            kindergartenWarehouseResDTOList.add(kindergartenWarehouseResDTO);
//            }
        }
        //        customPageable.setAllPageSize(kindergartenWarehousePage.getTotalPages());
        return new CustomPageable(kindergartenWarehousePage.getSize(), kindergartenWarehousePage.getNumber(), kindergartenWarehouseResDTOList, kindergartenWarehousePage.getTotalElements());
    }

    public StateMessage getFile(Users users, Integer kindergartenId) {
        Kindergarten kindergarten = null;

        if (users.getKindergarten() != null) {
            kindergarten = users.getKindergarten();
        } else {
            Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(kindergartenId);
            if (optionalKindergarten.isPresent()) {
                kindergarten = optionalKindergarten.get();
            }
        }
        try {

            if (kindergarten != null) {
                String file = kindergartenWarehouseExcel.getFile(kindergarten);
                String s = StaticWords.URI + file;

                return new StateMessage(s, true, 200);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new StateMessage("Xatolik", false, 417);
        }
        return new StateMessage("MTT topilmadi", false, 417);
    }
}