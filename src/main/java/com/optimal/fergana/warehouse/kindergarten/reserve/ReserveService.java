package com.optimal.fergana.warehouse.kindergarten.reserve;


import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.product.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReserveService {

    private final ReserveRepo reserveRepo;
    private final AmountSpentRepo amountSpentRepo;

    public void add(Product product, Kindergarten kindergarten, BigDecimal weight, BigDecimal packWeight, BigDecimal pack) {

        Optional<Reserve> optionalReserve = reserveRepo.findByKindergarten_IdAndProduct_Id(kindergarten.getId(), product.getId());
        if (optionalReserve.isPresent()) {
            Reserve reserve = optionalReserve.get();
            reserve.setWeight(reserve.getWeight().add(weight));
            reserve.setPackWeight(reserve.getPackWeight().add(packWeight));
            reserveRepo.save(reserve);
        } else {
            reserveRepo.save(new Reserve(kindergarten, weight, pack, packWeight, product));
        }
    }


    public void subtract(Product product, Kindergarten kindergarten, BigDecimal weight, BigDecimal packWeight, BigDecimal pack, LocalDate date, boolean result) {

        Optional<Reserve> optionalReserve = reserveRepo.findByKindergarten_IdAndProduct_Id(kindergarten.getId(), product.getId());

        if (optionalReserve.isPresent()) {

            Reserve reserve = optionalReserve.get();

            BigDecimal weightSub = BigDecimal.ZERO;
            BigDecimal packWeightSub = BigDecimal.ZERO;

            if (reserve.getPackWeight().compareTo(packWeight) >= 0) {
                reserve.setWeight(reserve.getWeight().subtract(weight));
                reserve.setPackWeight(reserve.getPackWeight().subtract(packWeight));

                weightSub = weight;
                packWeightSub = packWeight;
            } else {
                weightSub = reserve.getWeight();
                packWeightSub = reserve.getPackWeight();

                reserve.setPackWeight(BigDecimal.ZERO);
                reserve.setWeight(BigDecimal.ZERO);
            }

            if (result){
                Optional<AmountSpent> optional = amountSpentRepo.findAllByDateAndReserve_Id(date, reserve.getId());

                if (optional.isPresent()) {
                    AmountSpent amountSpent = optional.get();

                    amountSpent.setWeight(amountSpent.getWeight().add(weightSub));
                    amountSpent.setPackWeight(amountSpent.getPackWeight().add(packWeightSub));
                    amountSpentRepo.save(amountSpent);
                } else {
                    List<AmountSpent> amountSpentList = reserve.getAmountSpentList();
                    amountSpentList.add(new AmountSpent(weightSub, pack, packWeightSub, date, product, reserve));
                    reserve.setAmountSpentList(amountSpentList);
                }
            }

            reserveRepo.save(reserve);
        }
    }


    public boolean checkReserve(Product product, Kindergarten kindergarten) {
        Optional<Reserve> optional = reserveRepo.findByKindergarten_IdAndProduct_Id(kindergarten.getId(), product.getId());

        if (optional.isPresent()) {
            Reserve reserve = optional.get();
            return reserve.getPackWeight().compareTo(BigDecimal.ZERO) > 0;
        }

        return false;
    }

}
