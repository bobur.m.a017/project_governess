package com.optimal.fergana.warehouse.kindergarten.reserve;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ReserveRepo extends JpaRepository<Reserve, Integer> {

    Optional<Reserve> findByKindergarten_IdAndProduct_Id(Integer kindergarten_id, Integer product_id);
}
