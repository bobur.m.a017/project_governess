package com.optimal.fergana.warehouse.kindergarten;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface KindergartenWarehouseRepo extends JpaRepository<KindergartenWarehouse, UUID> {

    Optional<KindergartenWarehouse> findByKindergarten_IdAndProduct_Id(Integer kindergarten_id, Integer product_id);
    List<KindergartenWarehouse> findAllByKindergarten_Id(Integer kindergarten_id);
    Page<KindergartenWarehouse> findAllByKindergarten_Id(Integer kindergarten_id, Pageable pageable);
    Page<KindergartenWarehouse> findAllByKindergarten_IdAndTotalPackWeightBetween(Integer kindergarten_id, BigDecimal totalPackWeight, BigDecimal totalPackWeight2, Pageable pageable);
    Page<KindergartenWarehouse> findAllByKindergarten_IdAndTotalPackWeightBetweenOrderByProduct_Name(Integer kindergarten_id, BigDecimal totalPackWeight, BigDecimal totalPackWeight2, Pageable pageable);
}
