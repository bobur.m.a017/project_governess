package com.optimal.fergana.waste;


import com.optimal.fergana.department.Department;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Waste {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String status;

    private BigDecimal weight;

    private BigDecimal packWeight;

    private BigDecimal price;

    private UUID inOutId;

    @CreationTimestamp
    private Timestamp createDate;


    @UpdateTimestamp
    private Timestamp updateDate;


    private Timestamp verifiedDate = new Timestamp(System.currentTimeMillis());


    @ManyToOne
    private Product product;

    @ManyToOne
    private Kindergarten kindergarten;

    @ManyToOne
    private Department department;

    @ManyToOne
    private Warehouse warehouse;

    @ManyToOne
    private KindergartenWarehouse kindergartenWarehouse;


    public Waste(String status, BigDecimal weight, BigDecimal packWeight, Product product, Kindergarten kindergarten) {
        this.status = status;
        this.weight = weight;
        this.packWeight = packWeight;
        this.product = product;
        this.kindergarten = kindergarten;
    }


    public Waste(String status, BigDecimal weight, BigDecimal packWeight, Product product, Department department, UUID inOutId) {
        this.status = status;
        this.weight = weight;
        this.packWeight = packWeight;
        this.product = product;
        this.department = department;
        this.inOutId = inOutId;
    }
}
