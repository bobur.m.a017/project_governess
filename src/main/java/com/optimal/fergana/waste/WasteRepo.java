package com.optimal.fergana.waste;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WasteRepo extends JpaRepository<Waste, Integer> {

    Page<Waste> findAllByKindergarten_IdOrderByCreateDateDesc(Integer kindergarten_id, Pageable pageable);

    Page<Waste> findAllByDepartment_IdOrderByCreateDateDesc(Integer department_id, Pageable pageable);

    Page<Waste> findAllByKindergarten_District_IdOrderByCreateDateDesc(Integer districtId, Pageable pageable);

    Page<Waste> findAllByOrderByCreateDateDesc(Pageable pageable);
}
