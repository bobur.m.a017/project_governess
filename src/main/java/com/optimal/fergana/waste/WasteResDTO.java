package com.optimal.fergana.waste;


import com.optimal.fergana.department.Department;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WasteResDTO {

    private Integer id;

    private String status;

    private BigDecimal weight;

    private BigDecimal packWeight;

    private BigDecimal price;

    private UUID inOutId;

    private Timestamp createDate;
    private Timestamp updateDate;
    private Timestamp verifiedDate;
    private String productName;
    private Integer productId;

    private String chiqarvchiKorxonaNomi;

    private Integer kindergartenId;
    private String kindergartenName;

    private String departmentName;
    private Integer departmentId;
}
