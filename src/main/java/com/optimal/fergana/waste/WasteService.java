package com.optimal.fergana.waste;


import com.optimal.fergana.department.Department;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.role.RoleType;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseRepo;
import com.optimal.fergana.warehouse.inOut.InOutPrice;
import com.optimal.fergana.warehouse.inOut.InOutPriceRepo;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouse;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouseRepo;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class WasteService {


    private final WasteRepo wasteRepo;
    private final ProductRepo productRepo;
    private final KindergartenWarehouseRepo kindergartenWarehouseRepo;
    private final KindergartenWarehouseService kindergartenWarehouseService;
    private final WarehouseRepo warehouseRepo;
    private final InOutPriceRepo inOutPriceRepo;
    private final ProductPackService productPackService;


    public StateMessage add(Users users, Integer productId, BigDecimal weightPack) {


        boolean res = true;

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<Product> optionalProduct = productRepo.findById(productId);

        if (optionalProduct.isPresent() && weightPack.compareTo(BigDecimal.ZERO) > 0) {

            Product product = optionalProduct.get();


            BigDecimal weight = weightPack;

            BigDecimal pack = productPackService.checkPack(users.getDepartment().getId(), productId);

            if (pack != null) {
                BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.valueOf(1));
                if (bigDecimals[1].compareTo(BigDecimal.valueOf(0)) > 0) {
                    return new StateMessage("Ushbu maxsulot miqdorini donada kiriting", false, 417);
                }
                weight = weightPack.multiply(pack).divide(BigDecimal.valueOf(1000),6, RoundingMode.HALF_UP);
            }


            Kindergarten kindergarten = users.getKindergarten();
            Optional<KindergartenWarehouse> optional = kindergartenWarehouseRepo.findByKindergarten_IdAndProduct_Id(kindergarten.getId(), productId);

            if (optional.isPresent()) {
                KindergartenWarehouse kindergartenWarehouse = optional.get();
                if (kindergartenWarehouse.getTotalWeight().compareTo(weightPack) >= 0) {
                    kindergartenWarehouseService.subtract(product, weight, weightPack, kindergarten, LocalDate.now(), false);

                    Waste waste = new Waste(
                            Status.NEW.getName(),
                            weight,
                            weightPack,
                            product,
                            kindergarten
                    );
                    waste.setKindergartenWarehouse(kindergartenWarehouse);
                    wasteRepo.save(waste);

                    res = false;

                    message = Message.SUCCESS_UZ;
                }
            }
        }
        if (res) {
            return new StateMessage("Ushbu maxsulotdan omborda kiritilgan miqdorda mavjud emas", false, 417);
        }

        return new StateMessage().parse(message);
    }

    public StateMessage add(Users users, BigDecimal weightPack, UUID inOutId) {

        Optional<InOutPrice> optionalInOutPrice = inOutPriceRepo.findById(inOutId);

        boolean res = true;

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (weightPack.compareTo(BigDecimal.ZERO) > 0 && optionalInOutPrice.isPresent()) {

            InOutPrice inOutPrice = optionalInOutPrice.get();

            Product product = inOutPrice.getWarehouse().getProduct();

            Department department = users.getDepartment();

            BigDecimal weight = weightPack;

            BigDecimal pack = productPackService.checkPack(users.getDepartment().getId(), product.getId());

            if (pack != null) {
                BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.valueOf(1));
                if (bigDecimals[1].compareTo(BigDecimal.valueOf(0)) > 0) {
                    return new StateMessage("Ushbu maxsulot miqdorini donada kiriting", false, 417);
                }
                weight = weightPack.multiply(pack).divide(BigDecimal.valueOf(1000),6, RoundingMode.HALF_UP);
            }

            if (inOutPrice.getWeight().compareTo(weightPack) >= 0) {

                inOutPrice.setWeight(inOutPrice.getWeight().subtract(weight));
                inOutPrice.setPackWeight(inOutPrice.getPackWeight().subtract(weightPack));

                Warehouse warehouse = inOutPrice.getWarehouse();
                warehouse.setTotalWeight(warehouse.getTotalWeight().subtract(weight));
                warehouse.setTotalPackWeight(warehouse.getTotalPackWeight().subtract(weightPack));

                inOutPriceRepo.save(inOutPrice);
                warehouseRepo.save(warehouse);

                Waste waste = new Waste(
                        Status.NEW.getName(),
                        weight,
                        weightPack,
                        product,
                        department,
                        inOutId
                );

                waste.setPrice(inOutPrice.getPrice());
                waste.setWarehouse(warehouse);
                wasteRepo.save(waste);
                message = Message.SUCCESS_UZ;
                res = false;
            }
        }
        if (res) {
            return new StateMessage("Ushbu maxsulotdan omborda kiritilgan miqdorda mavjud emas", false, 417);
        }

        return new StateMessage().parse(message);
    }

    public StateMessage delete(Users users, Integer wasteId) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        Optional<Waste> optional = wasteRepo.findById(wasteId);
        boolean res = false;

        if (optional.isPresent()) {
            Waste waste = optional.get();

            if (waste.getKindergarten() != null) {
                KindergartenWarehouse kindergartenWarehouse = waste.getKindergartenWarehouse();
                kindergartenWarehouse.setTotalWeight(kindergartenWarehouse.getTotalWeight().add(waste.getWeight()));
                kindergartenWarehouse.setTotalPackWeight(kindergartenWarehouse.getTotalPackWeight().add(waste.getWeight()));

                kindergartenWarehouseRepo.save(kindergartenWarehouse);

                res = true;
            } else if (waste.getDepartment() != null) {
                Optional<InOutPrice> outPriceOptional = inOutPriceRepo.findById(waste.getInOutId());

                if (outPriceOptional.isPresent()) {
                    InOutPrice inOutPrice = outPriceOptional.get();

                    inOutPrice.setWeight(inOutPrice.getWeight().add(waste.getWeight()));
                    inOutPrice.setPackWeight(inOutPrice.getPackWeight().add(waste.getWeight()));

                    inOutPriceRepo.save(inOutPrice);

                    Warehouse warehouse = waste.getWarehouse();

                    warehouse.setTotalWeight(warehouse.getTotalWeight().add(waste.getWeight()));
                    warehouse.setTotalPackWeight(warehouse.getTotalPackWeight().add(waste.getWeight()));

                    warehouseRepo.save(warehouse);
                }
                res = true;
            }
            if (res) {
                waste.setStatus(Status.DELETE.getName());
                wasteRepo.save(waste);
                message = Message.DELETE_UZ;
            }
        }
        return new StateMessage().parse(message);
    }

    public StateMessage verified(Users users, Integer wasteId, boolean res) {

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        Optional<Waste> optional = wasteRepo.findById(wasteId);

        if (optional.isPresent()) {
            Waste waste = optional.get();
            if (waste.getStatus().equals(Status.NEW.getName())) {
                waste.setVerifiedDate(new Timestamp(System.currentTimeMillis()));
                waste.setStatus(res ? Status.TASDIQLANDI.getName() : Status.REJECTED.getName());

                wasteRepo.save(waste);
                message = Message.SUCCESS_VERIFIED;
                if (!res) {
                    if (waste.getKindergarten() != null) {
                        KindergartenWarehouse kindergartenWarehouse = waste.getKindergartenWarehouse();
                        kindergartenWarehouse.setTotalWeight(kindergartenWarehouse.getTotalWeight().add(waste.getWeight()));
                        kindergartenWarehouse.setTotalPackWeight(kindergartenWarehouse.getTotalPackWeight().add(waste.getPackWeight()));

                        kindergartenWarehouseRepo.save(kindergartenWarehouse);


                        UUID inOutId = waste.getInOutId();

                        Optional<InOutPrice> outPrice = inOutPriceRepo.findById(inOutId);

                        if (outPrice.isPresent()) {

                            InOutPrice inOutPrice = outPrice.get();

                            inOutPrice.setWeight(inOutPrice.getWeight().add(waste.getWeight()));

                            inOutPrice.setPackWeight(inOutPrice.getPackWeight().add(waste.getWeight()));

                            inOutPriceRepo.save(inOutPrice);

                            Warehouse warehouse = waste.getWarehouse();

                            warehouse.setTotalWeight(warehouse.getTotalWeight().add(waste.getWeight()));
                            warehouse.setTotalPackWeight(warehouse.getTotalPackWeight().add(waste.getWeight()));
                            warehouseRepo.save(warehouse);

                        }
                    }
                }
            }
        }
        return new StateMessage().parse(message);
    }

    public CustomPageable getAll(Users users, Integer pageNumber, Integer pageSize, Integer districtId, Integer kindergartenId, Boolean kattaOmbor) {

        Pageable pageable = PageRequest.of(pageNumber != null ? pageNumber : 0, pageSize != null ? pageSize : 20);

        if (kattaOmbor) {
            kindergartenId = null;
            districtId = null;
        }

        Page<Waste> page = null;

        if (users.getRoles().get(0).getName().equals(RoleType.COOK.getName())) {
            page = wasteRepo.findAllByKindergarten_IdOrderByCreateDateDesc(users.getKindergarten().getId(), pageable);
        } else if (users.getRoles().get(0).getName().equals(RoleType.ROLE_OMBOR_MUDIRI.getName())) {
            page = wasteRepo.findAllByDepartment_IdOrderByCreateDateDesc(users.getDepartment().getId(), pageable);
        } else {
            if (districtId != null && kindergartenId != null) {
                page = wasteRepo.findAllByKindergarten_IdOrderByCreateDateDesc(kindergartenId, pageable);
            } else if (districtId != null) {
                page = wasteRepo.findAllByKindergarten_District_IdOrderByCreateDateDesc(districtId, pageable);
            } else if (kattaOmbor) {
                page = wasteRepo.findAllByDepartment_IdOrderByCreateDateDesc(users.getDepartment().getId(), pageable);
            } else {
                page = wasteRepo.findAllByOrderByCreateDateDesc(pageable);
            }
        }
        return new CustomPageable(page.getSize(), page.getNumber(), wasteToDTo(page.stream().toList()), page.getTotalElements());
    }

    public List<WasteResDTO> wasteToDTo(List<Waste> list) {

        List<WasteResDTO> dtoList = new ArrayList<>();

        for (Waste waste : list) {
            dtoList.add(
                    new WasteResDTO(
                            waste.getId(),
                            waste.getStatus(),
                            waste.getWeight(),
                            waste.getPackWeight(),
                            waste.getPrice(),
                            waste.getInOutId(),
                            waste.getCreateDate(),
                            waste.getUpdateDate(),
                            waste.getVerifiedDate(),
                            waste.getProduct().getName(),
                            waste.getProduct().getId(),
                            waste.getDepartment() != null ? "ASOSIY OMBOR" : waste.getKindergarten().getNumber() + waste.getKindergarten().getName(),
                            waste.getKindergarten() != null ? waste.getKindergarten().getId() : null,
                            waste.getKindergarten() != null ? waste.getKindergarten().getNumber() + waste.getKindergarten().getName() : null,
                            waste.getDepartment() != null ? waste.getDepartment().getName() : null,
                            waste.getDepartment() != null ? waste.getDepartment().getId() : null
                    )
            );
        }
        return dtoList;

    }
}
