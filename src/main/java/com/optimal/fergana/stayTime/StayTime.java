package com.optimal.fergana.stayTime;


import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.mealTime.MealTime;
import com.optimal.fergana.multiMenu.MultiMenu;
import com.optimal.fergana.product.sanpin_catecory.dailyNorm.DailyNorm;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class StayTime {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;


    @ManyToMany(mappedBy = "stayTime")
    private List<MealTime> mealTimeList;

    @ManyToMany(mappedBy = "stayTimeList")
    private List<Kindergarten> kindergartenList;

    @OneToMany(mappedBy = "stayTime")
    private List<MultiMenu> multiMenuList;

    @OneToMany(mappedBy = "stayTime")
    private List<DailyNorm> dailyNormList;

    @OneToMany(mappedBy = "stayTime")
    private List<AgeGroup> ageGroupList;
}
