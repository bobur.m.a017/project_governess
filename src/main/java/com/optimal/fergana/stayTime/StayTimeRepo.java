package com.optimal.fergana.stayTime;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StayTimeRepo extends JpaRepository<StayTime, Integer> {
}
