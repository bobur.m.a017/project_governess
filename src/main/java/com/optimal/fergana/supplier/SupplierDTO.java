package com.optimal.fergana.supplier;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
public class SupplierDTO {
    private Integer id;
    @NotNull
    private String name;

    @Size(min = 9, max = 9)
    @NotNull
    private String STIR;

    private String date;

    private String status;

    private String THSHT;

    private String DBIBT;

    private String IFUT;

    private String fond;

    private String address;

    private String phoneNumber;

    private String director;

}
