package com.optimal.fergana.supplier.contract;

import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContractDTO;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ContractDTO {

    @NotNull
    private String number;
    @NotNull
    private String lotNumber;
    @NotNull
    private Timestamp startDay;
    @NotNull
    private Timestamp endDay;
    @NotNull
    private Integer supplierId;
    @NotNull
    private List<KindergartenContractDTO> kindergartenContractList;
}
