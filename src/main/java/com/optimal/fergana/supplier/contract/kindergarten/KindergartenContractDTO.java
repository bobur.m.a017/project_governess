package com.optimal.fergana.supplier.contract.kindergarten;

import com.optimal.fergana.supplier.contract.product.ProductContractDTO;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
public class KindergartenContractDTO {

    private UUID id;

    @NotNull
    private Integer kindergartenId;
    @NotNull
    private List<ProductContractDTO> productContracts;
}