package com.optimal.fergana.supplier.contract;

import com.optimal.fergana.product.product_price.ProductPrice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface ContractRepo extends JpaRepository<Contract, Integer> {

    boolean existsAllByNumber(String number);

    Page<Contract> findAllByDeleteIsFalseAndDepartment_RegionalDepartment_Id(Integer regionalDepartmentId, Pageable pageable);

    Page<Contract> findAllByDeleteIsFalseAndDepartment_RegionalDepartment_IdAndSupplier_Id(Integer departmentId, Integer supplierId, Pageable pageable);

    Page<Contract> findAllByDeleteIsFalseAndDepartment_Id(Integer departmentId, Pageable pageable);

    Page<Contract> findAllByDeleteIsFalseAndDepartment_IdAndSupplier_Id(Integer departmentId, Integer supplierId, Pageable pageable);


    @Query(nativeQuery = true,
            value = "SELECT e FROM contract e WHERE e.end_day < :date AND e.delete == :false AND e.verified == :true")
    List<Contract> checkContractPeriod(LocalDate date);

    @Query(nativeQuery = true,
            value = "SELECT e FROM contract e WHERE e.end_day < :date AND e.delete == :false AND e.verified == :true")
    List<Contract> checkContractPeriodByKindergarten(LocalDate date);


}
