package com.optimal.fergana.supplier.contract;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.supplier.Supplier;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContract;
import com.optimal.fergana.users.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private boolean fullyCompleted = false;
    private boolean verified = false;

    @Column(unique = true)
    private String number;

    @Column(precision = 19, scale = 6)
    private BigDecimal totalSum;
    private String lotNumber;
    private String status;
    private boolean delete = false;

    //contract period
    private Timestamp startDay;
    private Timestamp endDay;


    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Supplier supplier;


    @OneToOne(fetch = FetchType.LAZY)
    private Department department;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "contract", cascade = CascadeType.ALL)
    private List<KindergartenContract> kindergartenContractList;


    @CreationTimestamp
    private Timestamp createDate;


    @UpdateTimestamp
    private Timestamp updateDate;


    @ManyToOne
    private Users createdBy;

    @ManyToOne
    private Users verifiedBy;


    @ManyToOne
    private Users updatedBy;

    public Contract(boolean fullyCompleted, String number, BigDecimal totalSum, String lotNumber, Timestamp startDay, Timestamp endDay, Supplier supplier, Department department, Users createdBy) {
        this.fullyCompleted = fullyCompleted;
        this.number = number;
        this.totalSum = totalSum;
        this.lotNumber = lotNumber;
        this.startDay = startDay;
        this.endDay = endDay;
        this.supplier = supplier;
        this.department = department;
        this.createdBy = createdBy;
    }
}
