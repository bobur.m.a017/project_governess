package com.optimal.fergana.supplier.contract;

import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContractDTO;
import com.optimal.fergana.supplier.contract.kindergarten.KindergartenContractResDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ContractResDTO {
    private Integer id;
    private String number;
    private String lotNumber;
    private Timestamp startDay;
    private Timestamp endDay;
    private Integer supplierId;
    private String supplierName;
    private Integer departmentId;
    private String departmentName;
    private List<KindergartenContractResDTO> kindergartenContractList;
    private boolean fullyCompleted;
    private BigDecimal totalSum;
    private String status;
    private boolean delete;
    private Timestamp createDate;
    private Timestamp updateDate;
    private String createdBy;
    private String updatedBy;
}
