package com.optimal.fergana.supplier.contract.kindergarten;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.supplier.contract.Contract;
import com.optimal.fergana.supplier.contract.product.ProductContract;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KindergartenContract {

    @Id
    private UUID id = UUID.randomUUID();


    @ManyToOne(fetch = FetchType.LAZY)
    private Kindergarten kindergarten;

    private BigDecimal totalSum;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private Contract contract;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kindergartenContract", cascade = CascadeType.ALL)
    private Set<ProductContract> productContracts;


    @CreationTimestamp
    private Timestamp createDate = new Timestamp(System.currentTimeMillis());

    @UpdateTimestamp
    private Timestamp updateDate;


    public KindergartenContract(Kindergarten kindergarten, BigDecimal totalSum, Contract contract, Set<ProductContract> productContracts) {
        this.kindergarten = kindergarten;
        this.totalSum = totalSum;
        this.contract = contract;
        this.productContracts = productContracts;
    }
}