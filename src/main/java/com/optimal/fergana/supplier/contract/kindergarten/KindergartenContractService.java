package com.optimal.fergana.supplier.contract.kindergarten;

import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.supplier.contract.Contract;
import com.optimal.fergana.supplier.contract.product.ProductContract;
import com.optimal.fergana.supplier.contract.product.ProductContractDTO;
import com.optimal.fergana.supplier.contract.product.ProductContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class KindergartenContractService {


    private final KindergartenRepo kindergartenRepo;
    private final ProductContractService productContractService;
    private final KindergartenContractRepo kindergartenContractRepo;


    public KindergartenContract add(KindergartenContractDTO dto, Contract contract) {


        Optional<Kindergarten> optionalKindergarten = kindergartenRepo.findById(dto.getKindergartenId());

        if (optionalKindergarten.isPresent()) {
            Kindergarten kindergarten = optionalKindergarten.get();

            KindergartenContract kindergartenContract = new KindergartenContract(
                    kindergarten,
                    BigDecimal.valueOf(0),
                    contract,
                    null
            );

            Set<ProductContract> productContracts = productContractService.add(dto.getProductContracts(), kindergartenContract);

            if (productContracts != null) {
                kindergartenContract.setTotalSum(getTotalSum(productContracts));
                kindergartenContract.setProductContracts(productContracts);
                return kindergartenContract;
            }
        }
        return null;
    }

    public List<KindergartenContract> add(List<KindergartenContractDTO> dtoList, Contract contract) {
        List<KindergartenContract> list = new ArrayList<>();

        for (KindergartenContractDTO kindergartenContractDTO : dtoList) {
            KindergartenContract kindergartenContract = add(kindergartenContractDTO, contract);
            if (kindergartenContract == null) {
                return null;
            }
            list.add(kindergartenContract);
        }
        return list;
    }

    public BigDecimal getTotalSum(Set<ProductContract> productContractList) {

        BigDecimal totalSum = BigDecimal.valueOf(0);

        for (ProductContract productContract : productContractList) {
            totalSum = totalSum.add(productContract.getTotalSum());
        }
        return totalSum;

    }

    public List<KindergartenContract> edit(List<KindergartenContractDTO> dtoList, Contract contract) {

        List<KindergartenContract> kindergartenContractList = contract.getKindergartenContractList();
        List<KindergartenContract> returnList = new ArrayList<>();
        List<KindergartenContract> deleteList = new ArrayList<>();


        for (KindergartenContract kindergartenContract : kindergartenContractList) {
            boolean res = true;
            for (KindergartenContractDTO kindergartenContractDTO : dtoList) {
                if (kindergartenContract.getId().equals(kindergartenContractDTO.getId())) {
                    res = false;
                    break;
                }
            }
            if (res) {
                kindergartenContract.setContract(null);
                deleteList.add(kindergartenContract);
            }
        }

        for (KindergartenContractDTO kindergartenContractDTO : dtoList) {
            if (kindergartenContractDTO.getId() != null) {
                Optional<KindergartenContract> optionalKindergartenContract = kindergartenContractRepo.findById(kindergartenContractDTO.getId());
                if (optionalKindergartenContract.isPresent()) {

                    KindergartenContract kindergartenContract = optionalKindergartenContract.get();

                    Set<ProductContract> productContractList = productContractService.edit(kindergartenContract, kindergartenContractDTO.getProductContracts());
                    kindergartenContract.setProductContracts(productContractList);
                    kindergartenContract.setTotalSum(getTotalSum(productContractList));

                    returnList.add(kindergartenContract);
                } else {
                    KindergartenContract add = add(kindergartenContractDTO, contract);
                    if (add != null)
                        returnList.add(add);
                }
            }else {
                KindergartenContract add = add(kindergartenContractDTO, contract);
                if (add != null)
                    returnList.add(add);
            }
        }

        for (int i = 0; i < deleteList.size(); i++) {
            kindergartenContractRepo.deleteById(deleteList.get(i).getId());
        }


        return kindergartenContractRepo.saveAll(returnList);
    }
}
