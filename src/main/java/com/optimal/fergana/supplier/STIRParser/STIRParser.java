package com.optimal.fergana.supplier.STIRParser;

import com.optimal.fergana.supplier.SupplierDTO;
import com.optimal.fergana.supplier.SupplierRepo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class STIRParser {

    public SupplierDTO parserLiveSTIR(String STIR) {

        try {
            String baseURL = "https://orginfo.uz";

            String linkSearchBySTIR = baseURL + "/search/organizations?q=" + STIR;

            Document document = Jsoup.connect(linkSearchBySTIR).get();

            Elements value = document.getElementsByAttributeValue("class", "organization-page-link");
            String companyLink = value.attr("href");
            String companyInfoLink = baseURL + companyLink;

            Document doc = Jsoup.connect(companyInfoLink).get();

            Elements elementsByTag = doc.getElementsByTag("dd");
            Elements elementsByTagName = doc.getElementsByTag("dt");

            Elements companyName = doc.getElementsByTag("h4");


            if (companyName.size() > 0) {
                SupplierDTO supplier = new SupplierDTO();

                supplier.setName(companyName.text());


                int index = 0;
                for (Element name : elementsByTagName) {
                    if (name.text().equals("Faoliyati")) {
                        supplier.setStatus(elementsByTag.get(index).text());
                    } else if (name.text().equals("Ro'yxatdan o'tgan sana")) {
                        supplier.setDate(elementsByTag.get(index).text());
                    } else if (name.text().equals("STIR")) {
                        supplier.setSTIR(elementsByTag.get(index).text());
                    } else if (name.text().equals("THSHT")) {
                        supplier.setTHSHT(elementsByTag.get(index).text());
                    } else if (name.text().equals("DBIBT")) {
                        supplier.setDBIBT(elementsByTag.get(index).text());
                    } else if (name.text().equals("IFUT")) {
                        supplier.setIFUT(elementsByTag.get(index).text());
                    } else if (name.text().equals("Ustav fondi")) {
                        supplier.setFond(elementsByTag.get(index).text());
                    } else if (name.text().equals("Manzili")) {
                        supplier.setAddress(elementsByTag.get(index).text());
                    } else if (name.text().equals("Telefon raqami")) {
                        supplier.setPhoneNumber(elementsByTag.get(index).text());
                    } else if (name.text().equals("Rahbar")) {
                        supplier.setDirector(elementsByTag.get(index).text());
                    }

                    if (!(name.text().equals("Kontakt ma'lumotlari") || name.text().equals("Boshqaruv ma'lumotlari") || name.text().equals("Ta'sischilar"))) {

                        index++;
                    }
                }

//                supplier.setStatus(elementsByTag.get(0).text());
//                supplier.setDate(elementsByTag.get(1).text());
//                supplier.setSTIR(elementsByTag.get(3).text());
//                supplier.setTHSHT(elementsByTag.get(4).text());
//                supplier.setDBIBT(elementsByTag.get(5).text());
//                supplier.setIFUT(elementsByTag.get(6).text());
//                supplier.setFond(elementsByTag.get(7).text());
//                supplier.setAddress(elementsByTag.get(8).text());
//                supplier.setPhoneNumber(elementsByTag.get(9).text());
//                if (elementsByTag.size() > 11)
//                    supplier.setDirector(elementsByTag.get(11).text());
                return supplier;

            } else {
                throw new UsernameNotFoundException("bunday STIR mavjud emas!!!!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
