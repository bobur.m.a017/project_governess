package com.optimal.fergana.supplier;

import lombok.Getter;

@Getter

public class SupplierResDTO {
    private Integer id;
    private String name;

    private String STIR;


    private String date;

    private String status;

    private String THSHT;

    private String DBIBT;

    private String IFUT;

    private String fond;

    private String address;

    private String phoneNumber;

    private String director;

    public SupplierResDTO(Integer id, String name, String STIR, String date, String status, String THSHT, String DBIBT, String IFUT, String fond, String address, String phoneNumber, String director) {
        this.id = id;
        this.name = name;
        this.STIR = STIR;
        this.date = date;
        this.status = status;
        this.THSHT = THSHT;
        this.DBIBT = DBIBT;
        this.IFUT = IFUT;
        this.fond = fond;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.director = director;
    }
}
