package com.optimal.fergana.supplier;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SupplierRepo extends JpaRepository<Supplier, Integer> {

    Optional<Supplier> findBySTIR(String STIR);
    boolean existsBySTIR(Integer STIR);

    Page<Supplier> findAllByRegionalDepartments_Id(Integer regionalDepartments_id, Pageable pageable);
    Page<Supplier> findAllByDepartments_Id(Integer departments_id, Pageable pageable);
    List<Supplier> findAllByDepartments_Id(Integer departments_id);

    @Override
    long count();

    long countAllByRegionalDepartments_Id(Integer regionalDepartments_id);
    long countAllByDepartments_Id(Integer departments_id);
}
