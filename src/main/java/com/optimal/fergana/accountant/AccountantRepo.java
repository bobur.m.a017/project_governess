package com.optimal.fergana.accountant;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountantRepo extends JpaRepository<Accountant,Integer> {
    List<Accountant> findAllByUser_Department_Id(Integer department_id);
}
