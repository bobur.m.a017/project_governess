package com.optimal.fergana.accountant;

import lombok.Getter;

import java.util.List;

@Getter
public class AccountantDTO {
    private Integer userId;
    private List<Integer> kindergartenIds;
}
