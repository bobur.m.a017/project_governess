package com.optimal.fergana.department;

import com.optimal.fergana.address.district.District;
import com.optimal.fergana.address.district.DistrictRepo;
import com.optimal.fergana.address.region.Region;
import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.kindergarten.KindergartenService;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentRepo;
import com.optimal.fergana.role.Role;
import com.optimal.fergana.role.RoleRepo;
import com.optimal.fergana.role.RoleType;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Service
public class DepartmentService implements DepartmentInterface, DepartmentServiceInter {

    private final DepartmentRepo departmentRepo;
    private final DistrictRepo districtRepo;
    private final RegionalDepartmentRepo regionalDepartmentRepo;
    private final CustomConverter converter;

    private final UserService userService;

    public StateMessage add(DepartmentDTO dto, Users users) {
        boolean res = departmentRepo.existsAllByRegionalDepartmentAndName(users.getRegionalDepartment(), dto.getName());
        Optional<District> optional = districtRepo.findById(dto.getDistrictId());
        Message message;

        if (res || optional.isEmpty()) {
            message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        } else {
            District district = optional.get();
            RegionalDepartment regionalDepartment = users.getRegionalDepartment();
            Department department = new Department(dto.getName(), district, regionalDepartment);

            Department save = departmentRepo.save(department);
            message = Message.SUCCESS_UZ;

        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(DepartmentDTO dto, Users users, Integer id) {
        Message message;
        Optional<Department> optionalDepartment = departmentRepo.findById(id);

        if (optionalDepartment.isPresent()) {
            Department department = optionalDepartment.get();
            boolean res = false;
            if (!department.getName().equals(dto.getName())) {
                res = departmentRepo.existsAllByRegionalDepartmentAndName(users.getRegionalDepartment(), dto.getName());
            }

            if (!res) {
                department.setName(dto.getName());
                departmentRepo.save(department);
                message = Message.SUCCESS_UZ;
            } else {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            }

        } else {
            message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
        }
        return new StateMessage().parse(message);
    }

    public ResponseEntity<?> getOne(Users users, Integer id) {
        Optional<Department> optionalDepartment = departmentRepo.findById(id);
        if (optionalDepartment.isEmpty()) {
            StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
            return ResponseEntity.status(message.getCode()).body(message);
        } else {
            Department department = optionalDepartment.get();
            DepartmentResDTO dto = parse(department);
            dto.setUsersList(converter.userToDTO(department.getUsersList()));
//            dto.setKindergartenList(kindergartenService.getAll(users));
            return ResponseEntity.status(200).body(dto);
        }
    }

    public ResponseEntity<?> getAll(Users users) {

        List<DepartmentResDTO> list = new ArrayList<>();

        if (users.getDepartment() != null) {
            list.add(parse(users.getDepartment()));
        } else {
            for (Department department : departmentRepo.findAllByRegionalDepartment(users.getRegionalDepartment())) {

                list.add(parse(department));
            }
        }

        list.sort(Comparator.comparing(DepartmentResDTO::getName));
        return ResponseEntity.status(200).body(list);
    }

    public StateMessage delete(Users users, Integer id) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();

        Optional<Department> optionalDepartment = departmentRepo.findById(id);
        Message message;
        if (optionalDepartment.isEmpty()) {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        } else {
            Department department = optionalDepartment.get();

            List<Department> departmentList = regionalDepartment.getDepartmentList();
            departmentList.remove(department);
            regionalDepartment.setDepartmentList(departmentList);
            regionalDepartmentRepo.save(regionalDepartment);
            department.setRegionalDepartment(null);
            Department save = departmentRepo.save(department);


            departmentRepo.delete(save);
            message = Message.DELETE_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage setTime(int hours, int minutes, HttpServletRequest request) {


        if (hours > 23 || hours < 0 || minutes > 59 || minutes < 0) {
            return new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
        }
        Users user = userService.parseToken(request);

        Department department = user.getDepartment();

        department.setLocalTime(LocalTime.of(hours, minutes));
        departmentRepo.save(department);
        return new StateMessage().parse(Message.SUCCESS_UZ);
    }

    public LocalTime getTime(HttpServletRequest request) {
        Users user = userService.parseToken(request);
        return user.getDepartment().getLocalTime();
    }
}
