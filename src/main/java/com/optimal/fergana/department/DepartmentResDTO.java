package com.optimal.fergana.department;

import com.optimal.fergana.address.district.District;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenResDTO;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.users.UsersResDTO;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

public class DepartmentResDTO {
    private Integer id;
    private String name;
    private String districtName;
    private String regionalDepartmentName;
    private Integer regionalDepartmentId;
    private List<KindergartenResDTO> kindergartenList;
    private List<UsersResDTO> usersList;


    public DepartmentResDTO() {
    }

    public DepartmentResDTO(Integer id, String name, String districtName, String regionalDepartmentName, Integer regionalDepartmentId) {
        this.id = id;
        this.name = name;
        this.districtName = districtName;
        this.regionalDepartmentName = regionalDepartmentName;
        this.regionalDepartmentId = regionalDepartmentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getRegionalDepartmentName() {
        return regionalDepartmentName;
    }

    public void setRegionalDepartmentName(String regionalDepartmentName) {
        this.regionalDepartmentName = regionalDepartmentName;
    }

    public Integer getRegionalDepartmentId() {
        return regionalDepartmentId;
    }

    public void setRegionalDepartmentId(Integer regionalDepartmentId) {
        this.regionalDepartmentId = regionalDepartmentId;
    }

    public List<KindergartenResDTO> getKindergartenList() {
        return kindergartenList;
    }

    public void setKindergartenList(List<KindergartenResDTO> kindergartenList) {
        this.kindergartenList = kindergartenList;
    }

    public List<UsersResDTO> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<UsersResDTO> usersList) {
        this.usersList = usersList;
    }
}
