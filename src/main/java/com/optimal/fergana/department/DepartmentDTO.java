package com.optimal.fergana.department;

import com.sun.istack.NotNull;

public class DepartmentDTO {
    @NotNull
    private String name;
    @NotNull
    private Integer districtId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }
}
