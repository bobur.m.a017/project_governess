package com.optimal.fergana.department;

public interface DepartmentInterface {

    default DepartmentResDTO parse(Department department) {
        return new DepartmentResDTO(
                department.getId(),
                department.getName(),
                department.getDistrict().getName(),
                department.getRegionalDepartment().getName(),
                department.getRegionalDepartment().getId()
        );
    }
}
