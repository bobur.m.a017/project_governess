package com.optimal.fergana.department;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;

public interface DepartmentServiceInter {
    StateMessage add(DepartmentDTO dto, Users users);
    StateMessage edit(DepartmentDTO dto, Users users, Integer id);
    ResponseEntity<?> getOne(Users users, Integer id);
    ResponseEntity<?> getAll(Users users);
    StateMessage delete(Users users, Integer id);
}
