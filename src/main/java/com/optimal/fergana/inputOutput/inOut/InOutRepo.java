package com.optimal.fergana.inputOutput.inOut;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface InOutRepo extends JpaRepository<InOut, UUID> {

    Optional<InOut> findByInputOutput_IdAndProduct_Id(UUID inputOutput_id, Integer product_id);

}
