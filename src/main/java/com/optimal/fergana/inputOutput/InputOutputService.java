package com.optimal.fergana.inputOutput;


import com.optimal.fergana.address.district.District;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.inputOutput.inOut.InOutRepo;
import com.optimal.fergana.inputOutput.inOutAgeGroup.InOutAgeGroup;
import com.optimal.fergana.inputOutput.inOutAgeGroup.InOutAgeGroupRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.menu.ProductMenuDTO;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.product_price.ProductPrice;
import com.optimal.fergana.product.product_price.ProductPriceRepo;
import com.optimal.fergana.warehouse.inOut.InOutPriceDTO;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouse;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class InputOutputService {

    private final InputOutputRepo inputOutputRepo;
    private final InOutRepo inOutRepo;
    private final InOutAgeGroupRepo inOutAgeGroupRepo;
    private final KindergartenRepo kindergartenRepo;
    private final ProductPriceRepo productPriceRepo;


    public void addInputKindergarten(Product product, Kindergarten kindergarten, LocalDate localDate, BigDecimal weight, BigDecimal packWeight, BigDecimal pack) {

        Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByKindergarten_IdAndLocalDate(kindergarten.getId(), localDate);

        if (inputOutputOptional.isPresent()) {
            InputOutput inputOutput = inputOutputOptional.get();
            Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());

            if (optionalInOut.isPresent()) {
                InOut inOut = optionalInOut.get();
                inOut.setInputWeight(inOut.getInputWeight().add(weight));
                inOut.setInputPackWeight(inOut.getInputPackWeight().add(packWeight));
                inOutRepo.save(inOut);
            } else {
                InOut inOut = new InOut(product, inputOutput);
                inOut.setInputWeight(weight);
                inOut.setInputPackWeight(packWeight);
                inOutRepo.save(inOut);
            }
        } else {

            InputOutput inputOutput = new InputOutput(localDate, kindergarten);
            InOut inOut = new InOut(product, inputOutput);
            inOut.setInputWeight(weight);
            inOut.setInputPackWeight(packWeight);
            inputOutputRepo.save(inputOutput);
            inOutRepo.save(inOut);
        }
    }

    public void addOutputKindergarten(Product product, Kindergarten kindergarten, LocalDate localDate, BigDecimal weight, BigDecimal weightPack) {

        Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByKindergarten_IdAndLocalDate(kindergarten.getId(), localDate);

        if (inputOutputOptional.isPresent()) {
            InputOutput inputOutput = inputOutputOptional.get();
            Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());

            if (optionalInOut.isPresent()) {
                InOut inOut = optionalInOut.get();
                inOut.setOutputWeight(inOut.getOutputWeight().add(weight));
                inOut.setOutputPackWeight(inOut.getOutputPackWeight().add(weightPack));
                inOutRepo.save(inOut);
            } else {
                InOut inOut = new InOut(product, inputOutput);
                inOut.setOutputWeight(weight);
                inOut.setOutputPackWeight(weightPack);
                inOutRepo.save(inOut);
            }
        } else {
            InputOutput inputOutput = new InputOutput(localDate, kindergarten);
            InOut inOut = new InOut(product, inputOutput);

            inOut.setOutputWeight(weight);
            inOut.setOutputPackWeight(weightPack);
            inputOutputRepo.save(inputOutput);
        }
    }

    public Vector<ProductMenuDTO> getPrice(Vector<ProductMenuDTO> productList, District district, int year, int month, Department department) {

        for (ProductMenuDTO productMenuDTO : productList) {

            Optional<ProductPrice> optional = productPriceRepo.findByDepartment_IdAndDistrict_IdAndYearAndMonthAndProduct_Id(department.getId(), district.getId(), year, month, productMenuDTO.getId());
            BigDecimal price = BigDecimal.valueOf(0);

            if (optional.isPresent()) {
                ProductPrice productPrice = optional.get();
                price = productPrice.getPrice();
            }

            productMenuDTO.setPrice(price);

        }

        return productList;

    }

    public void addInputDepartment(Product product, Department department, LocalDate localDate, BigDecimal weight, BigDecimal packWeight, BigDecimal pack) {

        Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByDepartment_IdAndLocalDate(department.getId(), localDate);

        if (inputOutputOptional.isPresent()) {
            InputOutput inputOutput = inputOutputOptional.get();
            Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());

            if (optionalInOut.isPresent()) {
                InOut inOut = optionalInOut.get();
                inOut.setInputWeight(inOut.getInputWeight().add(weight));
                inOut.setInputPackWeight(inOut.getInputPackWeight().add(packWeight));
                inOutRepo.save(inOut);
            } else {
                InOut inOut = new InOut(product, inputOutput);
                inOut.setInputWeight(weight);
                inOut.setInputPackWeight(packWeight);
                inOutRepo.save(inOut);
            }
        } else {
            InputOutput inputOutput = new InputOutput(localDate, department);
            InOut inOut = new InOut(product, inputOutput);
            inOut.setInputWeight(weight);
            inOut.setInputPackWeight(packWeight);
            inputOutputRepo.save(inputOutput);
            inOutRepo.save(inOut);
        }
    }

    public void addOutputDepartment(Product product, Department department, LocalDate localDate, BigDecimal weight, BigDecimal packWeight, BigDecimal pack) {

        Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByDepartment_IdAndLocalDate(department.getId(), localDate);

        if (inputOutputOptional.isPresent()) {
            InputOutput inputOutput = inputOutputOptional.get();
            Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());

            if (optionalInOut.isPresent()) {
                InOut inOut = optionalInOut.get();
                inOut.setOutputWeight(inOut.getOutputWeight().add(weight));
                inOut.setOutputPackWeight(inOut.getOutputPackWeight().add(packWeight));
                inOutRepo.save(inOut);
            } else {
                InOut inOut = new InOut(product, inputOutput);
                inOut.setOutputWeight(weight);
                inOut.setOutputPackWeight(packWeight);
                inOutRepo.save(inOut);
            }
        } else {
            InputOutput inputOutput = new InputOutput(localDate, department);
            InOut inOut = new InOut(product, inputOutput);
            inOut.setOutputWeight(weight);
            inOut.setOutputPackWeight(packWeight);
            inputOutputRepo.save(inputOutput);
            inOutRepo.save(inOut);
        }
    }


    @Scheduled(cron = "0 50 23 * * ?")
    public void qoldiqXisoblash() {

        List<Kindergarten> all = kindergartenRepo.findAll();
        LocalDate localDate = LocalDate.now();

        for (Kindergarten kindergarten : all) {
            List<KindergartenWarehouse> kindergartenWarehouses = kindergarten.getKindergartenWarehouses();

            for (KindergartenWarehouse kindergartenWarehouse : kindergartenWarehouses) {

                if (kindergartenWarehouse.getTotalWeight().compareTo(BigDecimal.ZERO) > 0) {

                    Product product = kindergartenWarehouse.getProduct();
                    BigDecimal weight = kindergartenWarehouse.getTotalWeight();
                    BigDecimal packWeight = kindergartenWarehouse.getTotalPackWeight();

                    Optional<InputOutput> inputOutputOptional = inputOutputRepo.findByKindergarten_IdAndLocalDate(kindergarten.getId(), localDate);

                    if (inputOutputOptional.isPresent()) {
                        InputOutput inputOutput = inputOutputOptional.get();
                        Optional<InOut> optionalInOut = inOutRepo.findByInputOutput_IdAndProduct_Id(inputOutput.getId(), product.getId());

                        if (optionalInOut.isPresent()) {
                            InOut inOut = optionalInOut.get();
                            inOut.setResidual(inOut.getResidual().add(weight));
                            inOut.setResidualPack(inOut.getResidualPack().add(packWeight));
                            inOutRepo.save(inOut);
                        } else {
                            InOut inOut = new InOut(product, inputOutput);
                            inOut.setResidual(weight);
                            inOut.setResidualPack(packWeight);
                            inOutRepo.save(inOut);
                        }
                    } else {

                        InputOutput inputOutput = new InputOutput(localDate, kindergarten);
                        InOut inOut = new InOut(product, inputOutput);
                        inOut.setResidual(weight);
                        inOut.setResidualPack(packWeight);
                        inputOutputRepo.save(inputOutput);
                        inOutRepo.save(inOut);
                    }
                }
            }
        }
    }


}
