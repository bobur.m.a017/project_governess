package com.optimal.fergana.inputOutput.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DateInputOutPutDTO {
    private LocalDate date;
    private BigDecimal input;
    private BigDecimal output;
    private BigDecimal residual;
}
