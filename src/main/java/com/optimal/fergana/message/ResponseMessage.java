package com.optimal.fergana.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessage {
    private String text;
    private boolean success;
    private Integer code;
    private Object data;

    public StateMessage parse(Message message){
        return new StateMessage(message.getName(), message.isState(), message.getCode());
    }
}
