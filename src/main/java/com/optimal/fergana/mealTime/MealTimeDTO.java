package com.optimal.fergana.mealTime;


import com.sun.istack.NotNull;

import java.util.List;

public class MealTimeDTO {
    @NotNull
    private String name;

    @NotNull
    private List<Integer> ageGroupIdList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getAgeGroupIdList() {
        return ageGroupIdList;
    }

    public void setAgeGroupIdList(List<Integer> ageGroupIdList) {
        this.ageGroupIdList = ageGroupIdList;
    }
}
