package com.optimal.fergana.mealTime;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MealTimeRepo extends JpaRepository<MealTime, Integer> {

    boolean existsAllByNameAndRegionalDepartment(String name, RegionalDepartment regionalDepartment);

    Optional<MealTime> findByRegionalDepartmentAndNameAndDelete(RegionalDepartment regionalDepartment, String name, boolean res);

    List<MealTime> findAllByRegionalDepartmentAndDeleteIsFalse(RegionalDepartment regionalDepartment);

}
