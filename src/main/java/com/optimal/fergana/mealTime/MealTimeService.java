package com.optimal.fergana.mealTime;

import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.ageGroup.AgeGroupResDTO;
import com.optimal.fergana.ageGroup.AgeGroupService;
import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class MealTimeService implements MealTimeInterface, MealTimeServiceInter {

    private final MealTimeRepo mealTimeRepo;
    private final AgeGroupRepo ageGroupRepo;
    private final AgeGroupService ageGroupService;

    public MealTimeService(MealTimeRepo mealTimeRepo, AgeGroupRepo ageGroupRepo, AgeGroupService ageGroupService) {
        this.mealTimeRepo = mealTimeRepo;
        this.ageGroupRepo = ageGroupRepo;
        this.ageGroupService = ageGroupService;
    }


    public StateMessage add(MealTimeDTO dto, Users users) {

        RegionalDepartment regionalDepartment = users.getRegionalDepartment();
        boolean res = mealTimeRepo.existsAllByNameAndRegionalDepartment(dto.getName(), regionalDepartment);

        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        List<AgeGroup> ageGroupList = ageGroupRepo.findAllById(dto.getAgeGroupIdList());
        if (res) {

            Optional<MealTime> optionalMealTime = mealTimeRepo.findByRegionalDepartmentAndNameAndDelete(regionalDepartment, dto.getName(), true);
            if (optionalMealTime.isPresent()) {
                MealTime mealTime = optionalMealTime.get();
                mealTime.setDelete(false);
                mealTime.setAgeGroupList(ageGroupList);
                mealTimeRepo.save(mealTime);
                message = Message.SUCCESS_UZ;
            }
        } else {
            MealTime mealTime = mealTimeRepo.save(new MealTime(dto.getName(), false, ageGroupList, regionalDepartment));
            message = Message.SUCCESS_UZ;
        }

        return new StateMessage().parse(message);
    }

    public StateMessage edit(MealTimeDTO dto, Users users, Integer id) {

        Optional<MealTime> optional = mealTimeRepo.findById(id);

        Message message;

        if (optional.isPresent()) {
            MealTime mealTime = optional.get();
            boolean res = false;
            if (!dto.getName().equals(mealTime.getName())) {
                res = mealTimeRepo.existsAllByNameAndRegionalDepartment(dto.getName(), users.getRegionalDepartment());
            }
            if (res) {
                message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;
            } else {
                List<AgeGroup> ageGroupList = ageGroupRepo.findAllById(dto.getAgeGroupIdList());
                mealTime.setName(dto.getName());
                mealTime.setAgeGroupList(ageGroupList);
                mealTimeRepo.save(mealTime);
                message = Message.SUCCESS_UZ;
            }
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }
        return new StateMessage(message.getName(), message.isState(), message.getCode());
    }

    public ResponseEntity<?> getOne(Integer id) {

        Optional<MealTime> optional = mealTimeRepo.findById(id);

        if (optional.isPresent()) {
            MealTime mealTime = optional.get();
            MealTimeResDto dto = parse(mealTime);
            dto.setAgeGroupList(getAgeGroup(mealTime));
            return ResponseEntity.status(200).body(dto);
        }
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        return ResponseEntity.status(message.getCode()).body(new StateMessage().parse(message));
    }

    public List<MealTimeResDto> getAll(Users users) {

        List<MealTimeResDto> list = new ArrayList<>();

        List<MealTime> mealTimeList = mealTimeRepo.findAllByRegionalDepartmentAndDeleteIsFalse(users.getRegionalDepartment());

        mealTimeList.sort(Comparator.comparing(MealTime::getSortNumber));

        for (MealTime mealTime : mealTimeList) {
            MealTimeResDto dto = parse(mealTime);
            dto.setAgeGroupList(getAgeGroup(mealTime));
            list.add(dto);
        }
        return list;
    }

    private List<AgeGroupResDTO> getAgeGroup(MealTime mealTime) {
        List<AgeGroupResDTO> ageGroupList = new ArrayList<>();
        for (AgeGroup ageGroup : mealTime.getAgeGroupList()) {
            ageGroupList.add(ageGroupService.parse(ageGroup));
        }
        return ageGroupList;
    }

    public StateMessage delete(Users users, Integer id) {

        Optional<MealTime> optional = mealTimeRepo.findById(id);
        Message message;

        if (optional.isPresent()) {
            MealTime mealTime = optional.get();
            if (mealTime.getRegionalDepartment().getId().equals(users.getRegionalDepartment().getId())) {
                mealTime.setDelete(true);
                mealTimeRepo.save(mealTime);
                message = Message.DELETE_UZ;
            } else {
                message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
            }
        } else {
            message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;
        }

        return new StateMessage().parse(message);
    }
}