package com.optimal.fergana.mealTime;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MealTimeServiceInter {
    StateMessage add(MealTimeDTO dto, Users users);
    StateMessage edit(MealTimeDTO dto, Users users, Integer id);
    ResponseEntity<?> getOne(Integer id);
    List<MealTimeResDto> getAll(Users users);
    StateMessage delete(Users users, Integer id);
}
