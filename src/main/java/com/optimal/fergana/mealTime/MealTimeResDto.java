package com.optimal.fergana.mealTime;

import com.optimal.fergana.ageGroup.AgeGroupResDTO;

import java.util.List;

public class MealTimeResDto {

    private Integer id;
    private String name;
    private boolean delete;
    private List<AgeGroupResDTO> ageGroupList;

    public MealTimeResDto() {
    }

    public MealTimeResDto(Integer id, String name, boolean delete) {
        this.id = id;
        this.name = name;
        this.delete = delete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public List<AgeGroupResDTO> getAgeGroupList() {
        return ageGroupList;
    }

    public void setAgeGroupList(List<AgeGroupResDTO> ageGroupList) {
        this.ageGroupList = ageGroupList;
    }
}
