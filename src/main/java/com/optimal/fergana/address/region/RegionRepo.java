package com.optimal.fergana.address.region;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepo extends JpaRepository<Region,Integer> {
}
