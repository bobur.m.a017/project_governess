package com.optimal.fergana.address.district;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentResDTO;
import com.optimal.fergana.users.Users;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public record DistrictService(
        DistrictRepo districtRepo
) {

    public DistrictDTO parse(District district) {
        return new DistrictDTO(district.getId(), district.getName());
    }

    public List<DistrictDTO> getAll(Users users) {

        List<DistrictDTO> list = new ArrayList<>();


        for (District district : districtRepo.findAllByStateIsTrue()) {
            list.add(new DistrictDTO(district.getId(), district.getName()));
        }

        return list;
    }

    public List<DepartmentResDTO> getAllDistrict(Users users) {

        List<DepartmentResDTO> list = new ArrayList<>();
        if (users.getDepartment() != null) {
            Department department = users.getDepartment();
            for (District district : department.getAddressList()) {
                list.add(new DepartmentResDTO(district.getId(), district.getName(), district.getName(), department.getName(), department.getId()));
            }
        } else {
            for (District district : districtRepo.findAllByStateIsTrue()) {
                list.add(new DepartmentResDTO(district.getId(), district.getName(), district.getName(), "", null));
            }
        }
        return list;
    }
}
