package com.optimal.fergana.address.district;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DistrictRepo extends JpaRepository<District,Integer> {

    List<District> findAllByStateIsTrue();
}
