package com.optimal.fergana.mealCategory;

import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MealCategoryRepo extends JpaRepository<MealCategory, Integer> {
    boolean existsAllByNameAndRegionalDepartment(String name, RegionalDepartment regionalDepartment);

    List<MealCategory> findAllByRegionalDepartment(RegionalDepartment regionalDepartment);
}
