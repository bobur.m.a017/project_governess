package com.optimal.fergana.mealCategory;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MealCategoryServiceInter {
    StateMessage add(MealCategoryDTO dto, Users users);
    StateMessage edit(MealCategoryDTO dto, Users users, Integer id);
    ResponseEntity<?> getOne(Integer id);
    List<MealCategoryResDTO> getAll(Users users);
}
