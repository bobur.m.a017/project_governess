package com.optimal.fergana.mealCategory;

import com.optimal.fergana.message.Message;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentRepo;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MealCategoryService implements MealCategoryServiceInter {

    private final MealCategoryRepo mealCategoryRepo;
    private final RegionalDepartmentRepo regionalDepartmentRepo;

    public MealCategoryService(MealCategoryRepo mealCategoryRepo, RegionalDepartmentRepo regionalDepartmentRepo) {
        this.mealCategoryRepo = mealCategoryRepo;
        this.regionalDepartmentRepo = regionalDepartmentRepo;
    }

    public StateMessage add(MealCategoryDTO dto, Users users) {

        boolean res = mealCategoryRepo.existsAllByNameAndRegionalDepartment(dto.getName(), users.getRegionalDepartment());
        Message message = Message.INFORMATION_WITH_THIS_NAME_EXISTS;

        if (!res) {
            RegionalDepartment regionalDepartment = users.getRegionalDepartment();
            MealCategory mealCategory = mealCategoryRepo.save(new MealCategory(dto.getName(), regionalDepartment));

            List<MealCategory> mealCategoryList = regionalDepartment.getMealCategoryList() != null ? regionalDepartment.getMealCategoryList() : new ArrayList<>();
            mealCategoryList.add(mealCategory);
            regionalDepartment.setMealCategoryList(mealCategoryList);
            regionalDepartmentRepo.save(regionalDepartment);
            message = Message.SUCCESS_UZ;
        }
        return new StateMessage().parse(message);
    }

    public StateMessage edit(MealCategoryDTO dto, Users users, Integer id) {
        Optional<MealCategory> optionalMealCategory = mealCategoryRepo.findById(id);
        boolean res = mealCategoryRepo.existsAllByNameAndRegionalDepartment(dto.getName(), users.getRegionalDepartment());
        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optionalMealCategory.isPresent() && !res) {
            MealCategory mealCategory = optionalMealCategory.get();
            mealCategory.setName(dto.getName());
            mealCategoryRepo.save(mealCategory);
            message = Message.EDIT_UZ;
        }
        return new StateMessage().parse(message);
    }

    public ResponseEntity<?> getOne(Integer id) {

        Optional<MealCategory> optionalMealCategory = mealCategoryRepo.findById(id);
        if (optionalMealCategory.isPresent()) {
            MealCategory mealCategory = optionalMealCategory.get();
            MealCategoryResDTO dto = new MealCategoryResDTO(mealCategory.getId(), mealCategory.getName());
            return ResponseEntity.status(200).body(dto);
        }
        StateMessage message = new StateMessage().parse(Message.THE_DATA_WAS_ENTERED_INCORRECTLY);
        return ResponseEntity.status(message.getCode()).body(message);
    }

    public List<MealCategoryResDTO> getAll(Users users) {
        List<MealCategoryResDTO> list = new ArrayList<>();
        for (MealCategory mealCategory : mealCategoryRepo.findAllByRegionalDepartment(users.getRegionalDepartment())) {
            list.add(new MealCategoryResDTO(mealCategory.getId(), mealCategory.getName()));
        }
        return list;
    }

    public StateMessage delete(Integer id) {

        Optional<MealCategory> optional = mealCategoryRepo.findById(id);

        Message message = Message.THE_DATA_WAS_ENTERED_INCORRECTLY;

        if (optional.isPresent()) {
            MealCategory mealCategory = optional.get();

            if (mealCategory.getMealList().size() == 0) {
                mealCategoryRepo.delete(mealCategory);
                message = Message.DELETE_UZ;
            }
        }
        StateMessage parse = new StateMessage().parse(message);
        parse.setText("O`chirishni imkoni mavjud emas");
        return parse;
    }
}
