package com.optimal.fergana.mealCategory;


import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;


    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @OneToMany(mappedBy = "mealCategory")
    private List<Meal> mealList = new ArrayList<>();

    @ManyToOne
    private RegionalDepartment regionalDepartment;


    public MealCategory(String name, RegionalDepartment regionalDepartment) {
        this.name = name;
        this.regionalDepartment = regionalDepartment;
    }
}
