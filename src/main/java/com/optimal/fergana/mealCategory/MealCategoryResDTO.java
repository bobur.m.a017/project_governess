package com.optimal.fergana.mealCategory;


import com.optimal.fergana.meal.Meal;

import javax.persistence.*;
import java.util.List;

public class MealCategoryResDTO {
    private Integer id;
    private String name;

    public MealCategoryResDTO() {
    }

    public MealCategoryResDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
