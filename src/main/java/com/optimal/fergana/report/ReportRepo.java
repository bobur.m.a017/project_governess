package com.optimal.fergana.report;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ReportRepo extends JpaRepository<Report, UUID> {


    Optional<Report> findByYearAndMonthAndDayAndKindergarten_Id(Integer year, Integer month, Integer day, Integer kindergarten_Id);

    int countAllByYearAndMonthAndDayAndKindergarten_RegionalDepartment_Id(Integer year, Integer month, Integer day, Integer id);

    int countAllByYearAndMonthAndDayAndKindergarten_Department_Id(Integer year, Integer month, Integer day, Integer id);

    int countAllByYearAndMonthAndDayAndKindergarten_Id(Integer year, Integer month, Integer day, Integer id);

    int countAllByKindergarten_Department_IdAndKindergarten_District_IdAndYearAndMonthAndDayAndMenuNotNull(Integer departmentId,Integer districtId, Integer year, Integer month, Integer day);

    int countAllByKindergarten_IdAndYearAndMonthAndDayAndMenuNotNull(Integer departmentId, Integer year, Integer month, Integer day);

    Optional<Report> findByKindergarten_IdAndYearAndMonthAndDay(Integer kindergarten_id, Integer year, Integer month, Integer day);

    Page<Report> findByKindergarten_IdAndYearAndMonthAndDay(Integer id, Integer year, Integer month, Integer day, Pageable pageable);

    Page<Report> findAllByKindergarten_Department_IdAndYearAndMonthAndDay(Integer id, Integer year, Integer month, Integer day, Pageable pageable);

    Page<Report> findAllByKindergarten_RegionalDepartment_IdAndYearAndMonthAndDay(Integer id, Integer year, Integer month, Integer day, Pageable pageable);

    boolean existsAllByKindergarten_IdAndYearAndMonthAndDayAndMenuNotNull(Integer kindergarten_id, Integer year, Integer month, Integer day);


    Optional<Report> findByKindergarten_Id(Integer kindergarten_id);

    Optional<Report> findByKindergarten_IdAndYearAndMonthAndDayAndMenuNotNull(Integer kindergarten_id, Integer year, Integer month, Integer day);

    List<Report> findAllByKindergarten_IdAndDateBetween(Integer kindergarten_id, LocalDate date, LocalDate date2);
}
