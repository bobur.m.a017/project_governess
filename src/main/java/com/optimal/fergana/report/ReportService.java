package com.optimal.fergana.report;

import com.optimal.fergana.converter.CustomConverter;
import com.optimal.fergana.exception.NotificationNotFoundException;
import com.optimal.fergana.inputOutput.InputOutputService;
import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.inputOutput.inOut.InOutRepo;
import com.optimal.fergana.kids.KidsNumber;
import com.optimal.fergana.kids.KidsNumberRepo;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.ProductMenuDTO;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.reporter.excel.InputOutputReporterExcel;
import com.optimal.fergana.reporter.excel.MenuExcelReporter;
import com.optimal.fergana.reporter.excel.ReportPDF;
import com.optimal.fergana.reporter.pdf.InputOutputReporterPDF;
import com.optimal.fergana.reporter.pdf.InputOutputReporterPDFOmbor;
import com.optimal.fergana.reporter.pdf.KidsNumberPDFReporterByDate;
import com.optimal.fergana.reporter.pdf.MenuPDFReporter;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.Warehouse;
import com.optimal.fergana.warehouse.WarehouseRepo;
import com.optimal.fergana.warehouse.inOut.InOutPrice;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouse;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.Vector;

@RequiredArgsConstructor
@Service
public class ReportService implements ReportServiceInter {


    private final ReportRepo reportRepo;
    private final CustomConverter converter;
    private final MenuExcelReporter menuExcelReporter;
    private final MenuPDFReporter menuPDFReporter;
    private final KindergartenRepo kindergartenRepo;
    private final KidsNumberRepo kidsNumberRepo;
    private final KidsNumberPDFReporterByDate kidsNumberPDFReporterByDate;
    private final ReportPDF reportPDF;
    private final UserService userService;
    private final InputOutputReporterPDF inputOutputReporterPDF;
    private final InputOutputReporterPDFOmbor inputOutputReporterPDFOmbor;
    private final AgeStandardRepo ageStandardRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final ProductRepo productRepo;
    private final KindergartenWarehouseService kindergartenWarehouseService;
    private final InputOutputService inputOutputService;


    private final ProductPackService productPackService;


    public ReportResDTO getReportById(UUID id, HttpServletRequest request) {
        Users users = userService.parseToken(request);
        Optional<Report> optionalReport = reportRepo.findById(id);

        ReportResDTO dto = new ReportResDTO();

        if (optionalReport.isPresent()) {
            Report report = optionalReport.get();
            dto = converter.reportToDto(report);
        }
        return dto;
    }


    synchronized
    public void calculate(Report report) {

        if (report.getMenu() != null) {

            Menu menu = report.getMenu();
            Kindergarten kindergarten = report.getKindergarten();
            LocalDate date = report.getDate();

            Vector<AgeStandard> vector = ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_Id(menu.getId());
            Vector<ProductMenuDTO> productList = new Vector<>();

            for (AgeStandard ageStandard : vector) {
                Optional<KidsNumberSub> numberSubOptional = kidsNumberSubRepo.findByAgeGroup_IdAndKidsNumber_Report_IdAndKidsNumberDeleteIsFalse(ageStandard.getAgeGroup().getId(), report.getId());
                if (numberSubOptional.isPresent()) {
                    KidsNumberSub kidsNumberSub = numberSubOptional.get();

                    Integer number = kidsNumberSub.getNumber();
                    Meal meal = ageStandard.getMealAgeStandard().getMeal();
                    BigDecimal mealWeight = meal.getWeight();
                    for (ProductMeal productMeal : meal.getProductMealList()) {
                        BigDecimal productMealWeight = productMeal.getWeight();
                        BigDecimal divide = productMealWeight.divide(mealWeight, 6, RoundingMode.HALF_UP);
                        BigDecimal weight = divide.multiply(ageStandard.getWeight()).multiply(BigDecimal.valueOf(number));
                        weight = weight.divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP);
                        addProductList(productList, productMeal.getProduct(), weight);
                    }
                }
            }

            for (ProductMenuDTO productMenuDTO : productList) {
                Optional<Product> optionalProduct = productRepo.findById(productMenuDTO.getId());
                if (optionalProduct.isPresent()) {

                    Product product = optionalProduct.get();

                    BigDecimal checkPack = productPackService.checkPack(report.getKindergarten().getDepartment().getId(), product.getId());

                    BigDecimal weight = productMenuDTO.getWeight();
                    BigDecimal weightPack = productMenuDTO.getWeight();

                    if (checkPack != null) {
                        BigDecimal[] bigDecimals = weight.divideAndRemainder(BigDecimal.valueOf(1));
                        weightPack = weight.divide(checkPack.divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP), 6, RoundingMode.HALF_UP);
//                        weight = weightPack.multiply(checkPack.divide(BigDecimal.valueOf(1000), 6, RoundingMode.HALF_UP));
                    }

                    inputOutputService.addOutputKindergarten(product, kindergarten, report.getDate(), weight, weightPack);

                    kindergartenWarehouseService.subtract(product, weight, weightPack, kindergarten, report.getDate(), true);
                }
            }
        }
    }


    public void addProductList(Vector<ProductMenuDTO> list, Product product, BigDecimal weight) {

        boolean res = true;
        for (ProductMenuDTO productMenuDTO : list) {
            if (productMenuDTO.getId().equals(product.getId())) {
                productMenuDTO.setWeight(productMenuDTO.getWeight().add(weight));
                res = false;
                break;
            }
        }

        if (res) {
            list.add(new ProductMenuDTO(product.getId(), weight));
        }
    }


    public String getMenuReport(UUID reportId, String type, HttpServletResponse response) {

        Optional<Report> optionalReport = reportRepo.findById(reportId);
        if (optionalReport.isPresent()) {
            Report report = optionalReport.get();
            if (report.getMenu() != null && report.getKidsNumber() != null) {
//                if (report.getKidsNumber().isVerified()) {

                String path = "";

                if (type.equals("excel")) {
                    if (report.getFilePathExcel() != null) {
                        path = report.getFilePathExcel();
                    } else {
                        path = menuExcelReporter.createExcelReport(report);
                    }
                } else if (type.equals("pdf")) {
                    if (report.getFilePathPdf() != null) {
                        path = report.getFilePathPdf();
                    } else {
                        path = menuPDFReporter.createPDFReport(report);
                    }
                }

                if (path != null) {
                    return path;
                }
//                } else {
//                    throw new NotificationNotFoundException("Bu kunga kiritilgan bola soni mtt tomonidan tasdiqlanmagan.");
//                }
            } else {
                throw new NotificationNotFoundException("Bu kunga bola soni mtt tomonidan kiritilmagan.");
            }
        }
        return null;
    }


    public ReportResDTO getReportByDate(Users users, LocalDate date) {
        ReportResDTO dto = new ReportResDTO();
        if (users.getKindergarten() != null) {

            Optional<Report> optionalReport = reportRepo.findByKindergarten_IdAndYearAndMonthAndDay(users.getKindergarten().getId(), date.getYear(), date.getMonthValue(), date.getDayOfMonth());
            if (optionalReport.isPresent()) {
                Report report = optionalReport.get();
                dto = converter.reportToDto(report);
            }
        }

        return dto;
    }


    public String getReportInputOutPut(LocalDate start, LocalDate end, Integer kindergartenId, Users users) {
        if (users.getKindergarten() != null) {
            kindergartenId = users.getKindergarten().getId();
        }
        if (kindergartenId != null) {
            Optional<Kindergarten> optional = kindergartenRepo.findById(kindergartenId);
            if (optional.isPresent()) {
                try {

                    return inputOutputReporterPDF. createInputOutputPDF(start, end, optional.get());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public String getReportInputOutPut(LocalDate start, LocalDate end, Users users) {

        end = end.plusDays(1);

        if (users.getDepartment() != null) {
            try {
                if (users.getKindergarten() != null) {
                    return inputOutputReporterPDF.createInputOutputPDF(start, end, users.getKindergarten());
                }
                return inputOutputReporterPDFOmbor.createInputOutputPDF(start, end, users.getDepartment());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public String getKidsNumberPDF(LocalDate start, LocalDate end, Integer kindergartenId, HttpServletResponse response, Users users) {

        if (users.getKindergarten() != null) {
            kindergartenId = users.getKindergarten().getId();
        }

        if (kindergartenId != null) {

            try {

                List<KidsNumber> kidsNumberList = kidsNumberRepo.findAllByReport_Kindergarten_IdAndDateBetweenAndDeleteIsFalse(kindergartenId, start, end);
                String path = kidsNumberPDFReporterByDate.createPDFKindergarten(kidsNumberList, start, end);

                return path;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public String getReport(LocalDate start, LocalDate end, Integer districtId, List<Integer> kindergartenIdList, Users users, Integer ustama) {

        try {

            List<Kindergarten> kindergartenList = kindergartenRepo.findAllById(kindergartenIdList);

            return reportPDF.getReportFile(start, end, kindergartenList, users.getDepartment().getId(), districtId, start.getYear(), start.getMonthValue(), ustama);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public StateMessage menuAll(LocalDate start, LocalDate end, Integer kindergartenId) {

        List<Report> reportList = reportRepo.findAllByKindergarten_IdAndDateBetween(kindergartenId, start, end);

        String path = menuPDFReporter.createPDFReport(reportList);

        if (path == null) {
            return new StateMessage("MA'LUMOTLAR NOTO'G'RI KIRITILGAN", false, 417);
        }

        String s = StaticWords.URI + path.substring(StaticWords.DEST.length());
        return new StateMessage(s, true, 200);
    }
}
