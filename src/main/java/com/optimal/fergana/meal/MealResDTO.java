package com.optimal.fergana.meal;

import com.optimal.fergana.attachment.Attachment;
import com.optimal.fergana.mealCategory.MealCategory;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.productMeal.ProductMealResDTO;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import io.swagger.models.auth.In;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class MealResDTO {

    private Integer id;
    private String name;
    private BigDecimal weight;
    private Boolean delete;
    private String comment;
    private BigDecimal protein;
    private BigDecimal kcal;
    private BigDecimal oil;
    private BigDecimal carbohydrates;
    private String mealCategoryName;
    private Integer mealCategoryId;
    private String regionalDepartmentName;
    private Integer regionalDepartmentId;
    private String imagePath;
    private List<ProductMealResDTO> productMealList;
    private Integer attachmentId;

    public MealResDTO(Integer id, String name, BigDecimal weight, Boolean delete, String comment, BigDecimal protein, BigDecimal kcal, BigDecimal oil, BigDecimal carbohydrates, String mealCategoryName, Integer mealCategoryId, String regionalDepartmentName, Integer regionalDepartmentId, String imagePath, Integer attachmentId) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.delete = delete;
        this.comment = comment;
        this.protein = protein;
        this.kcal = kcal;
        this.oil = oil;
        this.carbohydrates = carbohydrates;
        this.mealCategoryName = mealCategoryName;
        this.mealCategoryId = mealCategoryId;
        this.regionalDepartmentName = regionalDepartmentName;
        this.regionalDepartmentId = regionalDepartmentId;
        this.imagePath = imagePath;
        this.attachmentId = attachmentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getProtein() {
        return protein;
    }

    public void setProtein(BigDecimal protein) {
        this.protein = protein;
    }

    public BigDecimal getKcal() {
        return kcal;
    }

    public void setKcal(BigDecimal kcal) {
        this.kcal = kcal;
    }

    public BigDecimal getOil() {
        return oil;
    }

    public void setOil(BigDecimal oil) {
        this.oil = oil;
    }

    public BigDecimal getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(BigDecimal carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public String getMealCategoryName() {
        return mealCategoryName;
    }

    public void setMealCategoryName(String mealCategoryName) {
        this.mealCategoryName = mealCategoryName;
    }

    public Integer getMealCategoryId() {
        return mealCategoryId;
    }

    public void setMealCategoryId(Integer mealCategoryId) {
        this.mealCategoryId = mealCategoryId;
    }

    public String getRegionalDepartmentName() {
        return regionalDepartmentName;
    }

    public void setRegionalDepartmentName(String regionalDepartmentName) {
        this.regionalDepartmentName = regionalDepartmentName;
    }

    public Integer getRegionalDepartmentId() {
        return regionalDepartmentId;
    }

    public void setRegionalDepartmentId(Integer regionalDepartmentId) {
        this.regionalDepartmentId = regionalDepartmentId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<ProductMealResDTO> getProductMealList() {
        return productMealList;
    }

    public void setProductMealList(List<ProductMealResDTO> productMealList) {
        this.productMealList = productMealList;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }
}