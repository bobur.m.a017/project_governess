package com.optimal.fergana.meal;

import com.optimal.fergana.productMeal.ProductMealDTO;
import com.sun.istack.NotNull;

import javax.validation.constraints.Size;
import java.util.List;

public class MealDTO {

    @Size(min = 2,max = 64)
    @NotNull
    private String name;

    @NotNull
    private Double weight;
    private String comment;

    @NotNull
    private Integer mealCategoryId;

    @NotNull
    private List<ProductMealDTO> productMealList;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getMealCategory() {
        return mealCategoryId;
    }

    public void setMealCategory(Integer mealCategoryId) {
        this.mealCategoryId = mealCategoryId;
    }

    public List<ProductMealDTO> getProductMealList() {
        return productMealList;
    }

    public void setProductMealList(List<ProductMealDTO> productMealList) {
        this.productMealList = productMealList;
    }
}
