package com.optimal.fergana.meal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.optimal.fergana.attachment.Attachment;
import com.optimal.fergana.mealCategory.MealCategory;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.regionalDepartment.RegionalDepartment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Meal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @CreationTimestamp
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp updateDate;

    @Column(precision = 19, scale = 6)
    private BigDecimal weight;

    private Boolean delete;


    @Column(columnDefinition = "TEXT")
    private String comment;

    @Column(precision = 19, scale = 6)
    private BigDecimal protein;
    @Column(precision = 19, scale = 6)
    private BigDecimal kcal;
    @Column(precision = 19, scale = 6)
    private BigDecimal oil;
    @Column(precision = 19, scale = 6)
    private BigDecimal carbohydrates;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private MealCategory mealCategory;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne
    private RegionalDepartment regionalDepartment;

    @OneToOne
    private Attachment attachment;

    @JsonIgnore(value = false)
    @OneToMany(mappedBy = "meal", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ProductMeal> productMealList;


    public Meal(String name, BigDecimal weight, Boolean delete, String comment, MealCategory mealCategory, RegionalDepartment regionalDepartment) {
        this.name = name;
        this.weight = weight;
        this.delete = delete;
        this.comment = comment;
        this.mealCategory = mealCategory;
        this.regionalDepartment = regionalDepartment;
    }
}
