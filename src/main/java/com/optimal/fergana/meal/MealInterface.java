package com.optimal.fergana.meal;

public interface MealInterface {

    default MealResDTO parse(Meal meal){
        return new MealResDTO(
                meal.getId(),
                meal.getName(),
                meal.getWeight(),
                meal.getDelete(),
                meal.getComment(),
                meal.getProtein(),
                meal.getKcal(),
                meal.getOil(),
                meal.getCarbohydrates(),
                meal.getMealCategory().getName(),
                meal.getMealCategory().getId(),
                meal.getRegionalDepartment().getName(),
                meal.getRegionalDepartment().getId(),
                meal.getAttachment() != null ? meal.getAttachment().getPath() :"",
                meal.getAttachment()!= null ? meal.getAttachment().getId():null
        );
    }
}
