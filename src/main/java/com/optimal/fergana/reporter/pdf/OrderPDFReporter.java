package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.multiMenu.product.ProductWeight;
import com.optimal.fergana.order.MyOrder;
import com.optimal.fergana.order.OrderRepo;
import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.order.product.ProductOrder;
import com.optimal.fergana.product.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;


@Service
@RequiredArgsConstructor
public class OrderPDFReporter {

    private final OrderRepo orderRepo;

    public String createPDFMenu(String paragraph, Integer orderId) throws FileNotFoundException, DocumentException {

        String path = DEST + orderId + "Buyurtma_xisobot.pdf";

        File file = new File(path);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(path));
        document.open();
        document.add(new Paragraph(paragraph + " \n \n "));


        Optional<MyOrder> optionalMyOrder = orderRepo.findById(orderId);
        if (optionalMyOrder.isPresent()) {
            MyOrder myOrder = optionalMyOrder.get();
            Set<ProductOrder> productOrders = myOrder.getKindergartenOrderList().get(0).getProductOrders();
            int productSize = productOrders.size();


            float[] aa = new float[2 + productSize];
            aa[0] = (1.5F);
            aa[1] = (5F);

            for (int i = 2; i < productSize + 2; i++) {
                aa[i] = (2F);
            }

            PdfPTable table = new PdfPTable(aa);
            table.setWidthPercentage(105);
            int fontSize = 7;
            table.addCell(createCell("T/R", 1, fontSize, 1));
            table.addCell(createCell("MTT", 1, fontSize, 1));

            List<ProductOrder> productOrderList1 = productOrders.stream().sorted(Comparator.comparing(ProductOrder::getProductName)).toList();




            for (ProductOrder productOrder : productOrderList1) {
                table.addCell(createCell(productOrder.getProduct().getName(), 1, fontSize, 1));
            }

            int tr = 0;
            List<KindergartenOrder> kindergartenOrderList = myOrder.getKindergartenOrderList();

            List<ProductOrder> list = new ArrayList<>();

            for (KindergartenOrder kindergartenOrder : kindergartenOrderList) {
                table.addCell(createCell(String.valueOf(++tr), 1, fontSize, 1));
                table.addCell(createCell(kindergartenOrder.getKindergarten().getNumber() + kindergartenOrder.getKindergarten().getName(), 1, fontSize, 1));

                Set<ProductOrder> productOrders1 = kindergartenOrder.getProductOrders();
                List<ProductOrder> productOrderList = productOrders1.stream().sorted(Comparator.comparing(ProductOrder::getProductName)).toList();

                for (ProductOrder productOrder :productOrderList) {
                    table.addCell(createCell(String.valueOf(productOrder.getPackWeight().setScale(2, RoundingMode.HALF_UP)), 1, fontSize, 1));
                    BigDecimal packWeight = productOrder.getPackWeight().setScale(2, RoundingMode.HALF_UP);
                    addList(productOrder.getProduct(), list, packWeight);
                }
            }

            list.sort(Comparator.comparing(ProductOrder::getProductName));

            table.addCell(createCell("Jami: ", 1, fontSize, 2));
            for (ProductOrder productOrder : list) {
                table.addCell(createCell(productOrder.getPackWeight().setScale(2, RoundingMode.HALF_UP).toString(), 1, fontSize, 1));
            }
            document.add(table);
        }

        document.close();
        return path;
    }

    public void addList(Product product, List<ProductOrder> list, BigDecimal weightPack) {

        boolean res = true;
        for (ProductOrder productOrder : list) {
            if (productOrder.getProduct().getId().equals(product.getId())) {
                productOrder.setPackWeight(productOrder.getPackWeight().add(weightPack));
                res = false;
            }
        }
        if (res) {
            list.add(new ProductOrder(weightPack, product));
        }
    }
}