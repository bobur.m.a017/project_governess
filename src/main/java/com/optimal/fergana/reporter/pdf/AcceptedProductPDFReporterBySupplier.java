package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.product.acceptedProducts.AcceptedProduct;
import com.optimal.fergana.product.acceptedProducts.AcceptedProductRepo;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
@RequiredArgsConstructor
public class AcceptedProductPDFReporterBySupplier {
    private final AcceptedProductRepo acceptedProductRepo;
    private final UserService userService;


    public String createPDFTable(Integer productId, Integer supplierId, Integer departmentId, Long startDate, Long endDate, HttpServletRequest request) throws FileNotFoundException, DocumentException {

        String path = DEST + System.currentTimeMillis() + ".pdf";
        File file = new File(path);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4.rotate());
        PdfWriter.getInstance(document, new FileOutputStream(path));
        document.open();

        float[] aa = new float[6];

        aa[0] = (1F);
        aa[1] = (3F);
        aa[2] = (3F);
        aa[3] = (2F);
        aa[4] = (2F);
        aa[5] = (5F);


        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(100);

        int fontSize = 7;


        table.addCell(createCell("T/R", 1, fontSize,1));
        table.addCell(createCell("Maxsulot Nomi", 1, fontSize,1));
        table.addCell(createCell("Sana", 1, fontSize,1));
        table.addCell(createCell("O'lchov birligi", 1, fontSize,1));
        table.addCell(createCell("Miqdori", 1, fontSize,1));
        table.addCell(createCell("Yetkazib beruvchi tashkilot", 1, fontSize,1));





        Users user = userService.parseToken(request);
        if (departmentId == null) {
            departmentId = user.getDepartment().getId();
        }


        List<AcceptedProduct> productList;


        if (productId == null) {


            if (startDate != null && endDate != null) {
                Date date1 = new Date(startDate);
                Date date2 = new Date(endDate);

                if (supplierId != null) {
                    productList = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndSupplier_IdAndDateBetween(departmentId, supplierId, date1, date2);
                } else {
                    productList = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndDateBetween(departmentId, date1, date2);
                }
            } else {
                productList = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_Id(departmentId);
            }
        } else {

            if (startDate != null && endDate != null) {
                Date date1 = new Date(startDate);
                Date date2 = new Date(endDate);

                if (supplierId != null) {
                    productList = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndSupplier_IdAndDateBetweenAndProduct_Id(departmentId, supplierId, date1, date2, productId);
                } else {
                    productList = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndDateBetweenAndProduct_Id(departmentId, date1, date2, productId);
                }
            } else {
                productList = acceptedProductRepo.findAllByDeleteIsFalseAndDepartment_IdAndProduct_Id(departmentId, productId);
            }

        }


        int tr=0;

        for (AcceptedProduct acceptedProduct : productList) {
            table.addCell(createCell(String.valueOf(++tr), 1, fontSize,1));
            table.addCell(createCell(acceptedProduct.getProduct().getName(), 1, fontSize,1));
            table.addCell(createCell(String.valueOf(acceptedProduct.getDate()), 1, fontSize,1));
            table.addCell(createCell(acceptedProduct.getProduct().getMeasurementType(), 1, fontSize,1));
            table.addCell(createCell(String.valueOf(acceptedProduct.getWeight()), 1, fontSize,1));
            table.addCell(createCell(String.valueOf(acceptedProduct.getSupplier().getName()), 1, fontSize,1));
        }


        document.add(table);
        document.newPage();
        document.close();
        return path;
    }

}
