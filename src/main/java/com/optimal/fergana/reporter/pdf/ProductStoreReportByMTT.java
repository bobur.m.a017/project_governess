package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.kindergarten.KindergartenRepo;
import com.optimal.fergana.warehouse.Warehouse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;


@Service
@RequiredArgsConstructor
public class ProductStoreReportByMTT {

    private final KindergartenRepo kindergartenRepo;
    private final DepartmentRepo departmentRepo;

    public String createPDFOmbor(Department department) {


        String path = DEST + department.getId() + "OmborQoldiq.pdf";
        File file = new File(path);
        file.getParentFile().mkdirs();
        Document document = new Document();
        document.setPageSize(PageSize.A4);

        try {
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();
            Paragraph paragraph = new Paragraph(department.getName() + " \n" + LocalDate.now() + " kun xolatiga omborda mavjud maxsulotlar\n \n ");
            paragraph.setAlignment(Element.ALIGN_CENTER);

            document.add(paragraph);

            document.add(createTableMenu(department));
            document.newPage();

        } catch (Exception e) {
            return "";
        }

        document.close();
        return path;
    }

    private PdfPTable createTableMenu(Department department) {

        float[] aa = new float[3];


        aa[0] = (5F);
        aa[1] = (3F);
        aa[2] = (5F);

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(105);

        int fontSize = 7;

        PdfPCell ovqatlar_vaqti = createCell("Mahsulot Nomi", 1, 8, 1);
        table.addCell(ovqatlar_vaqti);
        table.addCell(createCell("O'lchov birligi", 1, 8, 1));
//        table.addCell(createCell("Qadoq miqdori", 1, 8, 1));
        table.addCell(createCell("Mahsulot Miqdori", 1, 8, 1));

        List<Warehouse> warehouseList = department.getWarehouseList();
        for (Warehouse warehouse : warehouseList) {
            table.addCell(createCell(warehouse.getProduct().getName(), 1, fontSize, 1));
            table.addCell(createCell(warehouse.getProduct().getMeasurementType(), 1, fontSize, 1));
            table.addCell(createCell(warehouse.getTotalWeight().setScale(2, RoundingMode.HALF_UP).toString(), 1, fontSize, 1));
        }
        return table;
    }


//    private String parseString(BigDecimal sum) {
//        return sum.toString().replace(".", ",");
//    }

}
