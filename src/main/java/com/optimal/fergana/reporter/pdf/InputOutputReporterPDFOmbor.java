package com.optimal.fergana.reporter.pdf;


import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.inputOutput.InputOutput;
import com.optimal.fergana.inputOutput.InputOutputRepo;
import com.optimal.fergana.inputOutput.dto.DateInputOutPutDTO;
import com.optimal.fergana.inputOutput.dto.InputOutputDTO;
import com.optimal.fergana.inputOutput.inOut.InOut;
import com.optimal.fergana.kindergarten.Kindergarten;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
@RequiredArgsConstructor
public class InputOutputReporterPDFOmbor {

    private final InputOutputRepo inputOutputRepo;

    public String createInputOutputPDF(LocalDate startDate, LocalDate endDate, Department department) {

        String path = DEST + department.getName() + "_kirim_chiqim.pdf";

        try {
            File file = new File(path);
            file.getParentFile().mkdirs();
            Document document = new Document();
            document.setPageSize(PageSize.A4.rotate());
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();

            Paragraph paragraph = new Paragraph(department.getName() + "ning  " + (startDate.toString().replace('-', '.')) + " - " + (endDate.minusDays(1).toString().replace('-', '.')) + "  kunlar oralig`idagi kirim chiqim xisoboti. \n\n\n");
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            PdfPTable tableMenu = createTableMenu(department, startDate, endDate);
            if (tableMenu != null) {
                document.add(tableMenu);
                document.close();
                return path;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private PdfPTable createTableMenu(Department department, LocalDate start, LocalDate end) {

        List<InputOutputDTO> product = getProduct(department, start, end);

        if (product.size() > 0) {

            int size = 2 + (product.get(0).getDateList().size() * 3);
            float[] aa = new float[size];

            aa[0] = (1F);
            aa[1] = (3F);
            for (int i = 2; i < size; i++) {
                aa[i] = (2F);
            }
            PdfPTable table = new PdfPTable(aa);

            table.setWidthPercentage(100);

            int fontSize = 8;

            table.addCell(createCell("T/r", 2, fontSize, 1));
            table.addCell(createCell("Maxsulot nomi", 2, fontSize, 1));

            for (DateInputOutPutDTO dto : product.get(0).getDateList()) {
                table.addCell(createCell(dto.getDate().toString().replace('-', '.'), 1, fontSize, 3));
            }

            for (DateInputOutPutDTO dto : product.get(0).getDateList()) {
                table.addCell(createCell("Kirim", 1, fontSize, 1));
                table.addCell(createCell("Chiqim", 1, fontSize, 1));
                table.addCell(createCell("Qoldiq", 1, fontSize, 1));
            }


            int num = 1;

            for (InputOutputDTO dto : product) {
                table.addCell(createCell(String.valueOf(num++), 1, fontSize, 1));
                table.addCell(createCell(dto.getProductName(), 1, fontSize, 1));

                for (DateInputOutPutDTO dateInputOutPutDTO : dto.getDateList()) {
                    table.addCell(createCell(dateInputOutPutDTO.getInput().compareTo(BigDecimal.ZERO) == 0?"":dateInputOutPutDTO.getInput().setScale(2, RoundingMode.HALF_UP).toString(), 1, fontSize, 1));
                    table.addCell(createCell(dateInputOutPutDTO.getOutput().compareTo(BigDecimal.ZERO) == 0?"":dateInputOutPutDTO.getOutput().setScale(2, RoundingMode.HALF_UP).toString(), 1, fontSize, 1));
                    table.addCell(createCell(dateInputOutPutDTO.getResidual().compareTo(BigDecimal.ZERO) == 0?"":dateInputOutPutDTO.getResidual().setScale(2, RoundingMode.HALF_UP).toString(), 1, fontSize, 1));
                }
            }

            return table;
        }
        return null;
    }

    public List<InputOutputDTO> getProduct(Department department, LocalDate start, LocalDate end) {
        List<InputOutput> list = inputOutputRepo.findAllByDepartment_IdAndLocalDateBetween(department.getId(), start, end);
        List<InputOutputDTO> dtoList = new ArrayList<>();

        for (InputOutput inputOutput : list) {
            for (InOut inOut : inputOutput.getInOuts()) {
                boolean res = true;
                for (InputOutputDTO inputOutputDTO : dtoList) {
                    if (inputOutputDTO.getProductId().equals(inOut.getProduct().getId())) {
                        for (DateInputOutPutDTO dateDTO : inputOutputDTO.getDateList()) {
                            if (dateDTO.getDate().equals(inputOutput.getLocalDate())) {
                                dateDTO.setInput(dateDTO.getInput().add(inOut.getInputWeight()));
                                dateDTO.setOutput(dateDTO.getOutput().add(inOut.getOutputWeight()));
                                dateDTO.setResidual(dateDTO.getResidual().add(inOut.getResidual()));
                            }
                        }
                        res = false;
                    }
                }
                if (res) {
                    dtoList.add(new InputOutputDTO(inOut.getProduct().getId(), inOut.getProduct().getName(), getDate(start, end)));

                    for (InputOutputDTO inputOutputDTO : dtoList) {
                        if (inputOutputDTO.getProductId().equals(inOut.getProduct().getId())) {
                            for (DateInputOutPutDTO dateDTO : inputOutputDTO.getDateList()) {
                                if (dateDTO.getDate().equals(inputOutput.getLocalDate())) {
                                    dateDTO.setInput(dateDTO.getInput().add(inOut.getInputWeight()));
                                    dateDTO.setOutput(dateDTO.getOutput().add(inOut.getOutputWeight()));
                                    dateDTO.setResidual(dateDTO.getResidual().add(inOut.getResidual()));
                                }
                            }
                        }
                    }
                }
            }
        }

        return dtoList;
    }

    public List<DateInputOutPutDTO> getDate(LocalDate start, LocalDate end) {

        List<LocalDate> dateList = start.datesUntil(end).sorted().toList();
        List<DateInputOutPutDTO> dateDTOList = new ArrayList<>();

        for (LocalDate localDate : dateList) {
            dateDTOList.add(new DateInputOutPutDTO(localDate, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO));
        }

        dateDTOList.sort(Comparator.comparing(DateInputOutPutDTO::getDate));
        return dateDTOList;
    }
}
