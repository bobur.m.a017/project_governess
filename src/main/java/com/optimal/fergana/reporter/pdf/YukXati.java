package com.optimal.fergana.reporter.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.warehouse.delivery.Delivery;
import com.optimal.fergana.warehouse.delivery.ProductDelivery;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.optimal.fergana.statics.StaticMethods.createCell;
import static com.optimal.fergana.statics.StaticMethods.createCellNoBorder;
import static com.optimal.fergana.statics.StaticWords.DEST;

@Service
public class YukXati {

    public String getFileProduct(Delivery delivery) {


        String path = DEST + delivery.getId() + ".pdf";


        try {


            File file = new File(path);
            file.getParentFile().mkdirs();
            Document document = new Document();
            document.setPageSize(PageSize.A4);
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();


            Paragraph paragraph0 = new Paragraph(delivery.getDate().toLocalDate().toString());
            paragraph0.setAlignment(Element.ALIGN_RIGHT);
            document.add(paragraph0);

            Paragraph paragraph = new Paragraph("Xisobvaraq faktura raqami ______");
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);

            Paragraph paragraph1 = new Paragraph("Shartnoma raqami ______\n\n");
            paragraph1.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph1);

            document.add(createTable(delivery));
            document.add(createTableProduct(delivery));
            document.add(createTableTagi());


            document.close();


        } catch (Exception e) {

            e.printStackTrace();

        }
        return path;
    }

    private PdfPTable createTable(Delivery delivery) {

        int size = 3;
        float[] aa = new float[size];

        aa[0] = (10F);
        aa[1] = (1F);
        aa[2] = (10F);

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(100);

        int fontSize = 12;

        Department department = delivery.getDepartment();

        table.addCell(createCellNoBorder("Qayerdan:", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Qayerga:", 1, fontSize, 1));

        table.addCell(createCellNoBorder("Tashkilot nomi: " + department.getName(), 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Tashkilot nomi: " + delivery.getKindergarten().getNumber() + delivery.getKindergarten().getName(), 1, fontSize, 1));

        table.addCell(createCellNoBorder("Manzil: " + department.getAddress(), 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Manzil: " + delivery.getKindergarten().getDistrict().getName(), 1, fontSize, 1));

        table.addCell(createCellNoBorder("Telefon: +998 95 195-22-20", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("STIR: " + department.getStir(), 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Bank: " + department.getBank(), 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("MFO: " + department.getMfo(), 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("X/r: " + department.getXr(), 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Avtotransport raqami: " + delivery.getDeliver()+"\n\n", 1, fontSize, 1));

        return table;
    }

    private PdfPTable createTableTagi() {

        int size = 3;
        float[] aa = new float[size];

        aa[0] = (10F);
        aa[1] = (10F);
        aa[2] = (10F);

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(100);

        int fontSize = 12;

        table.addCell(createCellNoBorder(" ", 1, fontSize, 1));
        table.addCell(createCellNoBorder(" ", 1, fontSize, 1));
        table.addCell(createCellNoBorder(" ", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Ombor:", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Maxsulot yetkazishga masul:", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Oshpaz:", 1, fontSize, 1));
        table.addCell(createCellNoBorder(" ", 1, fontSize, 1));
        table.addCell(createCellNoBorder(" ", 1, fontSize, 1));
        table.addCell(createCellNoBorder(" ", 1, fontSize, 1));

        table.addCell(createCellNoBorder("Topshirdi:", 1, fontSize, 1));
        table.addCell(createCellNoBorder("", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Qabul qildi:", 1, fontSize, 1));

        table.addCell(createCellNoBorder("F.I.Sh._____________________" , 1, fontSize, 1));
        table.addCell(createCellNoBorder("F.I.Sh._____________________" , 1, fontSize, 1));
        table.addCell(createCellNoBorder("F.I.Sh._____________________", 1, fontSize, 1));

        table.addCell(createCellNoBorder("Imzo:_____________", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Imzo:_____________", 1, fontSize, 1));
        table.addCell(createCellNoBorder("Imzo:_____________", 1, fontSize, 1));
        return table;
    }

    private PdfPTable createTableProduct(Delivery delivery) {


        int size = 4;
        float[] aa = new float[size];

        aa[0] = (1F);
        for (int i = 1; i < size; i++) {
            aa[i] = (5F);
        }

        PdfPTable table = new PdfPTable(aa);
        table.setWidthPercentage(100);

        int fontSize = 12;

        table.addCell(createCell("T/r", 1, fontSize, 1));
        table.addCell(createCell("Maxsulot nomi", 1, fontSize, 1));
        table.addCell(createCell("O‘lchov birligi", 1, fontSize, 1));
        table.addCell(createCell("Miqdori", 1, fontSize, 1));


        int num = 1;

        for (ProductDelivery productDelivery : delivery.getProductDeliveryList()) {
            table.addCell(createCell(String.valueOf(num++), 1, fontSize, 1));
            table.addCell(createCell(nameCustom(productDelivery.getProduct().getName(), 20), 1, fontSize, 1));
            table.addCell(createCell(productDelivery.getPack().compareTo(BigDecimal.ZERO) == 0 ? "kg":"dona", 1, fontSize, 1));
            table.addCell(createCell(productDelivery.getPackWeight().setScale(2, RoundingMode.HALF_UP).toString(), 1, fontSize, 1));
        }
        return table;
    }

    public String nameCustom(String name, int number) {
        if (name.length() <= number)
            return name;
        return name.substring(0, number - 3) + "...";
    }
}
