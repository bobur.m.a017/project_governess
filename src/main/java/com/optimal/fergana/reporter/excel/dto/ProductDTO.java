package com.optimal.fergana.reporter.excel.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProductDTO {
    private Integer id;
    private String name;
    private List<DateDTO> dateList;
    private boolean state = false;

    public ProductDTO(Integer id, String name, List<DateDTO> dateList) {
        this.id = id;
        this.name = name;
        this.dateList = dateList;
    }

    public ProductDTO() {
    }
}
