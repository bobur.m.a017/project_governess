package com.optimal.fergana.reporter.excel;

import com.optimal.fergana.order.MyOrder;
import com.optimal.fergana.order.OrderRepo;
import com.optimal.fergana.order.kindergarten.KindergartenOrder;
import com.optimal.fergana.order.product.ProductOrder;
import com.optimal.fergana.product.Product;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.*;

import static com.optimal.fergana.statics.StaticMethods.paragraph;
import static com.optimal.fergana.statics.StaticMethods.style;
import static com.optimal.fergana.statics.StaticWords.DEST;


@Service
@RequiredArgsConstructor
public class OrderEcxelReporter {

    private final OrderRepo orderRepo;

    public String createExcel(Integer orderId){
        try {
            String path = DEST + orderId + "Buyurtma_xisobot.xlsx";

            // workbook object
            XSSFWorkbook workbook = new XSSFWorkbook();

            int fontHeight = 12;

            //CELL STYLE
            XSSFFont font = workbook.createFont();
            font.setFontName("Times New Roman");
            font.setFontHeight(fontHeight);


            XSSFFont fontParagraph = workbook.createFont();
            fontParagraph.setFontName("Times New Roman");
            fontParagraph.setFontHeight(fontHeight);
            fontParagraph.setBold(true);

            XSSFFont fontBold = workbook.createFont();
            fontBold.setFontName("Times New Roman");
            fontBold.setFontHeight(fontHeight);
            fontBold.setBold(true);

            DataFormat format = workbook.createDataFormat();

            short shortFormat = format.getFormat("0.00");
            short shortFormatNumber = format.getFormat("0");

            XSSFCellStyle cellStyle = workbook.createCellStyle();
            style(font, cellStyle);
            cellStyle.setRotation((short) 0);
            cellStyle.setDataFormat(shortFormat);

            XSSFCellStyle cellStyleNumber = workbook.createCellStyle();
            style(font, cellStyleNumber);
            cellStyleNumber.setRotation((short) 0);
            cellStyleNumber.setDataFormat(shortFormatNumber);


            XSSFCellStyle cellStyleBold = workbook.createCellStyle();
            style(fontBold, cellStyleBold);
            cellStyleBold.setRotation((short) 0);
            cellStyle.setDataFormat(shortFormat);

            XSSFCellStyle paragraphStyle = workbook.createCellStyle();
            paragraph(fontParagraph, paragraphStyle);
            paragraphStyle.setRotation((short) 0);
            paragraphStyle.setDataFormat(shortFormat);


            Optional<MyOrder> optionalMyOrder = orderRepo.findById(orderId);

            if (optionalMyOrder.isPresent()) {

                MyOrder myOrder = optionalMyOrder.get();

                Set<ProductOrder> productOrders = myOrder.getKindergartenOrderList().get(0).getProductOrders();
                List<ProductOrder> productOrderList1 = productOrders.stream().sorted(Comparator.comparing(ProductOrder::getProductName)).toList();

                XSSFSheet sheet = workbook.createSheet("Buyurtma");

                sheet.setColumnWidth(0, 1000);
                sheet.setColumnWidth(1, 4000);

                for (int i = 2; i < productOrderList1.size() + 2; i++) {
                    sheet.setColumnWidth(i, 3500);
                }


                int rowNum = 0;
                int columnIndex = 0;


                XSSFRow row0 = sheet.createRow(rowNum);
                row0.setHeight((short) 500);
                sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum++, 0, productOrderList1.size() + 1));

                XSSFCell cell0 = row0.createCell(0);
                cell0.setCellValue(myOrder.getName());
                cell0.setCellStyle(paragraphStyle);


                XSSFRow row = sheet.createRow(rowNum++);
                row.setHeight((short) 1000);
                sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 0));

                XSSFCell cell = row.createCell(columnIndex++);
                cell.setCellValue("T/R");
                cell.setCellStyle(cellStyleBold);

                XSSFCell cell1 = row.createCell(columnIndex++);
                cell1.setCellValue("MTT");
                cell1.setCellStyle(cellStyleBold);

                for (ProductOrder productOrder : productOrderList1) {

                    XSSFCell cell2 = row.createCell(columnIndex++);
                    cell2.setCellValue(productOrder.getProduct().getName());
                    cell2.setCellStyle(cellStyleBold);

                }

                int tr = 0;
                List<KindergartenOrder> kindergartenOrderList = myOrder.getKindergartenOrderList();

                List<ProductOrder> list = new ArrayList<>();

                for (KindergartenOrder kindergartenOrder : kindergartenOrderList) {

                    columnIndex = 0;

                    XSSFRow row1 = sheet.createRow(rowNum);
                    sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum++, 0, 0));

                    XSSFCell cell3 = row1.createCell(columnIndex++);
                    cell3.setCellValue(++tr);
                    cell3.setCellStyle(cellStyleNumber);

                    XSSFCell cell4 = row1.createCell(columnIndex++);
                    cell4.setCellValue(kindergartenOrder.getKindergarten().getNumber() + kindergartenOrder.getKindergarten().getName());
                    cell4.setCellStyle(cellStyle);

                    Set<ProductOrder> productOrders1 = kindergartenOrder.getProductOrders();
                    List<ProductOrder> productOrderList = productOrders1.stream().sorted(Comparator.comparing(ProductOrder::getProductName)).toList();

                    for (ProductOrder productOrder : productOrderList) {

                        BigDecimal packWeight = productOrder.getPackWeight();
                        addList(productOrder.getProduct(), list, packWeight);
                        XSSFCell cell5 = row1.createCell(columnIndex++);
                        cell5.setCellValue(getNumber(packWeight));
                        cell5.setCellStyle(cellStyle);

                    }
                }

                list.sort(Comparator.comparing(ProductOrder::getProductName));

                XSSFRow row1 = sheet.createRow(rowNum);
                sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum++, 0, 1));


                columnIndex = 0;

                XSSFCell cell3 = row1.createCell(columnIndex++);
                cell3.setCellValue("Jami:");
                cell3.setCellStyle(cellStyle);

                XSSFCell cell31 = row1.createCell(columnIndex++);
                cell31.setCellStyle(cellStyle);


                for (ProductOrder productOrder : list) {

                    XSSFCell cell5 = row1.createCell(columnIndex++);
                    cell5.setCellValue(getNumber(productOrder.getPackWeight()));
                    cell5.setCellStyle(cellStyle);
                }

                FileOutputStream out = new FileOutputStream(path);
                workbook.write(out);
                out.close();
            }
            return path;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public double getNumber(BigDecimal num) {

//        return num.toString().replace(".", ",");
        return num.doubleValue();
    }

    public void addList(Product product, List<ProductOrder> list, BigDecimal weightPack) {

        boolean res = true;
        for (ProductOrder productOrder : list) {
            if (productOrder.getProduct().getId().equals(product.getId())) {
                productOrder.setPackWeight(productOrder.getPackWeight().add(weightPack));
                res = false;
            }
        }
        if (res) {
            list.add(new ProductOrder(weightPack, product));
        }
    }
}