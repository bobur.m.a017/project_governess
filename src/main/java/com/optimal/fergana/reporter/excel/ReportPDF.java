package com.optimal.fergana.reporter.excel;

import com.optimal.fergana.address.district.District;
import com.optimal.fergana.address.district.DistrictRepo;
import com.optimal.fergana.ageGroup.AgeGroup;
import com.optimal.fergana.ageGroup.AgeGroupRepo;
import com.optimal.fergana.department.Department;
import com.optimal.fergana.department.DepartmentRepo;
import com.optimal.fergana.kids.kidsSub.KidsNumberSub;
import com.optimal.fergana.kids.kidsSub.KidsNumberSubRepo;
import com.optimal.fergana.kindergarten.Kindergarten;
import com.optimal.fergana.meal.Meal;
import com.optimal.fergana.menu.Menu;
import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.menu.ageStandard.AgeStandardRepo;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.product.ProductRepo;
import com.optimal.fergana.product.productPack.ProductPackService;
import com.optimal.fergana.product.product_price.ProductPrice;
import com.optimal.fergana.product.product_price.ProductPriceRepo;
import com.optimal.fergana.productMeal.ProductMeal;
import com.optimal.fergana.report.Report;
import com.optimal.fergana.report.ReportRepo;
import com.optimal.fergana.reporter.excel.dto.DateDTO;
import com.optimal.fergana.reporter.excel.dto.ProductDTO;
import com.optimal.fergana.warehouse.kindergarten.reserve.AmountSpent;
import com.optimal.fergana.warehouse.kindergarten.reserve.AmountSpentRepo;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.PaperSize;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.optimal.fergana.statics.StaticMethods.*;
import static com.optimal.fergana.statics.StaticWords.DEST;


@Service
@RequiredArgsConstructor
public class ReportPDF {


    private final ReportRepo reportRepo;
    private final ProductRepo productRepo;
    private final KidsNumberSubRepo kidsNumberSubRepo;
    private final AgeStandardRepo ageStandardRepo;
    private final AgeGroupRepo ageGroupRepo;
    private final ProductPriceRepo productPriceRepo;
    private final DistrictRepo districtRepo;
    private final DepartmentRepo departmentRepo;
    private final AmountSpentRepo amountSpentRepo;
    private final ProductPackService productPackService;

    private final int scale = 6;
    private final int scale6 = 6;
    private final int fontHeight = 12;


    public String getReportFile(LocalDate start, LocalDate end, List<Kindergarten> list, Integer departmentId, Integer districtId, Integer year, Integer month, Integer ustama) throws IOException {


        Optional<District> optionalDistrict = districtRepo.findById(districtId);

        Optional<Department> optionalDepartment = departmentRepo.findById(departmentId);
        String path = DEST + System.currentTimeMillis() + ".xlsx";

        if (optionalDistrict.isPresent() && optionalDepartment.isPresent()) {

            Department department = optionalDepartment.get();
            District district = optionalDistrict.get();

            // workbook object
            XSSFWorkbook workbook = new XSSFWorkbook();

            //CELL STYLE
            XSSFFont font = workbook.createFont();
            font.setFontName("Times New Roman");
            font.setFontHeight(fontHeight);


            XSSFFont fontParagraph = workbook.createFont();
            fontParagraph.setFontName("Times New Roman");
            fontParagraph.setFontHeight(fontHeight);
            fontParagraph.setBold(true);

            List<LocalDate> localDates;
            if (start.isEqual(end)) {
                localDates = List.of(start);
            } else {
                localDates = start.datesUntil(end.plusDays(1)).toList();
            }


            List<DateDTO> uchYetti = getDateList(localDates);
            List<DateDTO> qisqa = getDateList(localDates);
            List<DateDTO> xodim = getDateList(localDates);
            List<DateDTO> jami = getDateList(localDates);


//            List<DateDTO> uchTortQatnov = getDateList(localDates);
            List<DateDTO> tortYettiQatnov = getDateList(localDates);
            List<DateDTO> qisqaQatnov = getDateList(localDates);


            List<AgeGroup> all = ageGroupRepo.findAll();

            DataFormat format = workbook.createDataFormat();

            short shortFormat = format.getFormat("0.00");
            short shortFormatWeight = format.getFormat("0.00");
            short shortFormatNumber = format.getFormat("0");


            if (list.size() > 1) {
                for (Kindergarten kindergarten : list) {
                    getTable(shortFormatWeight, shortFormat, workbook, font, fontParagraph, start, end, localDates, List.of(kindergarten), all, departmentId, districtId, year, month, null, null, null, null, null, null, false);
                }
            }

            BigDecimal zaxiraSumma = getTable(shortFormatWeight, shortFormat, workbook, font, fontParagraph, start, end, localDates, list, all, departmentId, districtId, year, month, uchYetti, qisqa, jami, xodim, qisqaQatnov, tortYettiQatnov, true);


            String paragraph = (list.get(0).getDistrict().getName() + "da" + " " + start.toString().replace('-', '.') + " - " + end.toString().replace('-', '.') + " kunlarida sarflangan oziq ovqat maxsulotlari to‘g‘risida ma‘lumot");


            getTableTotal(shortFormatNumber, shortFormat, "3-7 yosh", paragraph, "3-4 va 4-7 yoshli guruxlarda", uchYetti, workbook, font, fontParagraph, ustama, district, department);
            getTableTotal(shortFormatNumber, shortFormat, "Qisqa muddatli", paragraph, "Qisqa muddatli guruxlarda", qisqa, workbook, font, fontParagraph, ustama, district, department);
            getTableTotal(shortFormatNumber, shortFormat, "Xodim", paragraph, "Xodimlarda", xodim, workbook, font, fontParagraph, ustama, district, department);
            getTableTotal(shortFormatNumber, shortFormat, "Jami (3-7 va qisqa muddatli)", paragraph, "", jami, workbook, font, fontParagraph, ustama, district, department);


            getTableQatnov(shortFormat, localDates, qisqaQatnov, tortYettiQatnov, workbook, font, fontParagraph, ustama, district, department, start, end, zaxiraSumma);


            FileOutputStream out = new FileOutputStream(path);
            workbook.write(out);
            out.close();
        }
        return path;
    }

    public BigDecimal getTable(short formatWeight, short format, XSSFWorkbook workbook, XSSFFont font, XSSFFont fontParagraph, LocalDate start, LocalDate end, List<LocalDate> localDates, List<Kindergarten> kindergartenList, List<AgeGroup> ageGroupList, Integer departmentId, Integer districtId, Integer year, Integer month, List<DateDTO> uchYetti, List<DateDTO> qisqa, List<DateDTO> jami, List<DateDTO> xodim, List<DateDTO> qisqaQatnov, List<DateDTO> tortYettiQatnov, boolean result) {

        BigDecimal zaxiraSumma = BigDecimal.ZERO;


        XSSFFont fontBold = workbook.createFont();
        fontBold.setFontName("Times New Roman");
        fontBold.setFontHeight(fontHeight);
        fontBold.setBold(true);

        int column = localDates.size() + 5;


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        style(font, cellStyle);
        cellStyle.setRotation((short) 0);
        cellStyle.setDataFormat(format);

        XSSFCellStyle cellStyleWeight = workbook.createCellStyle();
        style(font, cellStyleWeight);
        cellStyleWeight.setRotation((short) 0);
        cellStyleWeight.setDataFormat(formatWeight);


        XSSFCellStyle cellStyleBold = workbook.createCellStyle();
        style(fontBold, cellStyleBold);
        cellStyleBold.setRotation((short) 0);
        cellStyle.setDataFormat(format);


        XSSFCellStyle cellStyleBoldNoBorder = workbook.createCellStyle();
        styleNoBorder(fontBold, cellStyleBoldNoBorder);
        cellStyleBoldNoBorder.setRotation((short) 0);
        cellStyle.setDataFormat(format);


        XSSFCellStyle paragraphStyle = workbook.createCellStyle();
        paragraph(fontParagraph, paragraphStyle);
        paragraphStyle.setRotation((short) 0);
        cellStyle.setDataFormat(format);


        XSSFCellStyle cellStyleRotate = workbook.createCellStyle();
        style(font, cellStyleRotate);
        cellStyleRotate.setRotation((short) 90);
        cellStyle.setDataFormat(format);


        XSSFCellStyle cellStyleRotateBold = workbook.createCellStyle();
        style(fontBold, cellStyleRotateBold);
        cellStyleRotateBold.setRotation((short) 90);
        cellStyle.setDataFormat(format);


        XSSFSheet spreadsheet = workbook.createSheet(kindergartenList.size() == 1 ? kindergartenList.get(0).getNumber().toString() : "Jami MTT");

        spreadsheet.setColumnWidth(0, 1000);
        spreadsheet.setColumnWidth(1, 9000);
        spreadsheet.setColumnWidth(column - 1, 3000);
        spreadsheet.setColumnWidth(column - 2, 3000);
        spreadsheet.setColumnWidth(column - 3, 3000);


        Kindergarten kindergarten = null;
        String paragraph = "";
        if (kindergartenList.size() != 0) {
            kindergarten = kindergartenList.get(0);
            paragraph = kindergartenList.size() == 1 ? (kindergarten.getDistrict().getName() + " " + kindergarten.getNumber() + kindergarten.getName() + "da" + " " + localDates.get(0).toString().replace('-', '.') + " - " + localDates.get(localDates.size() - 1).toString().replace('-', '.') + " kunlarida sarflangan oziq ovqat maxsulotlari to‘g‘risida ma‘lumot") : (kindergarten.getDistrict().getName() + "da" + " " + localDates.get(0).toString().replace('-', '.') + " - " + localDates.get(localDates.size() - 1).toString().replace('-', '.') + " kunlarida sarflangan oziq ovqat maxsulotlari to‘g‘risida ma‘lumot");
        }

        XSSFRow row = spreadsheet.createRow(0);
        row.setHeight((short) 1000);
        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, column - 1));

        XSSFCell cell = row.createCell(0);
        cell.setCellValue(paragraph);
        cell.setCellStyle(paragraphStyle);

        XSSFRow row0 = spreadsheet.createRow(2);

        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 0));
//        spreadsheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));

        XSSFCell cell0 = row0.createCell(0);
        cell0.setCellValue("T/R");
        cell0.setCellStyle(cellStyleBold);

        XSSFCell cell1 = row0.createCell(1);
        cell1.setCellStyle(cellStyleBold);
        cell1.setCellValue("Maxsulot nomi");


        int num = 2;
        for (LocalDate localDate : localDates) {
            XSSFCell cell2 = row0.createCell(num++);
            cell2.setCellStyle(cellStyleRotateBold);

            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            String formattedDate = localDate.format(dateTimeFormatter);  //17-02-2022

            cell2.setCellValue(formattedDate);
        }


        XSSFCell cell2 = row0.createCell(num++);
        cell2.setCellValue("Jami");
        cell2.setCellStyle(cellStyleBold);


        XSSFCell cell3 = row0.createCell(num++);
        cell3.setCellValue("Narx");
        cell3.setCellStyle(cellStyleBold);

        XSSFCell cell4 = row0.createCell(num);
        cell4.setCellValue("Summa");
        cell4.setCellStyle(cellStyleBold);

        BigDecimal totalWeight = BigDecimal.ZERO;

        int rowNumber = 3;
        int number = 1;

        List<ProductDTO> productList = getProduct(kindergartenList, start, end, localDates, ageGroupList, uchYetti, qisqa, jami, xodim, districtId, year, month, qisqaQatnov, tortYettiQatnov);

        for (ProductDTO productDTO : productList) {
            BigDecimal checkPack = productPackService.checkPack(departmentId, productDTO.getId());

            if (checkPack != null) {
                for (DateDTO dateDTO : productDTO.getDateList()) {
                    dateDTO.setWeight(dateDTO.getWeight().divide(checkPack.divide(BigDecimal.valueOf(1000), 4, RoundingMode.HALF_UP), 4, RoundingMode.HALF_UP));
                }
            }
        }


        for (ProductDTO productDTO : productList) {
            XSSFRow row1 = spreadsheet.createRow(rowNumber);
            spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber++, 0, 0));

            XSSFCell cell5 = row1.createCell(0);
            cell5.setCellValue(number++);
            cell5.setCellStyle(cellStyleBold);

            XSSFCell cell10 = row1.createCell(1);
            cell10.setCellValue(productDTO.getName());
            cell10.setCellStyle(cellStyleBold);

            int number1 = 2;
            BigDecimal totalSum = BigDecimal.ZERO;


            BigDecimal checkWeight = BigDecimal.ZERO;


            for (DateDTO dateDTO : productDTO.getDateList()) {
                BigDecimal weight = dateDTO.getWeight().setScale(scale, RoundingMode.HALF_UP);
                totalSum = totalSum.add(weight);

                XSSFCell cell6 = row1.createCell(number1++);

                cell6.setCellValue(getNumber(weight));
                cell6.setCellStyle(cellStyleWeight);

                if (result) {
                    Optional<AmountSpent> optionalAmountSpent = amountSpentRepo.findByDateAndReserve_Product_IdAndReserve_Kindergarten_Id(dateDTO.getDate(), productDTO.getId(), kindergarten.getId());

                    if (optionalAmountSpent.isPresent()) {
                        AmountSpent amountSpent = optionalAmountSpent.get();

                        BigDecimal packWeight = amountSpent.getPackWeight();
                        if (packWeight.compareTo(weight) > 0) {
                            checkWeight = checkWeight.add(weight);
                        } else {
                            checkWeight = checkWeight.add(packWeight);
                        }
                    }
//
//                    for (AmountSpent amountSpent : amountSpentRepo.findAllByDateBetweenAndReserve_Product_IdAndReserve_Kindergarten_Id(start, end, productDTO.getId(), kindergarten.getId())) {
//                        checkWeight = checkWeight.add(amountSpent.getPackWeight());
//                    }
//                    if (checkWeight.compareTo(BigDecimal.ZERO) > 0) {
//                        zaxiraSumma = zaxiraSumma.add(checkWeight.multiply(price));
//                    }
                }
            }


            XSSFCell cell7 = row1.createCell(number1++);
            cell7.setCellValue(getNumber(totalSum));
            cell7.setCellStyle(cellStyleWeight);

            Optional<ProductPrice> optional = productPriceRepo.findByDepartment_IdAndDistrict_IdAndYearAndMonthAndProduct_Id(departmentId, districtId, year, month, productDTO.getId());
            BigDecimal price = BigDecimal.ZERO;
            if (optional.isPresent()) {
                ProductPrice productPrice = optional.get();
                price = productPrice.getPrice();
            }

            XSSFCell cell8 = row1.createCell(number1++);
            cell8.setCellValue(getNumber(price));
            cell8.setCellStyle(cellStyle);

            if (checkWeight.compareTo(BigDecimal.ZERO) > 0) {
                zaxiraSumma = zaxiraSumma.add(checkWeight.multiply(price));
            }

            XSSFCell cell9 = row1.createCell(number1++);
            BigDecimal totalSum2 = (price.multiply(totalSum)).setScale(scale, RoundingMode.HALF_UP);


            totalWeight = totalWeight.add(totalSum2);

            cell9.setCellValue(getNumber(totalSum2));
            cell9.setCellStyle(cellStyle);
        }

        XSSFRow row1 = spreadsheet.createRow(rowNumber);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber++, 0, column - 2));
        XSSFCell cell5 = row1.createCell(0);
        cell5.setCellValue("Jami");
        cell5.setCellStyle(cellStyleBold);

        for (int i = 1; i <= column - 2; i++) {
            XSSFCell cell51 = row1.createCell(i);
            cell51.setCellStyle(cellStyleBold);
        }

        XSSFCell cell6 = row1.createCell(column - 1);
        cell6.setCellValue(getNumber(totalWeight));
        cell6.setCellStyle(cellStyleBold);

        rowNumber = rowNumber + 3;

        if (kindergartenList.size() > 1) {

            XSSFRow row2 = spreadsheet.createRow(rowNumber);
            spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
            XSSFCell cell7 = row2.createCell(1);
            cell7.setCellValue("Autsorser");
            cell7.setCellStyle(cellStyleBoldNoBorder);


            spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, column - 5, column - 2));
            XSSFCell cell8 = row2.createCell(column - 5);
            cell8.setCellValue("Istemolchi");
            cell8.setCellStyle(cellStyleBoldNoBorder);


            rowNumber += 2;

            XSSFRow row3 = spreadsheet.createRow(rowNumber);
            spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
            XSSFCell cell9 = row3.createCell(1);
            cell9.setCellValue(kindergarten.getDepartment().getName());
            cell9.setCellStyle(cellStyleBoldNoBorder);


            spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, column - 5, column - 2));
            XSSFCell cell10 = row3.createCell(column - 5);
            cell10.setCellValue(kindergarten.getDistrict().getName() + " MTB");
            cell10.setCellStyle(cellStyleBoldNoBorder);


            rowNumber += 2;

            XSSFRow row4 = spreadsheet.createRow(rowNumber);
            spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
            XSSFCell cell11 = row4.createCell(1);
            cell11.setCellValue("rahbari___________________________");
            cell11.setCellStyle(cellStyleBoldNoBorder);


            spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, column - 5, column - 2));
            XSSFCell cell12 = row4.createCell(column - 5);
            cell12.setCellValue("rahbari___________________________");
            cell12.setCellStyle(cellStyleBoldNoBorder);

        }
        return zaxiraSumma;
    }

    public List<ProductDTO> getProduct(List<Kindergarten> kindergartenList, LocalDate start, LocalDate end, List<LocalDate> localDates, List<AgeGroup> ageGroupList, List<DateDTO> uchYetti, List<DateDTO> qisqa, List<DateDTO> jami, List<DateDTO> xodim, Integer districtId, int year, int month, List<DateDTO> qisqaQatnov, List<DateDTO> tortYettiQatnov) {


        List<ProductDTO> productList = getProductList(localDates);

        for (Kindergarten kindergarten : kindergartenList) {
            List<Report> reportList = reportRepo.findAllByKindergarten_IdAndDateBetween(kindergarten.getId(), start, end);

            for (Report report : reportList) {
                Menu menu = report.getMenu();

                for (AgeGroup ageGroup : ageGroupList) {

                    Vector<AgeStandard> ageStandards = ageStandardRepo.findAllByMealAgeStandard_MealTimeStandard_Menu_IdAndAgeGroup_Id(menu.getId(), ageGroup.getId());

                    for (AgeStandard ageStandard : ageStandards) {

                        Meal meal = ageStandard.getMealAgeStandard().getMeal();

                        Integer kidsNumberNum = 0;

                        BigDecimal number = BigDecimal.valueOf(0);
                        Optional<KidsNumberSub> kidsNumberSubOptional = kidsNumberSubRepo.findByAgeGroup_IdAndKidsNumber_Report_IdAndKidsNumberDeleteIsFalse(ageStandard.getAgeGroup().getId(), report.getId());
                        if (kidsNumberSubOptional.isPresent()) {
                            KidsNumberSub kidsNumberSub = kidsNumberSubOptional.get();
                            number = BigDecimal.valueOf(kidsNumberSub.getNumber());
                            kidsNumberNum = kidsNumberSub.getNumber();
                        }

                        for (ProductMeal productMeal : meal.getProductMealList()) {
                            BigDecimal weight = (productMeal.getWeight().divide(meal.getWeight(), 10, RoundingMode.HALF_UP)).multiply(ageStandard.getWeight());

                            weight = weight.setScale(scale6, RoundingMode.HALF_UP);

                            if (ageStandard.getAgeGroup().getReport()) {
                                addList(productList, productMeal.getProduct(), report.getDate(), weight.multiply(number));
                            }

                            if (uchYetti != null && jami != null) {
                                addListTotal(ageGroup, productMeal.getProduct(), report.getDate(), weight.multiply(number), uchYetti, qisqa, jami, xodim, kindergarten.getDepartment().getId(), districtId, year, month);

                                addListQatnov(ageGroup, productMeal.getProduct(), report.getDate(), weight.multiply(number), qisqaQatnov, tortYettiQatnov, kindergarten.getDepartment().getId(), districtId, year, month, kidsNumberNum);
                            }
                        }
                    }

                }
            }
            if (uchYetti != null && jami != null) {
                qatnov(qisqaQatnov, tortYettiQatnov, reportList);
            }
        }

        List<ProductDTO> returnList = new ArrayList<>();

        for (ProductDTO productDTO : productList) {
            if (productDTO.isState()) {
                returnList.add(productDTO);
            }
        }

//        for (ProductDTO productDTO : returnList) {
//            for (DateDTO dateDTO : productDTO.getDateList()) {
//                dateDTO.setWeight(dateDTO.getWeight().divide(BigDecimal.valueOf(1000), scale, RoundingMode.HALF_UP));
//            }
//        }

        returnList.sort(Comparator.comparing(ProductDTO::getName));
        return returnList;
    }

    public void addList(List<ProductDTO> productList, Product product, LocalDate date, BigDecimal weight) {
        boolean res = false;

        weight = weight.divide(BigDecimal.valueOf(1000), scale6, RoundingMode.HALF_UP);


        for (ProductDTO productDTO : productList) {
            if (Objects.equals(productDTO.getId(), product.getId())) {
                for (DateDTO dateDTO : productDTO.getDateList()) {
                    if (dateDTO.getDate().equals(date)) {
                        dateDTO.setWeight(dateDTO.getWeight().add(weight));
                        res = true;
                        break;
                    }
                }
                if (res) {
                    productDTO.setState(true);
                }
            }
        }
    }

    public void addListTotal(AgeGroup ageGroup, Product product, LocalDate date, BigDecimal weight, List<DateDTO> uchYetti, List<DateDTO> qisqa, List<DateDTO> jami, List<DateDTO> xodim, Integer departmentId, Integer districtId, int year, int month) {
        weight = weight.divide(BigDecimal.valueOf(1000), scale6, RoundingMode.HALF_UP);

        BigDecimal checkPack = productPackService.checkPack(departmentId, product.getId());

        if (checkPack != null) {
            weight = weight.divide(checkPack.divide(BigDecimal.valueOf(1000), scale, RoundingMode.HALF_UP), scale6, RoundingMode.HALF_UP);
        }


        Optional<ProductPrice> optional = productPriceRepo.findByDepartment_IdAndDistrict_IdAndYearAndMonthAndProduct_Id(departmentId, districtId, year, month, product.getId());
        BigDecimal price = BigDecimal.ZERO;
        if (optional.isPresent()) {
            ProductPrice productPrice = optional.get();
            price = productPrice.getPrice();
        }

        if (ageGroup.getId() == 1 || ageGroup.getId() == 2) {
            boolean res = true;
            for (DateDTO dateDTO : uchYetti) {
                if (dateDTO.getDate().equals(date)) {
                    dateDTO.setWeight(dateDTO.getWeight().add(weight.multiply(price)));
                    res = false;
                }
            }
            if (res) {
                uchYetti.add(new DateDTO(date, weight.multiply(price)));
            }
        }
        if (ageGroup.getId() == 1 || ageGroup.getId() == 2 || ageGroup.getId() == 3) {
            boolean res = true;
            for (DateDTO dateDTO : jami) {
                if (dateDTO.getDate().equals(date)) {
                    dateDTO.setWeight(dateDTO.getWeight().add(weight.multiply(price)));
                    res = false;
                }
            }
            if (res) {
                jami.add(new DateDTO(date, weight.multiply(price)));
            }
        }
        if (ageGroup.getId() == 3) {
            boolean res = true;
            for (DateDTO dateDTO : qisqa) {
                if (dateDTO.getDate().equals(date)) {
                    dateDTO.setWeight(dateDTO.getWeight().add(weight.multiply(price)));
                    res = false;
                }
            }
            if (res) {
                qisqa.add(new DateDTO(date, weight.multiply(price)));
            }
        }
        if (ageGroup.getId() == 4) {
            boolean res = true;
            for (DateDTO dateDTO : xodim) {
                if (dateDTO.getDate().equals(date)) {
                    dateDTO.setWeight(dateDTO.getWeight().add(weight.multiply(price)));
                    res = false;
                }
            }
            if (res) {
                xodim.add(new DateDTO(date, weight.multiply(price)));
            }
        }
    }

    public void addListQatnov(AgeGroup ageGroup, Product product, LocalDate date, BigDecimal weight, List<DateDTO> qisqaQatnov, List<DateDTO> tortYettiQatnov, Integer departmentId, Integer districtId, int year, int month, Integer number) {

        weight = weight.divide(BigDecimal.valueOf(1000), scale6, RoundingMode.HALF_UP);

        BigDecimal checkPack = productPackService.checkPack(departmentId, product.getId());

        if (checkPack != null) {
            weight = weight.divide(checkPack.divide(BigDecimal.valueOf(1000), scale, RoundingMode.HALF_UP), scale6, RoundingMode.HALF_UP);
        }

        Optional<ProductPrice> optional = productPriceRepo.findByDepartment_IdAndDistrict_IdAndYearAndMonthAndProduct_Id(departmentId, districtId, year, month, product.getId());
        BigDecimal price = BigDecimal.ZERO;
        if (optional.isPresent()) {
            ProductPrice productPrice = optional.get();
            price = productPrice.getPrice();
        }


        if (ageGroup.getId() == 2) {
            boolean res = true;
            for (DateDTO dateDTO : tortYettiQatnov) {
                if (dateDTO.getDate().equals(date)) {
                    BigDecimal divide = dateDTO.getWeight().add(weight.multiply(price));
                    dateDTO.setWeight(divide);
//                    dateDTO.setWeight(dateDTO.getWeight().add(weight.multiply(price)));
//                    if (dateDTO.isNumberState() || dateDTO.getNumber() == 0) {
//                        dateDTO.setNumber(dateDTO.getNumber() + number);
//                    }
                    res = false;
                }
            }
            if (res) {
                tortYettiQatnov.add(new DateDTO(date, weight.multiply(price), 0, true));
            }
        }
        if (ageGroup.getId() == 3) {
            boolean res = true;
            for (DateDTO dateDTO : qisqaQatnov) {
                if (dateDTO.getDate().equals(date)) {
                    BigDecimal divide = dateDTO.getWeight().add(weight.multiply(price));
                    dateDTO.setWeight(divide);
//                    dateDTO.setWeight(dateDTO.getWeight().add(weight.multiply(price)));
//                    if (dateDTO.isNumberState() || dateDTO.getNumber() == 0) {
//                        dateDTO.setNumber(dateDTO.getNumber() + number);
//                    }
                    res = false;
                }
            }
            if (res) {
                qisqaQatnov.add(new DateDTO(date, weight.multiply(price), 0, true));
            }
        }
    }

    public void qatnov(List<DateDTO> qisqaQatnov, List<DateDTO> tortYettiQatnov, List<Report> reportdList) {

        for (Report report : reportdList) {
            LocalDate date = report.getDate();
            if (report.getKidsNumber() != null) {
                for (KidsNumberSub kidsNumberSub : report.getKidsNumber().getKidsNumberSubList()) {

                    AgeGroup ageGroup = kidsNumberSub.getAgeGroup();
                    if (ageGroup.getId() == 2) {
                        boolean res = true;
                        for (DateDTO dateDTO : tortYettiQatnov) {
                            if (dateDTO.getDate().equals(date)) {
                                dateDTO.setNumber(dateDTO.getNumber() + kidsNumberSub.getNumber());
                                res = false;
                            }
                        }
                        if (res) {
                            tortYettiQatnov.add(new DateDTO(date, BigDecimal.ZERO, kidsNumberSub.getNumber(), true));
                        }
                    }
                    if (ageGroup.getId() == 3) {
                        boolean res = true;
                        for (DateDTO dateDTO : qisqaQatnov) {
                            if (dateDTO.getDate().equals(date)) {
                                dateDTO.setNumber(dateDTO.getNumber() + kidsNumberSub.getNumber());
                                res = false;
                            }
                        }
                        if (res) {
                            qisqaQatnov.add(new DateDTO(date, BigDecimal.ZERO, kidsNumberSub.getNumber(), true));
                        }
                    }
                }
            }
        }
    }

    public List<ProductDTO> getProductList(List<LocalDate> localDates) {


        List<ProductDTO> list = new ArrayList<>();
        for (Product product : productRepo.findAll()) {
            list.add(new ProductDTO(product.getId(), product.getName(), getDateList(localDates)));
        }
        list.sort(Comparator.comparing(ProductDTO::getName));

        return list;
    }

    public List<DateDTO> getDateList(List<LocalDate> localDates) {

        List<DateDTO> dateList = new ArrayList<>();

        for (LocalDate localDate : localDates) {

            dateList.add(new DateDTO(localDate));

        }
        return dateList;
    }

    public double getNumber(BigDecimal num) {

//        return num.toString().replace(".", ",");
        return num.doubleValue();
    }

    public void getTableTotal(short formatNumber, short format, String name, String paragraph, String paragraph2, List<DateDTO> list, XSSFWorkbook workbook, XSSFFont font, XSSFFont fontParagraph, Integer ustama, District district, Department department) {


        XSSFFont fontBold = workbook.createFont();
        fontBold.setFontName("Times New Roman");
        fontBold.setFontHeight(fontHeight);
        fontBold.setBold(true);


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        style(font, cellStyle);
        cellStyle.setRotation((short) 0);
        cellStyle.setDataFormat(format);

        XSSFCellStyle cellStyleNumber = workbook.createCellStyle();
        style(font, cellStyleNumber);
        cellStyleNumber.setRotation((short) 0);
        cellStyleNumber.setDataFormat(formatNumber);

        XSSFCellStyle cellStyleBoldNoBorder = workbook.createCellStyle();
        styleNoBorder(fontBold, cellStyleBoldNoBorder);
        cellStyleBoldNoBorder.setRotation((short) 0);
        cellStyleBoldNoBorder.setDataFormat(format);

        XSSFCellStyle cellStyleBold = workbook.createCellStyle();
        style(fontBold, cellStyleBold);
        cellStyleBold.setRotation((short) 0);
        cellStyleBold.setDataFormat(format);

        XSSFCellStyle cellStyleRotate = workbook.createCellStyle();
        style(font, cellStyleRotate);
        cellStyleRotate.setRotation((short) 90);

        XSSFCellStyle paragraphStyle = workbook.createCellStyle();
        paragraph(fontParagraph, paragraphStyle);
        paragraphStyle.setRotation((short) 0);
        paragraphStyle.setDataFormat(format);

        XSSFSheet spreadsheet = workbook.createSheet(name);
        XSSFPrintSetup printSetup = spreadsheet.getPrintSetup();
        printSetup.setPaperSize(PaperSize.A4_PAPER);
        printSetup.setLandscape(true);


        spreadsheet.setColumnWidth(0, 1000);
        spreadsheet.setColumnWidth(1, 6000);
        spreadsheet.setColumnWidth(2, 6000);
        spreadsheet.setColumnWidth(3, 6000);
        spreadsheet.setColumnWidth(4, 6000);
        spreadsheet.setColumnWidth(5, 6000);


        int rowNum = 0;


        XSSFRow row = spreadsheet.createRow(rowNum++);
        row.setHeight((short) 1000);
        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));

        XSSFCell cellPar1 = row.createCell(0);
        cellPar1.setCellValue(paragraph);
        cellPar1.setCellStyle(paragraphStyle);

        XSSFRow rowPar = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 5));

        XSSFCell cellPar = rowPar.createCell(0);
        cellPar.setCellValue(paragraph2);
        cellPar.setCellStyle(paragraphStyle);

        rowNum++;
        XSSFRow row0 = spreadsheet.createRow(rowNum++);

        spreadsheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 0));
//        spreadsheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));

        XSSFCell cell0 = row0.createCell(0);
        cell0.setCellValue("T/R");
        cell0.setCellStyle(cellStyle);

        XSSFCell cell1 = row0.createCell(1);
        cell1.setCellValue("Sana");
        cell1.setCellStyle(cellStyle);

        XSSFCell cell2 = row0.createCell(2);
        cell2.setCellValue("Summa (QQS bilan)");
        cell2.setCellStyle(cellStyle);


        XSSFCell cell3 = row0.createCell(3);
        cell3.setCellValue("Summa (QQS siz)");
        cell3.setCellStyle(cellStyle);


        XSSFCell cell4 = row0.createCell(4);
        cell4.setCellValue("Ustama xaq " + ustama + "%");
        cell4.setCellStyle(cellStyle);

        XSSFCell cell5 = row0.createCell(5);
        cell5.setCellValue("Jami");
        cell5.setCellStyle(cellStyle);

        int number = 1;

        BigDecimal totalSum = BigDecimal.ZERO;


        for (DateDTO dto : list) {

            XSSFRow row1 = spreadsheet.createRow(rowNum);
            int cell = 0;
            spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum++, 0, 0));

            XSSFCell cell6 = row1.createCell(cell++);
            cell6.setCellValue(number++);
            cell6.setCellStyle(cellStyleNumber);

            XSSFCell cell7 = row1.createCell(cell++);

            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            String formattedDate = dto.getDate().format(dateTimeFormatter);  //17-02-2022


            cell7.setCellValue(formattedDate);
            cell7.setCellStyle(cellStyle);

            XSSFCell cell8 = row1.createCell(cell++);

            BigDecimal weight = (dto.getWeight()).setScale(scale, RoundingMode.HALF_UP);

            totalSum = totalSum.add(weight);

            cell8.setCellValue(getNumber(weight));
            cell8.setCellStyle(cellStyle);

            XSSFCell cell9 = row1.createCell(cell++);
            cell9.setCellValue(getNumber((dto.getWeight().divide(BigDecimal.valueOf(112), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100))).setScale(scale, RoundingMode.HALF_UP)));
            cell9.setCellStyle(cellStyle);

            XSSFCell cell10 = row1.createCell(cell++);
            cell10.setCellValue(getNumber((dto.getWeight().divide(BigDecimal.valueOf(112), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(ustama))).setScale(scale, RoundingMode.HALF_UP)));
            cell10.setCellStyle(cellStyle);

            XSSFCell cell11 = row1.createCell(cell++);
            cell11.setCellValue(getNumber(((dto.getWeight().divide(BigDecimal.valueOf(112), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(ustama + 100))).setScale(scale, RoundingMode.HALF_UP))));
            cell11.setCellStyle(cellStyle);
        }


        XSSFRow row1 = spreadsheet.createRow(rowNum);
        int cell = 0;
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum++, 0, 1));
        XSSFCell cell71 = row1.createCell(cell++);
        cell71.setCellValue("Jami");
        cell71.setCellStyle(cellStyle);

        XSSFCell cell711 = row1.createCell(cell++);
        cell711.setCellStyle(cellStyle);

        XSSFCell cell81 = row1.createCell(cell++);

        cell81.setCellValue(getNumber(totalSum));
        cell81.setCellStyle(cellStyle);

        XSSFCell cell91 = row1.createCell(cell++);
        cell91.setCellValue(getNumber((totalSum.divide(BigDecimal.valueOf(112), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100))).setScale(scale, RoundingMode.HALF_UP)));
        cell91.setCellStyle(cellStyle);

        XSSFCell cell101 = row1.createCell(cell++);
        cell101.setCellValue(getNumber((totalSum.divide(BigDecimal.valueOf(112), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(ustama))).setScale(scale, RoundingMode.HALF_UP)));
        cell101.setCellStyle(cellStyle);

        XSSFCell cell111 = row1.createCell(cell++);
        cell111.setCellValue(getNumber(((totalSum.divide(BigDecimal.valueOf(112), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(ustama + 100))).setScale(scale, RoundingMode.HALF_UP))));
        cell111.setCellStyle(cellStyle);


        int rowNumber = rowNum + 3;

        XSSFRow row2 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
        XSSFCell cell7 = row2.createCell(1);
        cell7.setCellValue("Autsorser");
        cell7.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 4, 5));
        XSSFCell cell8 = row2.createCell(4);
        cell8.setCellValue("Istemolchi");
        cell8.setCellStyle(cellStyleBoldNoBorder);


        rowNumber += 2;

        XSSFRow row3 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
        XSSFCell cell9 = row3.createCell(1);
        cell9.setCellValue(department.getName());
        cell9.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 4, 5));
        XSSFCell cell10 = row3.createCell(4);
        cell10.setCellValue(district.getName() + " MTB");
        cell10.setCellStyle(cellStyleBoldNoBorder);


        rowNumber += 2;

        XSSFRow row4 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
        XSSFCell cell11 = row4.createCell(1);
        cell11.setCellValue("rahbari___________________________");
        cell11.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 4, 5));
        XSSFCell cell12 = row4.createCell(4);
        cell12.setCellValue("rahbari___________________________");
        cell12.setCellStyle(cellStyleBoldNoBorder);
    }

    public void getTableQatnov(short format, List<LocalDate> localDates, List<DateDTO> qisqaQatnov, List<DateDTO> tortYettiQatnov, XSSFWorkbook workbook, XSSFFont font, XSSFFont fontParagraph, Integer ustama, District district, Department department, LocalDate start, LocalDate end, BigDecimal zaxiraSumma) {


        BigDecimal total3_7Factura = BigDecimal.ZERO;
        BigDecimal totalQisqaFactura = BigDecimal.ZERO;


        XSSFFont fontBold = workbook.createFont();
        fontBold.setFontName("Times New Roman");
        fontBold.setFontHeight(fontHeight);
        fontBold.setBold(true);


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        style(font, cellStyle);
        cellStyle.setRotation((short) 0);
        cellStyle.setDataFormat(format);

        XSSFCellStyle cellStyleBoldNoBorder = workbook.createCellStyle();
        styleNoBorder(fontBold, cellStyleBoldNoBorder);
        cellStyleBoldNoBorder.setRotation((short) 0);
        cellStyleBoldNoBorder.setDataFormat(format);

        XSSFCellStyle cellStyleBold = workbook.createCellStyle();
        style(fontBold, cellStyleBold);
        cellStyleBold.setRotation((short) 0);
        cellStyleBold.setDataFormat(format);

        XSSFCellStyle cellStyleRotate = workbook.createCellStyle();
        style(font, cellStyleRotate);
        cellStyleRotate.setRotation((short) 90);
        cellStyleRotate.setDataFormat(format);

        XSSFCellStyle paragraphStyle = workbook.createCellStyle();
        paragraph(fontParagraph, paragraphStyle);
        paragraphStyle.setRotation((short) 0);
        paragraphStyle.setDataFormat(format);

        XSSFSheet spreadsheet = workbook.createSheet("Qatnov");

        XSSFPrintSetup printSetup = spreadsheet.getPrintSetup();
        printSetup.setPaperSize(PaperSize.A4_PAPER);
        printSetup.setLandscape(true);


        spreadsheet.setColumnWidth(0, 1500);

        for (int i = 1; i < 10; i++) {
            spreadsheet.setColumnWidth(i, 2850);
        }
        for (int i = 10; i < 18; i++) {
            spreadsheet.setColumnWidth(i, 4670);
        }


        int rowNum = 0;


        XSSFRow row = spreadsheet.createRow(rowNum++);
        row.setHeight((short) 1000);
        spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 17));

        XSSFCell cellPar1 = row.createCell(0);
        cellPar1.setCellValue(district.getName() + "da " + localDates.get(0) + " - " + localDates.get(localDates.size() - 1) + " kunlarida sarflangan oziq-ovaqat maxsulotlari to‘g‘risida ma‘lumot");
        cellPar1.setCellStyle(paragraphStyle);

        rowNum++;
        XSSFRow row0 = spreadsheet.createRow(rowNum);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum + 1, 0, 0));
        XSSFCell cell0 = row0.createCell(0);
        cell0.setCellValue("T/R");
        cell0.setCellStyle(cellStyle);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum + 1, 1, 1));
        XSSFCell cell1 = row0.createCell(1);
        cell1.setCellValue("Sana");
        cell1.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 2, 5));
        XSSFCell cell2 = row0.createCell(2);
        cell2.setCellValue("Buyurtma bo‘yicha");
        cell2.setCellStyle(cellStyle);

        for (int i = 3; i <= 5; i++) {
            XSSFCell cell01 = row0.createCell(i);
            cell01.setCellStyle(cellStyle);
        }

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 6, 9));
        XSSFCell cell3 = row0.createCell(6);
        cell3.setCellValue("Bir nafar bolaga sarflangan xarajat");
        cell3.setCellStyle(cellStyle);

        for (int i = 7; i <= 9; i++) {
            XSSFCell cell01 = row0.createCell(i);
            cell01.setCellStyle(cellStyle);
        }

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 10, 13));
        XSSFCell cell31 = row0.createCell(10);
        cell31.setCellValue("Jami maxsulotlarga sarflangan xarajat");
        cell31.setCellStyle(cellStyle);

        for (int i = 11; i <= 13; i++) {
            XSSFCell cell01 = row0.createCell(i);
            cell01.setCellStyle(cellStyle);
        }

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 14, 16));
        XSSFCell cell4 = row0.createCell(14);
        cell4.setCellValue("Ko‘rsatilgan xizmatlar uchun");
        cell4.setCellStyle(cellStyle);

        for (int i = 15; i <= 16; i++) {
            XSSFCell cell01 = row0.createCell(i);
            cell01.setCellStyle(cellStyle);
        }


        XSSFRow row0_1 = spreadsheet.createRow(++rowNum);
//        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 2, 2));
//        XSSFCell cell1_1 = row0_1.createCell(2);
//        cell1_1.setCellValue("3-4 yosh");
//        cell1_1.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 3, 3));
        XSSFCell cell1_2 = row0_1.createCell(3);
        cell1_2.setCellValue("3-7 yosh");
        cell1_2.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 4, 4));
        XSSFCell cell1_3 = row0_1.createCell(4);
        cell1_3.setCellValue("Qisqa muddatli");
        cell1_3.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 5, 5));
        XSSFCell cell1_4 = row0_1.createCell(5);
        cell1_4.setCellValue("Jami");
        cell1_4.setCellStyle(cellStyle);

//        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 6, 6));
//        XSSFCell cell1_5 = row0_1.createCell(6);
//        cell1_5.setCellValue("3-4 yosh");
//        cell1_5.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 7, 7));
        XSSFCell cell1_6 = row0_1.createCell(7);
        cell1_6.setCellValue("3-7 yosh");
        cell1_6.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 8, 8));
        XSSFCell cell1_7 = row0_1.createCell(8);
        cell1_7.setCellValue("Qisqa muddatli");
        cell1_7.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 9, 9));
        XSSFCell cell1_8 = row0_1.createCell(9);
        cell1_8.setCellValue("O‘rtacha");
        cell1_8.setCellStyle(cellStyle);


//        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 10, 10));
//        XSSFCell cell1_9 = row0_1.createCell(10);
//        cell1_9.setCellValue("3-4 yosh");
//        cell1_9.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 11, 11));
        XSSFCell cell1_10 = row0_1.createCell(11);
        cell1_10.setCellValue("3-7 yosh");
        cell1_10.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 12, 12));
        XSSFCell cell1_11 = row0_1.createCell(12);
        cell1_11.setCellValue("Qisqa muddatli");
        cell1_11.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 13, 13));
        XSSFCell cell1_12 = row0_1.createCell(13);
        cell1_12.setCellValue("Jami");
        cell1_12.setCellStyle(cellStyle);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 14, 14));
        XSSFCell cell1_13 = row0_1.createCell(14);
        cell1_13.setCellValue("Summa (QQS siz)");
        cell1_13.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 15, 15));
        XSSFCell cell1_14 = row0_1.createCell(15);
        cell1_14.setCellValue("Ustama xaq " + ustama + "%");
        cell1_14.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 16, 16));
        XSSFCell cell1_15 = row0_1.createCell(16);
        cell1_15.setCellValue("QQS 12%");
        cell1_15.setCellStyle(cellStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum - 1, rowNum, 17, 17));
        XSSFCell cell5 = row0.createCell(17);
        cell5.setCellValue("Jami to`lanadigan xaq (QQS bilan)");
        cell5.setCellStyle(cellStyle);


        XSSFCell cell01 = row0_1.createCell(0);
        cell01.setCellStyle(cellStyle);

        XSSFCell cell02 = row0_1.createCell(1);
        cell02.setCellStyle(cellStyle);

        XSSFCell cell03 = row0_1.createCell(17);
        cell03.setCellStyle(cellStyle);


        int index = 0;
        int number = 1;

        List<BigDecimal> values = new ArrayList<>();
        for (int i = 0; i < 17; i++) {
            values.add(BigDecimal.ZERO);
        }

        for (DateDTO dto4 : tortYettiQatnov) {

            int urtacha = 0;

//            DateDTO dto4 = tortYettiQatnov.get(index);
            DateDTO dtoQisqa = qisqaQatnov.get(index);

            BigDecimal totalWeight = BigDecimal.ZERO;
            BigDecimal totalJami = BigDecimal.ZERO;
            int totalNumber = 0;


            int cell = 0;
            XSSFRow row1 = spreadsheet.createRow(++rowNum);
            spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 0));

            XSSFCell cell6 = row1.createCell(cell++);
            cell6.setCellValue(number++);
            cell6.setCellStyle(cellStyle);

//            XSSFCell cell7 = row1.createCell(cell++);
//            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
//            String formattedDate = dto3.getDate().format(dateTimeFormatter);  //17-02-2022
//            cell7.setCellValue(formattedDate);
//            cell7.setCellStyle(cellStyle);

//            XSSFCell cell8 = row1.createCell(cell++);
//            cell8.setCellValue(dto3.getNumber());
//            values.set(1, values.get(1).add(BigDecimal.valueOf(dto3.getNumber())));
//            totalNumber += dto3.getNumber();
//            cell8.setCellStyle(cellStyle);

            XSSFCell cell9 = row1.createCell(cell++);
            cell9.setCellValue(dto4.getNumber());
            values.set(2, values.get(2).add(BigDecimal.valueOf(dto4.getNumber())));
            totalNumber += dto4.getNumber();
            cell9.setCellStyle(cellStyle);

            XSSFCell cell10 = row1.createCell(cell++);
            cell10.setCellValue(dtoQisqa.getNumber());
            values.set(3, values.get(3).add(BigDecimal.valueOf(dtoQisqa.getNumber())));
            totalNumber += dtoQisqa.getNumber();
            cell10.setCellStyle(cellStyle);

            XSSFCell cell11 = row1.createCell(cell++);
            values.set(4, values.get(4).add(BigDecimal.valueOf(totalNumber)));
            cell11.setCellValue(totalNumber);
            cell11.setCellStyle(cellStyle);


            XSSFCell cell12 = row1.createCell(cell++);

//            BigDecimal weight = BigDecimal.ZERO;
//            if ((dto3.getWeight().compareTo(BigDecimal.ZERO) != 0) && dto3.getNumber() > 0) {
//                BigDecimal weight3 = dto3.getWeight().setScale(scale, RoundingMode.HALF_UP);
//
//                weight = weight3.setScale(scale, RoundingMode.HALF_UP).divide(BigDecimal.valueOf(dto3.getNumber()), scale, RoundingMode.HALF_UP);
//
//                dto3.setWeight(BigDecimal.valueOf(dto3.getNumber()).multiply(weight));
//                urtacha++;
//            }

//            totalWeight = totalWeight.add(weight);
//            values.set(5, values.get(5).add(weight));
//            cell12.setCellValue(getNumber(weight));
//            cell12.setCellStyle(cellStyle);


            XSSFCell cell13 = row1.createCell(cell++);

            BigDecimal weight4 = BigDecimal.ZERO;
            if ((dto4.getWeight().compareTo(BigDecimal.ZERO) != 0) && dto4.getNumber() > 0) {
                BigDecimal weightDto4 = dto4.getWeight().setScale(scale, RoundingMode.HALF_UP);
                weight4 = weightDto4.setScale(scale, RoundingMode.HALF_UP).divide(BigDecimal.valueOf(dto4.getNumber()), scale, RoundingMode.HALF_UP);
                dto4.setWeight(BigDecimal.valueOf(dto4.getNumber()).multiply(weight4));
                urtacha++;

            }

            totalWeight = totalWeight.add(weight4);
            values.set(6, values.get(6).add(weight4));
            cell13.setCellValue(getNumber(weight4));
            cell13.setCellStyle(cellStyle);

            XSSFCell cell14 = row1.createCell(cell++);

            BigDecimal weightQisqa = BigDecimal.ZERO;
            if ((dtoQisqa.getWeight().compareTo(BigDecimal.ZERO) != 0) && dtoQisqa.getNumber() > 0) {
                BigDecimal weightDtoQisqa = dtoQisqa.getWeight().setScale(scale, RoundingMode.HALF_UP);
                weightQisqa = weightDtoQisqa.setScale(scale, RoundingMode.HALF_UP).divide(BigDecimal.valueOf(dtoQisqa.getNumber()), scale, RoundingMode.HALF_UP);

                dtoQisqa.setWeight(BigDecimal.valueOf(dtoQisqa.getNumber()).multiply(weightQisqa));
                urtacha++;
            }


            totalWeight = totalWeight.add(weightQisqa);
            values.set(7, values.get(7).add(weightQisqa));
            cell14.setCellValue(getNumber(weightQisqa));
            cell14.setCellStyle(cellStyle);


            if (totalWeight.compareTo(BigDecimal.ZERO) != 0) {
                totalWeight = totalWeight.divide(BigDecimal.valueOf(urtacha), scale, RoundingMode.HALF_UP);
            }


            XSSFCell cell15 = row1.createCell(cell++);
            values.set(8, values.get(8).add(totalWeight));
            cell15.setCellValue(getNumber(totalWeight));
            cell15.setCellStyle(cellStyle);


//            XSSFCell cell16 = row1.createCell(cell++);
//            BigDecimal total3 = (dto3.getWeight());

//            total3_7Factura = total3_7Factura.add(total3);
//
//            totalJami = totalJami.add(total3);
//            values.set(9, values.get(9).add(total3));
//            cell16.setCellValue(getNumber(total3));
//            cell16.setCellStyle(cellStyle);


            XSSFCell cell17 = row1.createCell(cell++);
            BigDecimal total4 = (dto4.getWeight());

            total3_7Factura = total3_7Factura.add(total4);
            totalJami = totalJami.add(total4);
            values.set(10, values.get(10).add(total4));
            cell17.setCellValue(getNumber(total4));
            cell17.setCellStyle(cellStyle);


            XSSFCell cell18 = row1.createCell(cell++);
            BigDecimal totalQisqa = (dtoQisqa.getWeight());

            totalQisqaFactura = totalQisqaFactura.add(totalQisqa);

            totalJami = totalJami.add(totalQisqa);
            values.set(11, values.get(11).add(totalQisqa));
            cell18.setCellValue(getNumber(totalQisqa));
            cell18.setCellStyle(cellStyle);


            XSSFCell cell19 = row1.createCell(cell++);
            values.set(12, values.get(12).add(totalJami));
            cell19.setCellValue(getNumber(totalJami));
            cell19.setCellStyle(cellStyle);

            XSSFCell cell20 = row1.createCell(cell++);

            BigDecimal value13 = BigDecimal.ZERO;
            if (totalJami.compareTo(BigDecimal.valueOf(0)) > 0) {
                value13 = totalJami.divide(BigDecimal.valueOf(112), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
            }

            values.set(13, values.get(13).add(value13));
            cell20.setCellValue(getNumber(value13));
            cell20.setCellStyle(cellStyle);


            XSSFCell cell21 = row1.createCell(cell++);

            BigDecimal value14 = BigDecimal.ZERO;
            if (value13.compareTo(BigDecimal.valueOf(0)) > 0) {
                value14 = value13.divide(BigDecimal.valueOf(100), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(ustama));
            }

            values.set(14, values.get(14).add(value14));
            cell21.setCellValue(getNumber(value14));
            cell21.setCellStyle(cellStyle);


            XSSFCell cell22 = row1.createCell(cell++);

            BigDecimal value15 = BigDecimal.ZERO;
            if (value13.compareTo(BigDecimal.valueOf(0)) > 0) {
                value15 = value13.add(value14).divide(BigDecimal.valueOf(100), scale, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(12));
            }
            values.set(15, values.get(15).add(value15));
            cell22.setCellValue(getNumber(value15));
            cell22.setCellStyle(cellStyle);


            XSSFCell cell23 = row1.createCell(cell++);

            BigDecimal value16 = value13.add(value14).add(value15);
            values.set(16, values.get(16).add(value16));
            cell23.setCellValue(getNumber(value16));
            cell23.setCellStyle(cellStyle);

            index++;
        }


        XSSFRow row1 = spreadsheet.createRow(++rowNum);

        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 1));
        XSSFCell cell13 = row1.createCell(0);
        cell13.setCellValue("Jami:");
        cell13.setCellStyle(cellStyle);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 0));
        int cell = 1;
        for (BigDecimal value : values) {
            XSSFCell cell6 = row1.createCell(cell++);
            cell6.setCellValue(getNumber(value));
            cell6.setCellStyle(cellStyle);
        }


        int rowNumber = rowNum + 3;

        XSSFRow row2 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 3, 5));
        XSSFCell cell7 = row2.createCell(3);
        cell7.setCellValue("Autsorser");
        cell7.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 10, 12));
        XSSFCell cell8 = row2.createCell(10);
        cell8.setCellValue("Istemolchi");
        cell8.setCellStyle(cellStyleBoldNoBorder);


        rowNumber += 2;

        XSSFRow row3 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 3, 5));
        XSSFCell cell9 = row3.createCell(3);
        cell9.setCellValue(department.getName());
        cell9.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 10, 12));
        XSSFCell cell10 = row3.createCell(10);
        cell10.setCellValue(district.getName() + " MTB");
        cell10.setCellStyle(cellStyleBoldNoBorder);


        rowNumber += 2;

        XSSFRow row4 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 3, 5));
        XSSFCell cell11 = row4.createCell(3);
        cell11.setCellValue("rahbari___________________________");
        cell11.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 10, 12));
        XSSFCell cell12 = row4.createCell(10);
        cell12.setCellValue("rahbari___________________________");
        cell12.setCellStyle(cellStyleBoldNoBorder);


        total3_7Factura = total3_7Factura.divide(BigDecimal.valueOf(112), scale6, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(119)).setScale(2, RoundingMode.HALF_UP);
        totalQisqaFactura = totalQisqaFactura.divide(BigDecimal.valueOf(112), scale6, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(119)).setScale(2, RoundingMode.HALF_UP);


        factura(format, workbook, font, fontParagraph, department, district, start, end, total3_7Factura, totalQisqaFactura, zaxiraSumma);
    }

    public void factura(short format, XSSFWorkbook workbook, XSSFFont font, XSSFFont fontParagraph, Department department, District district, LocalDate start, LocalDate end, BigDecimal totalSum3_4, BigDecimal totalSumQisqa, BigDecimal totalSumProduct) {


        fontParagraph.setBold(false);

        XSSFFont fontBold = workbook.createFont();
        fontBold.setFontName("Times New Roman");
        fontBold.setFontHeight(12);
//        fontBold.setBold(true);


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        style(font, cellStyle);
        cellStyle.setRotation((short) 0);
        cellStyle.setDataFormat(format);

        XSSFCellStyle cellStyleBoldNoBorder = workbook.createCellStyle();
        styleNoBorder(fontBold, cellStyleBoldNoBorder);
        cellStyleBoldNoBorder.setRotation((short) 0);
        cellStyleBoldNoBorder.setDataFormat(format);

        XSSFCellStyle cellStyleBold = workbook.createCellStyle();
        style(fontBold, cellStyleBold);
        cellStyleBold.setRotation((short) 0);
        cellStyleBold.setDataFormat(format);

        XSSFCellStyle cellStyleBoldMaxsus = workbook.createCellStyle();
        styleMaxsus(fontBold, cellStyleBoldMaxsus);
        cellStyleBoldMaxsus.setRotation((short) 0);
        cellStyleBoldMaxsus.setDataFormat(format);

        XSSFCellStyle paragraphStyle = workbook.createCellStyle();
        fontParagraph.setFontHeight(12);
        paragraph(fontParagraph, paragraphStyle);
        paragraphStyle.setRotation((short) 0);
        paragraphStyle.setDataFormat(format);

        XSSFCellStyle paragraphStyleFactura = workbook.createCellStyle();
        paragraphFactura(fontParagraph, paragraphStyleFactura);
        paragraphStyleFactura.setRotation((short) 0);


        XSSFSheet spreadsheet = workbook.createSheet("Faktura");

        spreadsheet.setColumnWidth(0, 1500);

        spreadsheet.setColumnWidth(1, 12500);
        spreadsheet.setColumnWidth(2, 2000);
        spreadsheet.setColumnWidth(3, 2000);
        spreadsheet.setColumnWidth(4, 4500);
        spreadsheet.setColumnWidth(5, 4500);
        spreadsheet.setColumnWidth(6, 1500);
        spreadsheet.setColumnWidth(7, 4500);
        spreadsheet.setColumnWidth(8, 4500);


        int rowNum = 0;
        int firstRow = 0;
        int old = 8;


        XSSFRow row = spreadsheet.createRow(rowNum++);
//        row.setHeight((short) 500);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, old));

        XSSFCell cellPar = row.createCell(0);
        cellPar.setCellValue("Xisob faktura №________         " + LocalDate.now());
        cellPar.setCellStyle(paragraphStyle);

        XSSFRow row1 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, old));

        XSSFCell cellPar1 = row1.createCell(0);
        cellPar1.setCellValue("\"_____\"_______ 202__ yildagi №_______   shartnomaga asosan");
        cellPar1.setCellStyle(paragraphStyle);

        rowNum += 1;
        firstRow += 1;

        XSSFRow row2 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 2));

        XSSFCell cellPar2 = row2.createCell(1);
        cellPar2.setCellValue("Autsourser");
        cellPar2.setCellStyle(paragraphStyle);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 6, old));

        XSSFCell cellPar3 = row2.createCell(6);
        cellPar3.setCellValue("Istemolchi");
        cellPar3.setCellStyle(paragraphStyle);

        rowNum += 1;
        firstRow += 1;


        XSSFRow row3 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 2));

        XSSFCell cellPar4 = row3.createCell(1);
        cellPar4.setCellValue("Tashkilot nomi:  " + department.getName());
        cellPar4.setCellStyle(paragraphStyleFactura);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 6, old));
        XSSFCell cellPar5 = row3.createCell(6);
        cellPar5.setCellValue("Tashkilot nomi:  " + district.getName() + " MTB");
        cellPar5.setCellStyle(paragraphStyleFactura);

        XSSFRow row4 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 2));

        XSSFCell cellPar6 = row4.createCell(1);
        cellPar6.setCellValue("Manzil:  " + department.getAddress());
        cellPar6.setCellStyle(paragraphStyleFactura);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 6, old));
        XSSFCell cellPar7 = row4.createCell(6);
        cellPar7.setCellValue("Manzil:  " + district.getName());
        cellPar7.setCellStyle(paragraphStyleFactura);


        XSSFRow row5 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 2));

        XSSFCell cellPar8 = row5.createCell(1);
        cellPar8.setCellValue("Telefon:  ");
        cellPar8.setCellStyle(paragraphStyleFactura);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 6, old));
        XSSFCell cellPar9 = row5.createCell(6);
        cellPar9.setCellValue("Telefon:  ");
        cellPar9.setCellStyle(paragraphStyleFactura);


        XSSFRow row6 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 2));

        XSSFCell cellPar10 = row6.createCell(1);
        cellPar10.setCellValue("STIR:  " + department.getStir());
        cellPar10.setCellStyle(paragraphStyleFactura);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 6, old));
        XSSFCell cellPar11 = row6.createCell(6);
        cellPar11.setCellValue("STIR:  ");
        cellPar11.setCellStyle(paragraphStyleFactura);

        XSSFRow row7 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 2));

        XSSFCell cellPar12 = row7.createCell(1);
        cellPar12.setCellValue("X/r:  " + department.getXr());
        cellPar12.setCellStyle(paragraphStyleFactura);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 6, old));
        XSSFCell cellPar13 = row7.createCell(6);
        cellPar13.setCellValue("X/r:  ");
        cellPar13.setCellStyle(paragraphStyleFactura);


        XSSFRow row8 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 2));

        XSSFCell cellPar14 = row8.createCell(1);
        cellPar14.setCellValue("Bank:  " + department.getBank());
        cellPar14.setCellStyle(paragraphStyleFactura);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 6, old));
        XSSFCell cellPar15 = row8.createCell(6);
        cellPar15.setCellValue("Bank:  ");
        cellPar15.setCellStyle(paragraphStyleFactura);

        XSSFRow row9 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 2));

        XSSFCell cellPar16 = row9.createCell(1);
        cellPar16.setCellValue("MFO:  " + department.getMfo());
        cellPar16.setCellStyle(paragraphStyleFactura);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 6, old));
        XSSFCell cellPar17 = row9.createCell(6);
        cellPar17.setCellValue("MFO:  ");
        cellPar17.setCellStyle(paragraphStyleFactura);


        rowNum += 2;
        firstRow += 2;


        XSSFRow row10 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow + 1, 0, 0));

        XSSFCell cellPar18 = row10.createCell(0);
        cellPar18.setCellValue("T/r");
        cellPar18.setCellStyle(cellStyleBold);


        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow + 1, 1, 1));
        XSSFCell cellPar19 = row10.createCell(1);
        cellPar19.setCellValue("Ish yoki xizmat nomi");
        cellPar19.setCellStyle(cellStyleBold);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow + 1, 2, 2));
        XSSFCell cellPar20 = row10.createCell(2);
        cellPar20.setCellValue("O'l-b.");
        cellPar20.setCellStyle(cellStyleBold);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow + 1, 3, 3));
        XSSFCell cellPar21 = row10.createCell(3);
        cellPar21.setCellValue("Soni");
        cellPar21.setCellStyle(cellStyleBold);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow + 1, 4, 4));
        XSSFCell cellPar22 = row10.createCell(4);
        cellPar22.setCellValue("Narxi");
        cellPar22.setCellStyle(cellStyleBold);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow + 1, 5, 5));
        XSSFCell cellPar23 = row10.createCell(5);
        cellPar23.setCellValue("Yetkaib berish narxi");
        cellPar23.setCellStyle(cellStyleBold);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 6, 7));
        XSSFCell cellPar24 = row10.createCell(6);
        cellPar24.setCellValue("QQS");
        cellPar24.setCellStyle(cellStyleBold);

        XSSFCell cellPar24_1 = row10.createCell(7);
        cellPar24_1.setCellStyle(cellStyleBold);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow + 1, 8, 8));
        XSSFCell cellPar25 = row10.createCell(8);
        cellPar25.setCellValue("Jami summa QQS bilan");
        cellPar25.setCellStyle(cellStyleBold);

        XSSFRow row11 = spreadsheet.createRow(rowNum++);
        for (int i = 0; i < 9; i++) {
            if (i != 6 && i != 7) {
                XSSFCell cellPar28 = row11.createCell(i);
                cellPar28.setCellStyle(cellStyleBold);
            }
        }
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 6, 6));
        XSSFCell cellPar26 = row11.createCell(6);
        cellPar26.setCellValue("%");
        cellPar26.setCellStyle(cellStyleBold);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 7, 7));
        XSSFCell cellPar27 = row11.createCell(7);
        cellPar27.setCellValue("summa");
        cellPar27.setCellStyle(cellStyleBold);


        XSSFRow row12 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, 0));

        XSSFCell cellPar28 = row12.createCell(0);
        cellPar28.setCellValue("1");
        cellPar28.setCellStyle(cellStyleBold);

        XSSFCell cellPar29 = row12.createCell(1);
        cellPar29.setCellValue(district.getName() + " DMTT 3-4 yosh  va 4-7 yoshli gurux tarbialanuvchiari uchun " + start.getYear() + "-yil " +
                start.getDayOfMonth() + "-" + end.getDayOfMonth() + "-" + getMonthName(start.getMonthValue()) + " oyida autsoursing asosida kuniga to'rt mahal ovqatlanishni tashkil etish bo'yicha.");
        cellPar29.setCellStyle(cellStyleBoldMaxsus);

        XSSFCell cellPar30 = row12.createCell(2);
        cellPar30.setCellValue("so'm");
        cellPar30.setCellStyle(cellStyleBold);

        XSSFCell cellPar31 = row12.createCell(3);
        cellPar31.setCellValue("1");
        cellPar31.setCellStyle(cellStyleBold);


        XSSFCell cellPar32 = row12.createCell(4);
        cellPar32.setCellValue(getNumber(totalSum3_4));
        cellPar32.setCellStyle(cellStyleBold);

        XSSFCell cellPar33 = row12.createCell(5);
        cellPar33.setCellValue(getNumber(totalSum3_4));
        cellPar33.setCellStyle(cellStyleBold);

        XSSFCell cellPar34 = row12.createCell(6);
        cellPar34.setCellValue("12");
        cellPar34.setCellStyle(cellStyleBold);

        XSSFCell cellPar35 = row12.createCell(7);
        BigDecimal qqs3_4 = totalSum3_4.divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(12)).setScale(2, RoundingMode.HALF_UP);
        cellPar35.setCellValue(getNumber(qqs3_4));
        cellPar35.setCellStyle(cellStyleBold);

        XSSFCell cellPar36 = row12.createCell(8);
        BigDecimal total3_4 = qqs3_4.add(totalSum3_4);
        cellPar36.setCellValue(getNumber(total3_4));
        cellPar36.setCellStyle(cellStyleBold);


        XSSFRow row13 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, 0));

        XSSFCell cellPar37 = row13.createCell(0);
        cellPar37.setCellValue("2");
        cellPar37.setCellStyle(cellStyleBold);

        XSSFCell cellPar38 = row13.createCell(1);
        cellPar38.setCellValue(district.getName() + " DMTT qisqa muddatli gurux tarbialanuvchiari uchun " + start.getYear() + "-yil " +
                start.getDayOfMonth() + "-" + end.getDayOfMonth() + "-" + getMonthName(start.getMonthValue()) + " oyida autsoursing asosida kuniga to'rt mahal ovqatlanishni tashkil etish bo'yicha.");
        cellPar38.setCellStyle(cellStyleBoldMaxsus);

        XSSFCell cellPar39 = row13.createCell(2);
        cellPar39.setCellValue("so'm");
        cellPar39.setCellStyle(cellStyleBold);

        XSSFCell cellPar40 = row13.createCell(3);
        cellPar40.setCellValue("1");
        cellPar40.setCellStyle(cellStyleBold);


        XSSFCell cellPar41 = row13.createCell(4);
        cellPar41.setCellValue(getNumber(totalSumQisqa));
        cellPar41.setCellStyle(cellStyleBold);

        XSSFCell cellPar42 = row13.createCell(5);
        cellPar42.setCellValue(getNumber(totalSumQisqa));
        cellPar42.setCellStyle(cellStyleBold);

        XSSFCell cellPar43 = row13.createCell(6);
        cellPar43.setCellValue("12");
        cellPar43.setCellStyle(cellStyleBold);

        XSSFCell cellPar44 = row13.createCell(7);
        BigDecimal qqsQisqa = totalSumQisqa.divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(12)).setScale(2, RoundingMode.HALF_UP);
        cellPar44.setCellValue(getNumber(qqsQisqa));
        cellPar44.setCellStyle(cellStyleBold);

        XSSFCell cellPar45 = row13.createCell(8);
        BigDecimal totalQisqa = qqsQisqa.add(totalSumQisqa);


        cellPar45.setCellValue(getNumber(totalQisqa));
        cellPar45.setCellStyle(cellStyleBold);

        XSSFRow row14 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, 0));

        XSSFCell cellPar46 = row14.createCell(0);
        cellPar46.setCellStyle(cellStyleBold);

        XSSFCell cellPar47 = row14.createCell(1);
        cellPar47.setCellValue("Jami: ");
        cellPar47.setCellStyle(cellStyleBold);

        XSSFCell cellPar48 = row14.createCell(2);
        cellPar48.setCellStyle(cellStyleBold);

        XSSFCell cellPar49 = row14.createCell(3);
        cellPar49.setCellStyle(cellStyleBold);


        XSSFCell cellPar50 = row14.createCell(4);
        cellPar50.setCellValue(getNumber(totalSumQisqa.add(totalSum3_4)));
        cellPar50.setCellStyle(cellStyleBold);

        XSSFCell cellPar51 = row14.createCell(5);
        cellPar51.setCellValue(getNumber(totalSumQisqa.add(totalSum3_4)));
        cellPar51.setCellStyle(cellStyleBold);

        XSSFCell cellPar52 = row14.createCell(6);
        cellPar52.setCellStyle(cellStyleBold);

        XSSFCell cellPar53 = row14.createCell(7);
        cellPar53.setCellValue(getNumber(qqs3_4.add(qqsQisqa)));
        cellPar53.setCellStyle(cellStyleBold);

        XSSFCell cellPar54 = row14.createCell(8);
        cellPar54.setCellValue(getNumber(total3_4.add(totalQisqa)));
        cellPar54.setCellStyle(cellStyleBold);


        XSSFRow row15 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(++firstRow, firstRow++, 0, 0));

        XSSFCell cellPar55 = row15.createCell(0);
        cellPar55.setCellStyle(cellStyleBold);

        XSSFCell cellPar56 = row15.createCell(1);
        cellPar56.setCellValue("Qish mavsumiga tayyorlangan zaxiradan sarflangan maxsulotlar shartnomaga asosan");
        cellPar56.setCellStyle(cellStyleBold);

        XSSFCell cellPar57 = row15.createCell(2);
        cellPar57.setCellStyle(cellStyleBold);

        XSSFCell cellPar58 = row15.createCell(3);
        cellPar58.setCellStyle(cellStyleBold);


        XSSFCell cellPar59 = row15.createCell(4);
        cellPar59.setCellStyle(cellStyleBold);

        XSSFCell cellPar60 = row15.createCell(5);
        cellPar60.setCellStyle(cellStyleBold);

        XSSFCell cellPar61 = row15.createCell(6);
        cellPar61.setCellStyle(cellStyleBold);

        XSSFCell cellPar62 = row15.createCell(7);
        cellPar62.setCellStyle(cellStyleBold);

        XSSFCell cellPar63 = row15.createCell(8);
        cellPar63.setCellValue(getNumber(totalSumProduct));
        cellPar63.setCellStyle(cellStyleBold);


        XSSFRow row16 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(++firstRow, firstRow++, 0, 0));

        XSSFCell cellPar64 = row16.createCell(0);
        cellPar64.setCellStyle(cellStyleBold);

        XSSFCell cellPar65 = row16.createCell(1);
        cellPar65.setCellValue("To'lanadigan jami summa");
        cellPar65.setCellStyle(cellStyleBold);

        XSSFCell cellPar66 = row16.createCell(2);
        cellPar66.setCellStyle(cellStyleBold);

        XSSFCell cellPar67 = row16.createCell(3);
        cellPar67.setCellStyle(cellStyleBold);


        XSSFCell cellPar68 = row16.createCell(4);
        cellPar68.setCellStyle(cellStyleBold);

        XSSFCell cellPar69 = row16.createCell(5);
        cellPar69.setCellStyle(cellStyleBold);

        XSSFCell cellPar70 = row16.createCell(6);
        cellPar70.setCellStyle(cellStyleBold);

        XSSFCell cellPar71 = row16.createCell(7);
        cellPar71.setCellStyle(cellStyleBold);

        XSSFCell cellPar72 = row16.createCell(8);
        cellPar72.setCellValue(getNumber(total3_4.add(totalQisqa).subtract(totalSumProduct)));
        cellPar72.setCellStyle(cellStyleBold);


        int rowNumber = rowNum + 1;

        XSSFRow row17 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
        XSSFCell cell7 = row17.createCell(1);
//        cell7.setCellValue("Autsorser");
        cell7.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 6, 8));
        XSSFCell cell8 = row17.createCell(6);
        cell8.setCellValue("Qabul qildi");
        cell8.setCellStyle(cellStyleBoldNoBorder);


        rowNumber += 1;

        XSSFRow row18 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
        XSSFCell cell9 = row18.createCell(1);
        cell9.setCellValue("Direktor__________________" + department.getDirektor());
        cell9.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 6, 8));
        XSSFCell cell10 = row18.createCell(6);
        cell10.setCellValue(district.getName() + " MTB");
        cell10.setCellStyle(cellStyleBoldNoBorder);


        rowNumber += 2;

        XSSFRow row19 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
        XSSFCell cell11 = row19.createCell(1);
        cell11.setCellValue("Bosh hisobchi_______________" + department.getXisobchi());
        cell11.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber++, 6, 8));
        XSSFCell cell12 = row19.createCell(6);
        cell12.setCellValue("______________________________________________________");
        cell12.setCellStyle(cellStyleBoldNoBorder);

        XSSFRow row20 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 2));
        XSSFCell cell13 = row20.createCell(1);
        cell13.setCellValue("Maxsulot chiqardii__________________");
        cell13.setCellStyle(cellStyleBoldNoBorder);


        dalolatnoma(format, workbook, font, fontParagraph, department, district, start, end, total3_4, totalQisqa);
    }


    public void dalolatnoma(short format, XSSFWorkbook workbook, XSSFFont font, XSSFFont fontParagraph, Department department, District district, LocalDate start, LocalDate end, BigDecimal totalSum3_4, BigDecimal totalSumQisqa) {


//        fontParagraph.setBold(false);

        XSSFFont fontBold = workbook.createFont();
        fontBold.setFontName("Times New Roman");
        fontBold.setFontHeight(12);
//        fontBold.setBold(true);


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        style(font, cellStyle);
        cellStyle.setRotation((short) 0);

        XSSFCellStyle cellStyleBoldNoBorder = workbook.createCellStyle();
        styleNoBorder(fontBold, cellStyleBoldNoBorder);
        cellStyleBoldNoBorder.setRotation((short) 0);

        XSSFCellStyle cellStyleBold = workbook.createCellStyle();
        style(fontBold, cellStyleBold);
        cellStyleBold.setRotation((short) 0);


        XSSFCellStyle cellStyleBoldMaxsus = workbook.createCellStyle();
        styleMaxsus(fontBold, cellStyleBoldMaxsus);
        cellStyleBoldMaxsus.setRotation((short) 0);
        cellStyleBoldMaxsus.setDataFormat(format);


        XSSFCellStyle paragraphStyle = workbook.createCellStyle();
        fontParagraph.setFontHeight(12);
        paragraph(fontParagraph, paragraphStyle);
        paragraphStyle.setRotation((short) 0);

        XSSFCellStyle paragraphStyleLeft = workbook.createCellStyle();
        fontParagraph.setFontHeight(12);
        paragraphLeft(fontParagraph, paragraphStyleLeft);
        paragraphStyleLeft.setRotation((short) 0);

        XSSFCellStyle paragraphStyleRight = workbook.createCellStyle();
        fontParagraph.setFontHeight(12);
        paragraphRight(fontParagraph, paragraphStyleRight);
        paragraphStyleRight.setRotation((short) 0);

        XSSFCellStyle paragraphStyleFactura = workbook.createCellStyle();
        paragraphFactura(fontParagraph, paragraphStyleFactura);
        paragraphStyle.setRotation((short) 0);


        XSSFSheet spreadsheet = workbook.createSheet("Dalolatnoma");


        spreadsheet.setColumnWidth(0, 1500);
        spreadsheet.setColumnWidth(1, 10000);
        spreadsheet.setColumnWidth(2, 2500);
        spreadsheet.setColumnWidth(3, 5000);
        spreadsheet.setColumnWidth(4, 5000);
        spreadsheet.setColumnWidth(5, 1500);


        int rowNum = 0;
        int firstRow = 0;
        int old = 5;


        XSSFRow row = spreadsheet.createRow(rowNum++);
//        row.setHeight((short) 500);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, old));

        XSSFCell cellPar = row.createCell(0);
        cellPar.setCellValue("Bajarilgan ishlar");
        cellPar.setCellStyle(paragraphStyle);

        XSSFRow row1 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, old));

        XSSFCell cellPar1 = row1.createCell(0);
        cellPar1.setCellValue("DALOLATNOMASI №_________");
        cellPar1.setCellStyle(paragraphStyle);

        rowNum += 1;
        firstRow += 1;

        XSSFRow row2 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 0, 1));

        XSSFCell cellPar2 = row2.createCell(0);
        cellPar2.setCellValue(district.getName());
        cellPar2.setCellStyle(paragraphStyleLeft);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 4, old));

        XSSFCell cellPar3 = row2.createCell(4);
        cellPar3.setCellValue(LocalDate.now().toString());
        cellPar3.setCellStyle(paragraphStyleRight);

        rowNum += 1;
        firstRow += 1;


        XSSFRow row3 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, old));

        XSSFCell cellPar4 = row3.createCell(0);
        cellPar4.setCellValue("\"_____\"_______ 202__ yilgadi  №_______   shartnoma ");
        cellPar4.setCellStyle(paragraphStyle);

        rowNum++;
        firstRow++;


        XSSFRow row4 = spreadsheet.createRow(rowNum++);
        row4.setHeight((short) 1300);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, old));
        XSSFCell cellPar6 = row4.createCell(0);
        cellPar6.setCellValue("BIzlar kim quyida imzo chekuvchi \"" + department.getName() + "\" direktori " + department.getDirektor() + "bir tomondan, " + district.getName() + " MTB (istemolchi) direktori___________________________ ikkinchi tomondan  mazkur dalolatnomani \n____ _________ 202___ yildagi №_______ sonli shartnoma asosida" + " quyidagi miqdorda ish bajarilganligi xaqida tuzdik");
        cellPar6.setCellStyle(paragraphStyle);


        rowNum++;


        XSSFRow row10 = spreadsheet.createRow(rowNum++);
        row10.setHeight((short) 700);
        spreadsheet.addMergedRegion(new CellRangeAddress(++firstRow, firstRow, 0, 0));

        XSSFCell cellPar18 = row10.createCell(0);
        cellPar18.setCellValue("T/r");
        cellPar18.setCellStyle(cellStyleBold);


        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 3));
        XSSFCell cellPar19 = row10.createCell(1);
        cellPar19.setCellValue("Ish yoki xizmat nomi");
        cellPar19.setCellStyle(cellStyleBold);


        XSSFCell cellPar191 = row10.createCell(2);
        cellPar191.setCellStyle(cellStyleBold);
        XSSFCell cellPar192 = row10.createCell(3);
        cellPar192.setCellStyle(cellStyleBold);


        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 4, old));
        XSSFCell cellPar20 = row10.createCell(4);
        cellPar20.setCellValue("Bajarilgan ishlar miqdori (QQS bilan)");
        cellPar20.setCellStyle(cellStyleBold);

        XSSFCell cellPar193 = row10.createCell(old);
        cellPar193.setCellStyle(cellStyleBold);


        XSSFRow row5 = spreadsheet.createRow(rowNum++);
        row5.setHeight((short) 1200);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 0, 0));

        XSSFCell cell = row5.createCell(0);
        cell.setCellValue("1");
        cell.setCellStyle(cellStyle);


        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 3));
        XSSFCell cell1 = row5.createCell(1);
        cell1.setCellValue(district.getName() + " DMTT 3-4 yosh  va 4-7 yoshli gurux tarbialanuvchiari uchun " + start.getYear() + "-yil " +
                start.getDayOfMonth() + "-" + end.getDayOfMonth() + "-" + getMonthName(start.getMonthValue()) + " oyida autsoursing asosida kuniga to'rt mahal ovqatlanishni tashkil etish bo'yicha.");
        cell1.setCellStyle(cellStyleBoldMaxsus);

        XSSFCell cell13 = row5.createCell(2);
        cell13.setCellStyle(cellStyle);

        XSSFCell cell14 = row5.createCell(3);
        cell14.setCellStyle(cellStyle);


        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 4, old));
        XSSFCell cell2 = row5.createCell(4);
        cell2.setCellValue(getNumber(totalSum3_4));
        cell2.setCellStyle(cellStyle);


        XSSFCell cell15 = row5.createCell(old);
        cell15.setCellStyle(cellStyle);


        XSSFRow row6 = spreadsheet.createRow(rowNum++);
        row6.setHeight((short) 1200);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 0, 0));

        XSSFCell cell3 = row6.createCell(0);
        cell3.setCellValue("2");
        cell3.setCellStyle(cellStyle);


        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 1, 3));
        XSSFCell cell4 = row6.createCell(1);
        cell4.setCellValue(district.getName() + " DMTT qisqa muddatli gurux tarbialanuvchiari uchun " + start.getYear() + "-yil " +
                start.getDayOfMonth() + "-" + end.getDayOfMonth() + "-" + getMonthName(start.getMonthValue()) + " oyida autsoursing asosida kuniga to'rt mahal ovqatlanishni tashkil etish bo'yicha.");
        cell4.setCellStyle(cellStyleBoldMaxsus);

        XSSFCell cell16 = row6.createCell(2);
        cell16.setCellStyle(cellStyle);

        XSSFCell cell17 = row6.createCell(3);
        cell17.setCellStyle(cellStyle);


        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 4, old));
        XSSFCell cell5 = row6.createCell(4);
        cell5.setCellValue(getNumber(totalSumQisqa));
        cell5.setCellStyle(cellStyle);


        XSSFCell cell18 = row6.createCell(old);
        cell18.setCellStyle(cellStyle);


        XSSFRow row7 = spreadsheet.createRow(rowNum++);

        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 0, 3));
        XSSFCell cell6 = row7.createCell(0);
        cell6.setCellValue("Jami:");
        cell6.setCellStyle(cellStyleBold);

        XSSFCell cell19 = row7.createCell(1);
        cell19.setCellStyle(cellStyleBold);
        XSSFCell cell20 = row7.createCell(2);
        cell20.setCellStyle(cellStyleBold);
        XSSFCell cell21 = row7.createCell(3);
        cell21.setCellStyle(cellStyleBold);


        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 4, old));
        XSSFCell cell61 = row7.createCell(4);
        cell61.setCellValue(getNumber(totalSumQisqa.add(totalSum3_4)));
        cell61.setCellStyle(cellStyleBold);

        XSSFCell cell22 = row7.createCell(old);
        cell22.setCellStyle(cellStyleBold);


        int rowNumber = rowNum + 2;

        XSSFRow row17 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 1));
        XSSFCell cell7 = row17.createCell(1);
        cell7.setCellValue("Autsorser");
        cell7.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 3, 4));
        XSSFCell cell8 = row17.createCell(3);
        cell8.setCellValue("Istemolchi");
        cell8.setCellStyle(cellStyleBoldNoBorder);


        rowNumber++;

        XSSFRow row18 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 1));
        XSSFCell cell9 = row18.createCell(1);
        cell9.setCellValue(department.getName());
        cell9.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 3, 4));
        XSSFCell cell10 = row18.createCell(3);
        cell10.setCellValue(district.getName() + " MTB");
        cell10.setCellStyle(cellStyleBoldNoBorder);


        rowNumber++;

        XSSFRow row19 = spreadsheet.createRow(rowNumber);
        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 1, 1));
        XSSFCell cell11 = row19.createCell(1);
        cell11.setCellValue("___________________________________");
        cell11.setCellStyle(cellStyleBoldNoBorder);


        spreadsheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 3, 4));
        XSSFCell cell12 = row19.createCell(3);
        cell12.setCellValue("___________________________________");
        cell12.setCellStyle(cellStyleBoldNoBorder);
    }


    public String getMonthName(int month) {

        String[] monthNames = {"", "Yanvar", "Fevral", "Mart", "Aprel", "May", "Iyun", "Iyul", "Avgust", "Sentabr", "Oktabr", "Noyabr", "Dekabr"};
        return monthNames[month];
    }
}
