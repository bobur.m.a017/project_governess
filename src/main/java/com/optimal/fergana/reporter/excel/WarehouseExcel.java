package com.optimal.fergana.reporter.excel;

import com.optimal.fergana.department.Department;
import com.optimal.fergana.product.Product;
import com.optimal.fergana.warehouse.Warehouse;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static com.optimal.fergana.statics.StaticMethods.paragraph;
import static com.optimal.fergana.statics.StaticMethods.style;
import static com.optimal.fergana.statics.StaticWords.DEST;


@Service
@RequiredArgsConstructor
public class WarehouseExcel {

    public String getFile(Department department) throws IOException {



        String name = department.getName() + "Qoldiq" + ".xlsx";
        String path = DEST + name;


        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        int fontHeight = 12;
        font.setFontHeight(fontHeight);


        XSSFFont fontParagraph = workbook.createFont();
        fontParagraph.setFontName("Times New Roman");
        fontParagraph.setFontHeight(fontHeight);
        fontParagraph.setBold(true);

        DataFormat format = workbook.createDataFormat();

        short shortFormatWeight = format.getFormat("0.00");

        ombor(shortFormatWeight, workbook, font, fontParagraph, department);

        FileOutputStream out = new FileOutputStream(path);
        workbook.write(out);
        out.close();

        return name;
    }

    public void ombor(short format, XSSFWorkbook workbook, XSSFFont font, XSSFFont fontParagraph, Department department) {


        List<Warehouse> list = department.getWarehouseList();

//        fontParagraph.setBold(false);

        XSSFFont fontBold = workbook.createFont();
        fontBold.setFontName("Times New Roman");
        fontBold.setFontHeight(12);
//        fontBold.setBold(true);


        XSSFCellStyle cellStyle = workbook.createCellStyle();
        style(font, cellStyle);
        cellStyle.setRotation((short) 0);
        cellStyle.setDataFormat(format);

        XSSFCellStyle cellStyleBold = workbook.createCellStyle();
        style(fontBold, cellStyleBold);
        cellStyleBold.setRotation((short) 0);

        XSSFCellStyle paragraphStyle = workbook.createCellStyle();
        fontParagraph.setFontHeight(12);
        paragraph(fontParagraph, paragraphStyle);
        paragraphStyle.setRotation((short) 0);


        XSSFSheet spreadsheet = workbook.createSheet("Ombor");


        spreadsheet.setColumnWidth(0, 2000);
        spreadsheet.setColumnWidth(1, 9000);
        spreadsheet.setColumnWidth(2, 3000);
        spreadsheet.setColumnWidth(3, 6000);


        int rowNum = 0;
        int firstRow = 0;
        int old = 3;


        XSSFRow row = spreadsheet.createRow(rowNum++);
        row.setHeight((short) 700);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow++, 0, old));

        XSSFCell cellPar = row.createCell(0);
        cellPar.setCellValue(department.getName() + "ning omborida mavjud maxsulotlar to'g'risida ma'lumot \n" + LocalDate.now() + " kun xolatiga ");
        cellPar.setCellStyle(paragraphStyle);


        rowNum += 1;
        firstRow += 1;


        XSSFRow row10 = spreadsheet.createRow(rowNum++);
        spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 0, 0));

        XSSFCell cellPar18 = row10.createCell(0);
        cellPar18.setCellValue("T/r");
        cellPar18.setCellStyle(cellStyleBold);


        XSSFCell cellPar19 = row10.createCell(1);
        cellPar19.setCellValue("Maxsulot noomi");
        cellPar19.setCellStyle(cellStyleBold);


        XSSFCell cellPar20 = row10.createCell(2);
        cellPar20.setCellValue("O'lchov birligi (kg/dona)");
        cellPar20.setCellStyle(cellStyleBold);

        XSSFCell cellPar21 = row10.createCell(3);
        cellPar21.setCellValue("Miqdori");
        cellPar21.setCellStyle(cellStyleBold);


        int num = 1;

        for (Warehouse warehouse : list) {
            if (warehouse.getTotalPackWeight().compareTo(BigDecimal.ZERO) > 0) {

                Product product = warehouse.getProduct();

                XSSFRow row1 = spreadsheet.createRow(rowNum++);
                spreadsheet.addMergedRegion(new CellRangeAddress(firstRow, firstRow, 0, 0));

                XSSFCell cell = row1.createCell(0);
                cell.setCellValue(num++);
                cell.setCellStyle(cellStyleBold);

                XSSFCell cell1 = row1.createCell(1);
                cell1.setCellValue(product.getName());
                cell1.setCellStyle(cellStyle);

                XSSFCell cell2 = row1.createCell(2);

                if (warehouse.getInOutPriceList().size() > 0) {

                    BigDecimal pack = warehouse.getInOutPriceList().get(0).getPack();

                    if (pack != null) {
                        cell2.setCellValue(pack.compareTo(BigDecimal.ZERO) > 0 ? "dona" : "kg");
                    } else {
                        cell2.setCellValue("kg");
                    }
                } else {
                    cell2.setCellValue("kg");
                }
                cell2.setCellStyle(cellStyle);

                XSSFCell cell3 = row1.createCell(3);
                cell3.setCellValue(getNumber(warehouse.getTotalPackWeight()));
                cell3.setCellStyle(cellStyle);
            }
        }
    }

    public double getNumber(BigDecimal num) {

//        return num.toString().replace(".", ",");
        return num.doubleValue();
    }
}
