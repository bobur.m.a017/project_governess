package com.optimal.fergana.reporter.excel.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DateDTO {

    private LocalDate date;
    private BigDecimal weight = BigDecimal.ZERO;
    private Integer number = 0;
    private boolean numberState = false;

    public DateDTO(LocalDate date) {
        this.date = date;
    }

    public DateDTO(LocalDate date, BigDecimal weight) {
        this.date = date;
        this.weight = weight;
    }
}
