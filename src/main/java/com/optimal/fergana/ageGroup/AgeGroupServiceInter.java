package com.optimal.fergana.ageGroup;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.Users;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;

public interface AgeGroupServiceInter {
    StateMessage add(AgeGroupDTO dto, Users users);
    ResponseEntity<?> getOne(Integer id);
    ResponseEntity<?> getAll(Users users);
    StateMessage edit(Integer id, AgeGroupDTO dto, Users users);
    StateMessage delete(Integer id, Users user) throws ChangeSetPersister.NotFoundException;
}
