package com.optimal.fergana.ageGroup;


import com.optimal.fergana.menu.ageStandard.AgeStandard;
import com.optimal.fergana.stayTime.StayTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgeGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Boolean delete = false;
    private Boolean report = false;
    @CreationTimestamp
    private Timestamp createDate;

    private Integer sortNumber = 99;

    @UpdateTimestamp
    private Timestamp updateDate;

    @ManyToOne
    private StayTime stayTime;

    public AgeGroup(String name, Boolean delete) {
        this.name = name;
        this.delete = delete;
    }
}
