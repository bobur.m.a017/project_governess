package com.optimal.fergana.controller;


import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.WarehouseService;
import com.optimal.fergana.warehouse.delivery.DeliverySubDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/warehouse")
public class WarehouseController {

    private final UserService userService;
    private final WarehouseService warehouseService;


    @PreAuthorize("hasAnyRole('ADMIN','OMBOR_MUDIRI')")
    @PostMapping("/{supplierId}")
    public ResponseEntity<?> add(@PathVariable Integer supplierId, @RequestParam("productId") Integer productId, HttpServletRequest request, @RequestParam("weight") Double weight, @RequestParam("price") Double price) {
        try {
            Users users = userService.parseToken(request);
            StateMessage stateMessage = warehouseService.add(productId, BigDecimal.valueOf(weight), BigDecimal.valueOf(price), users, supplierId);
            return ResponseEntity.status(200).body(stateMessage);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','RAXBAR','BOSHQARMA_BUXGALTER','BO`LIM_BUXGALTER','TEXNOLOG','HAMSHIRA','OMBOR_MUDIRI')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("departmentId") Integer departmentId, HttpServletRequest request, @Param("pageSize") Integer pageSize, @Param("pageNumber") Integer pageNumber) {
        try {
            Users users = userService.parseToken(request);

            CustomPageable customPageable = warehouseService.getAll(users, departmentId, pageSize, pageNumber);
            return ResponseEntity.status(200).body(customPageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','OMBORCHI','RAXBAR','BOSHQARMA_BUXGALTER','BO`L_IMBUXGALTER','TEXNOLOG','HAMSHIRA','OMBOR_MUDIRI')")
    @GetMapping("/inout")
    public ResponseEntity<?> getAll(@Param("departmentId") Integer departmentId, @Param("startDate") Long startDate, @Param("endDate") Long endDate, @Param("id") String id, HttpServletRequest request, @Param("pageSize") Integer pageSize, @Param("pageNumber") Integer pageNumber) {
        try {
            CustomPageable customPageable = warehouseService.getAll(request, startDate, endDate, id, departmentId, pageSize, pageNumber);
            return ResponseEntity.status(200).body(customPageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BO`LIM_BUXGALTER','BUXGALTER','BOSHQARMA_BUXGALTER','OMBOR_MUDIRI')")
    @GetMapping("/getFile")
    public ResponseEntity<?> getFile( HttpServletRequest request) {

        try {
            Users users = userService.parseToken(request);
            String path = warehouseService.getFile(users);


            if (path != null) {
                String str = StaticWords.URI + path.substring(StaticWords.DEST.length());
                return ResponseEntity.status(200).body(str);
            }
            return ResponseEntity.status(417).body("MA'LUMOT MAVJUD EMAS");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(e.hashCode()).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','TEXNOLOG','BO`LIM_BUXGALTER','BUXGALTER','BOSHQARMA_BUXGALTER','OMBOR_MUDIRI')")
    @GetMapping("/getFileEcxel")
    public ResponseEntity<?> getFileEcxel( HttpServletRequest request) {

        try {


            Users users = userService.parseToken(request);
            String path = warehouseService.getFileExcel(users);

            if (path != null) {
                String str = StaticWords.URI + path;
                return ResponseEntity.status(200).body(str);
            }
            return ResponseEntity.status(417).body("MA'LUMOT MAVJUD EMAS");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(e.hashCode()).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBOR_MUDIRI')")
    @PostMapping("accept/{deliveryId}")
    public ResponseEntity<?> add(HttpServletRequest request, @RequestBody DeliverySubDTO deliverySubDTO, @PathVariable UUID deliveryId) {
        try {
            StateMessage message = warehouseService.acceptedProducts(request, deliverySubDTO, deliveryId);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
