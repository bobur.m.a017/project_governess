package com.optimal.fergana.controller;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.delivery.DeliverySubDTO;
import com.optimal.fergana.warehouse.kindergarten.KindergartenWarehouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/kindergartenWarehouse")
public class KindergartenWarehouseController {


    private final KindergartenWarehouseService kindergartenWarehouseService;
    private final UserService userService;


    @PreAuthorize("hasAnyRole('OMBORCHI')")
    @PostMapping
    public ResponseEntity<?> add(HttpServletRequest request, @RequestBody DeliverySubDTO deliverySubDTO) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = kindergartenWarehouseService.acceptedProducts(deliverySubDTO, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBORCHI','ADMIN','BOSHQARMA_BUXGALTER','BO`LIM_BUXGALTER')")
    @GetMapping()
    public ResponseEntity<?> getAll(HttpServletRequest request, @Param("kindergartenId") Integer kindergartenId, @Param("pageSize") Integer pageSize, @Param("pageNumber") Integer pageNumber) {
        try {
            Users users = userService.parseToken(request);
            CustomPageable customPageable = kindergartenWarehouseService.getAll(kindergartenId, pageSize, pageNumber, users);
            return ResponseEntity.ok(customPageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBORCHI')")
    @PostMapping("/{productId}")
    public ResponseEntity<?> addOutput(@PathVariable Integer productId, @RequestParam("weight") Double weight, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);

            StateMessage message = kindergartenWarehouseService.addOutPut(productId, BigDecimal.valueOf(weight), users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBORCHI')")
    @PostMapping("/add/{productId}")
    public ResponseEntity<?> add(@PathVariable Integer productId, @RequestParam("weight") Double weight, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);

            StateMessage message = kindergartenWarehouseService.add(productId, BigDecimal.valueOf(weight), users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBORCHI','BOSHQARMA_BUXGALTER','BO`LIM_BUXGALTER','BUXGALTER','OMBOR_MUDIR','TEXNOLO')")
    @GetMapping("/getFile")
    public ResponseEntity<?> getFile(@Param("kindergartenId") Integer kindergartenId, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);

            StateMessage message = kindergartenWarehouseService.getFile(users, kindergartenId);

            if (message.isSuccess()) {

                return ResponseEntity.status(message.getCode()).body(message.getText());
            }
            return ResponseEntity.status(message.getCode()).body(message);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


}
