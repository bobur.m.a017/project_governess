package com.optimal.fergana.controller;

import com.optimal.fergana.calendar.CalendarService;
import com.optimal.fergana.calendar.Month;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDate;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/")
public class CalendarController {

    private final UserService userService;
    private final CalendarService calendarService;

    @PreAuthorize("hasAnyRole('ADMIN','BOG`CHA_MUDIRASI','XODIMLAR_BO`LIMI','TEXNOLOG','BO`LIM_BUXGALTER','HAMSHIRA')")
    @GetMapping("/byCalendar")
    public ResponseEntity<?> getCurrentUser(@Param("month") Integer month, @Param("year") Integer year, HttpServletRequest request) {
        try {
            Users user = userService.parseToken(request);
            Month month1 = calendarService.getCalendarAddMenu(year, month, user);
            return ResponseEntity.status(HttpStatus.OK).body(month1);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BOG`CHA_MUDIRASI','XODIMLAR_BO`LIMI','TEXNOLOG','BO`LIM_BUXGALTER','HAMSHIRA')")
    @GetMapping("/byCalendarKindergarten/{id}")
    public ResponseEntity<?> byCalendarKindergarten(@PathVariable Integer id, @Param("month") Integer month, @Param("year") Integer year, HttpServletRequest request) {
        try {
            Users user = userService.parseToken(request);
            LocalDate date;

            if (year == null || month == null) {
                date = LocalDate.now();
            }else {
                date = LocalDate.of(year, month, 6);
            }

            Month month1 = calendarService.byCalendarKindergarten(id, date, user);
            return ResponseEntity.status(HttpStatus.OK).body(month1);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
