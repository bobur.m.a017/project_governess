package com.optimal.fergana.controller;


import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentDTO;
import com.optimal.fergana.regionalDepartment.RegionalDepartmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/out/api/regionDepartment")
public class RegionalDepartmentController {


    private final RegionalDepartmentService regionalDepartmentService;

    public RegionalDepartmentController(RegionalDepartmentService regionalDepartmentService) {
        this.regionalDepartmentService = regionalDepartmentService;
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody RegionalDepartmentDTO dto) {
        try {
            StateMessage message = regionalDepartmentService.add(dto);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody RegionalDepartmentDTO dto) {
        try {
            StateMessage message = regionalDepartmentService.edit(dto, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = regionalDepartmentService.delete(id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            return regionalDepartmentService.getOne(id);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @GetMapping
    public ResponseEntity<?> getAll() {
        try {
            return regionalDepartmentService.getAll();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
