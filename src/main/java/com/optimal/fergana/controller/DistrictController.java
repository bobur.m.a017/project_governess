package com.optimal.fergana.controller;


import com.optimal.fergana.address.district.DistrictDTO;
import com.optimal.fergana.address.district.DistrictService;
import com.optimal.fergana.department.DepartmentDTO;
import com.optimal.fergana.department.DepartmentService;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/district")
public class DistrictController {


    private final DistrictService districtService;
    private final UserService userService;


    @PreAuthorize("hasAnyRole('ADMIN','HAMSHIRA','RAXBAR','OMBORCHI','BO`LIM_BUXGALTER','XODIMLAR_BO`LIMI','BOSHQARMA_BUXGALTER','TEXNOLOG')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<DistrictDTO> list = districtService.getAll(users);
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
