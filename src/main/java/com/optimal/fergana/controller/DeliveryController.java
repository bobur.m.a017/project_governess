package com.optimal.fergana.controller;

import com.optimal.fergana.message.ResponseMessage;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.status.Status;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.warehouse.delivery.Delivery;
import com.optimal.fergana.warehouse.delivery.DeliveryDTO;
import com.optimal.fergana.warehouse.delivery.DeliveryResDTO;
import com.optimal.fergana.warehouse.delivery.DeliveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/delivery")
public class DeliveryController {
    private final DeliveryService deliveryService;

    @PreAuthorize("hasAnyRole('OMBOR_MUDIRI')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody DeliveryDTO deliveryDTO, HttpServletRequest request) {
        try {
            ResponseMessage message = deliveryService.add(deliveryDTO, request);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('OMBORCHI','OMBOR_MUDIRI')")
    @GetMapping("/statusNew")
    public ResponseEntity<?> getAllNew(HttpServletRequest request) {
        try {
            List<DeliveryResDTO> deliveries = deliveryService.getDeliveriesNEW(Status.SEND.getName(), request);
            return ResponseEntity.ok(deliveries);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('OMBORCHI')")
    @GetMapping("/statusNotNew")
    public ResponseEntity<?> getAllNOTNew(HttpServletRequest request, @Param("pageSize") Integer pageSize, @Param("pageNumber") Integer pageNumber) {
        try {
            CustomPageable deliveriesNOTNEW = deliveryService.getDeliveriesNOTNEW(Status.SEND.getName(), request, pageSize, pageNumber);
            return ResponseEntity.ok(deliveriesNOTNEW);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('OMBOR_MUDIRI','BO`LIM_BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAllByDate(HttpServletRequest request, @Param("status") String status, @Param("pageSize") Integer pageSize, @Param("pageNumber") Integer pageNumber, @Param("MTTNumber") Integer MTTNumber, @Param("startDate") Long startDate, @Param("endDate") Long endDate) {
        try {
            CustomPageable deliveriesByDate = deliveryService.getDeliveriesByDate(status, request, MTTNumber, startDate, endDate, pageSize, pageNumber);
            return ResponseEntity.ok(deliveriesByDate);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBOR_MUDIRI')")
    @GetMapping("/getFile/{id}")
    public String getAllByDate( @PathVariable UUID id ) {

        try {
            String path = deliveryService.getFile(id);

            return StaticWords.URI + path.substring(StaticWords.DEST.length());

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}