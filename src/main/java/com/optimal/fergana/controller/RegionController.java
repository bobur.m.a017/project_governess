package com.optimal.fergana.controller;


import com.optimal.fergana.address.region.RegionDTO;
import com.optimal.fergana.address.region.RegionService;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/out/api/region")
public class RegionController {

    private final RegionService regionService;
    private final UserService userService;

    public RegionController(RegionService regionService, UserService userService) {
        this.regionService = regionService;
        this.userService = userService;
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            List<RegionDTO> list = regionService.getAll(users);
            list.sort(Comparator.comparing(RegionDTO::getName));
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
