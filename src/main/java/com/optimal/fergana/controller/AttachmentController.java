package com.optimal.fergana.controller;


import com.optimal.fergana.attachment.AttachmentService;
import com.optimal.fergana.statics.StaticWords;
import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;


@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/attachment")
public class AttachmentController {


    private final AttachmentService attachmentService;
    private final Path root = Paths.get("uploads");


    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable @NotNull Integer id) {
//        try {
        byte[] bytes = attachmentService.attachmentToBytes(id);
        return ResponseEntity.status(200).body(bytes);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
//        }
    }


    @GetMapping("/file1/{filename:.+}")
    public ResponseEntity<?> getFile1(@PathVariable String filename, HttpServletResponse response) {
        try {

//            /root/project_fergana/files/
//            File file = new File(StaticWords.DEST_PDF + name);
            Resource file = load(StaticWords.DEST + filename);

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"").body(file);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }



    @GetMapping("/file2")
    public ResponseEntity<?> getFile2(@RequestParam("name") String name, HttpServletResponse response) {
        try {

//            /root/project_fergana/files/
            File file = new File(StaticWords.DEST + name);

            FileInputStream fileInputStream = new FileInputStream(file);
            FileCopyUtils.copy(fileInputStream, response.getOutputStream());

            HttpHeaders headers = new HttpHeaders();
            headers.add("content-disposition", "inline;filename=" + name);
            ResponseEntity<byte[]> responsez = new ResponseEntity<byte[]>(
                    fileInputStream.readAllBytes(), headers, HttpStatus.OK);

            return responsez;

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @GetMapping("/file3")
    public ResponseEntity<?> getFile3(@RequestParam("name") String name, HttpServletResponse response) {
        try {

//            /root/project_fergana/files/
            File file = new File(StaticWords.DEST + name);

            FileInputStream fileInputStream = new FileInputStream(file);
            MediaType contentType = MediaType.APPLICATION_PDF;
//            FileCopyUtils.copy(fileInputStream, response.getOutputStream());


            ResponseEntity<File> body = ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;attachment; filename=ayuda.pdf")
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(file);
            return body;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/file4")
    public ResponseEntity<?> getFile4(@RequestParam("name") String name, HttpServletResponse response) {
        try {

//            /root/project_fergana/files/
            File file = new File(StaticWords.DEST + name);

            FileInputStream fileInputStream = new FileInputStream(file);
            MediaType contentType = MediaType.APPLICATION_PDF;
//            FileCopyUtils.copy(fileInputStream, response.getOutputStream());


            ResponseEntity<File> body = ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;attachment; filename=ayuda.pdf")
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(file);
            return body;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }



//    @RequestMapping(value = "/file5/{file_name}", method = RequestMethod.GET)
//    @ResponseBody
//    public FileSystemResource getFile(@PathVariable("file_name") String fileName) throws IOException, IOException {
//
//        Attachment attachment = attachmentRepository.findByFileOriginalName(fileName);
//        File file = new File(attachment.getPath());
//        Path path = Paths.get(file.getPath());
//        Files.readAllBytes(path);
//        return new FileSystemResource(file);
//    }

}
