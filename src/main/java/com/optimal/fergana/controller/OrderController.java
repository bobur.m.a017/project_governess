package com.optimal.fergana.controller;

import com.optimal.fergana.kindergarten.KindergartenService;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.order.MyOrderService;
import com.optimal.fergana.order.OrderDTO;
import com.optimal.fergana.order.OrderResDTO;
import com.optimal.fergana.order.toSendProduct.ProductWeightResDTO;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.reporter.excel.OrderEcxelReporter;
import com.optimal.fergana.reporter.pdf.OrderPDFReporter;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/order")
public class OrderController {

    private final MyOrderService myOrderService;
    private final OrderPDFReporter orderPDFReporter;
    private final UserService userService;
    private final KindergartenService kindergartenService;
    private final OrderEcxelReporter orderEcxelReporter;


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody @Valid OrderDTO dto, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);
            StateMessage message;

            if (dto.getStart() == null || dto.getEnd() == null) {
                message = new StateMessage("Null qiymat qabul qilinmaydi", false, 417);
            } else {
                message = myOrderService.add(dto, users);
            }
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@RequestBody @Valid OrderDTO orderDTO, @PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = myOrderService.edit(orderDTO, id, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = myOrderService.delete(id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','BUXGALTER')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            OrderResDTO orderResDTO = myOrderService.getOne(id);
            return ResponseEntity.status(HttpStatus.OK).body(orderResDTO);
        } catch (Exception e) {
            e.printStackTrace();

            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BOSHQARMA_BUXGALTER','BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("page") Integer page, @Param("pageSize") Integer pageSize, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            CustomPageable pageable = myOrderService.getAll(users, pageSize, page);
            return ResponseEntity.status(HttpStatus.OK).body(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER','BUXGALTER')")
    @PutMapping("/toRoundOff/{id}")
    public ResponseEntity<?> toRoundOff(@Param("weight") Double weight, @PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = myOrderService.toRoundOff(id, BigDecimal.valueOf(weight).divide(BigDecimal.valueOf(1000), 2, RoundingMode.HALF_UP));
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN','BO`LIM_BUXGALTER')")
    @PutMapping("/send/{id}")
    public ResponseEntity<?> send(@PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = myOrderService.send(id, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','BO`LIM_BUXGALTER','OMBOR_MUDIRI')")
    @GetMapping("/getProductByKindergarten/{kindergartenId}")
    public ResponseEntity<?> getByProduct(@PathVariable Integer kindergartenId) {
        try {
            List<ProductWeightResDTO> productByKindergarten = kindergartenService.getProductByKindergarten(kindergartenId);
            return ResponseEntity.status(200).body(productByKindergarten);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','BO`LIM_BUXGALTER','BUXGALTER')")
    @GetMapping("/reportPDFByOrder/{orderId}")
    public ResponseEntity<?> getReportByOrder(@PathVariable Integer orderId) {
        try {
            String path = orderPDFReporter.createPDFMenu("BuyurtmaJadvali", orderId);

            if (path == null){
                return ResponseEntity.status(417).body("MALUMOT MAVJUD EMAS");
            }

            String s = StaticWords.URI + path.substring(StaticWords.DEST.length());
            return ResponseEntity.status(200).body(s);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(e.hashCode()).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('BOSHQARMA_BUXGALTER','BO`LIM_BUXGALTER','BUXGALTER')")
    @GetMapping("/reportPDFByOrderExcel/{orderId}")
    public ResponseEntity<?> reportPDFByOrderExcel(@PathVariable Integer orderId) {
        try {
            String path = orderEcxelReporter.createExcel(orderId);

            if (path == null){
                return ResponseEntity.status(417).body("MALUMOT MAVJUD EMAS");
            }

            String s = StaticWords.URI + path.substring(StaticWords.DEST.length());
            return ResponseEntity.status(200).body(s);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(e.hashCode()).body(e.getMessage());
        }
    }
}