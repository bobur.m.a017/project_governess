package com.optimal.fergana.controller;


import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.product.acceptedProducts.AcceptedProductService;
import com.optimal.fergana.reporter.pdf.AcceptedProductPDFReporterBySupplier;
import com.optimal.fergana.statics.StaticWords;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/acceptedProduct")
public class AcceptedProductController {


    private final AcceptedProductService acceptedProductService;
    private final UserService userService;
    private final AcceptedProductPDFReporterBySupplier acceptedProductPDFReporterBySupplier;


    @PreAuthorize("hasAnyRole('ADMIN','OMBOR_MUDIRI','BO`LIM_BUXGALTER')")
    @GetMapping
    public ResponseEntity<?> getAll(@Param("page") Integer page, @Param("pageSize") Integer pageSize, HttpServletRequest request) {
        try {
            Users user = userService.parseToken(request);
            CustomPageable pageable = acceptedProductService.getAll(page, pageSize, user);
            return ResponseEntity.status(200).body(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','OMBOR_MUDIRI','BO`LIM_BUXGALTER')")
    @GetMapping("/byDate")
    public ResponseEntity<?> getAllByDate(@Param("productId") Integer productId, @Param("supplierId") Integer supplierId, @Param("departmentId") Integer departmentId, @Param("startDate") Long startDate, @Param("endDate") Long endDate, @Param("pageNumber") Integer pageNumber, @Param("pageSize") Integer pageSize, HttpServletRequest request) {
        try {
            CustomPageable pageable = acceptedProductService.getAllByDate(productId, supplierId, departmentId, startDate, endDate, pageNumber, pageSize, request);
            return ResponseEntity.status(200).body(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','OMBOR_MUDIRI','BO`LIM_BUXGALTER')")
    @GetMapping("pdf/byDate")
    public String getAllByDatePDF(@Param("productId") Integer productId, @Param("supplierId") Integer supplierId, @Param("departmentId") Integer departmentId, @Param("startDate") Long startDate, @Param("endDate") Long endDate, HttpServletRequest request) {
        try {

            String path = acceptedProductPDFReporterBySupplier.createPDFTable( productId, supplierId, departmentId, startDate, endDate, request);

            return StaticWords.URI + path.substring(StaticWords.DEST.length());
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }


}
