package com.optimal.fergana.controller;


import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.page.CustomPageable;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import com.optimal.fergana.waste.WasteService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.UUID;


@RequiredArgsConstructor
@RestController
@RequestMapping("/out/api/waste")
public class WasteController {


    private final UserService userService;
    private final WasteService wasteService;


    @PreAuthorize("hasAnyRole('OMBOR_MUDIRI')")
    @PostMapping
    public ResponseEntity<?> add(@RequestParam("inOutPriceId") String inOutPriceId, @RequestParam("weight") Double weight, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage stateMessage = wasteService.add(users, BigDecimal.valueOf(weight), UUID.fromString(inOutPriceId));
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBORCHI')")
    @PostMapping("/kindergarten/{productId}")
    public ResponseEntity<?> add(@PathVariable Integer productId, @RequestParam("weight") Double weight, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage stateMessage = wasteService.add(users, productId, BigDecimal.valueOf(weight));
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBORCHI','OMBOR_MUDIRI')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage stateMessage = wasteService.delete(users, id);
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('OMBORCHI','BO`LIM_BUXGALTER','OMBOR_MUDIRI')")
    @GetMapping()
    public ResponseEntity<?> getAll(@Param("pageNumber") Integer pageNumber, @Param("pageSize") Integer pageSize, @Param("districtId") Integer districtId, @Param("kindergartenId") Integer kindergartenId, @Param("kattaOmbor") Boolean kattaOmbor, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);

            if (kattaOmbor == null){
                kattaOmbor = false;
            }

            CustomPageable customPageable = wasteService.getAll(users, pageNumber, pageSize, districtId, kindergartenId,  kattaOmbor);

            return ResponseEntity.status(200).body(customPageable);

        } catch (Exception e) {

            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());

        }
    }


    @PreAuthorize("hasAnyRole('BO`LIM_BUXGALTER')")
    @PostMapping("/{id}")
    public ResponseEntity<?> verified(@PathVariable Integer id, @RequestParam("state") Boolean state, HttpServletRequest request) {
        try {

            Users users = userService.parseToken(request);
            StateMessage stateMessage = wasteService.verified(users, id, state);
            return ResponseEntity.status(stateMessage.getCode()).body(stateMessage);

        } catch (Exception e) {

            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());

        }
    }
}
