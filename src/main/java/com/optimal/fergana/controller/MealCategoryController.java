package com.optimal.fergana.controller;

import com.optimal.fergana.mealCategory.MealCategoryDTO;
import com.optimal.fergana.mealCategory.MealCategoryResDTO;
import com.optimal.fergana.mealCategory.MealCategoryService;
import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.users.UserService;
import com.optimal.fergana.users.Users;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/out/api/mealCategory")
public class MealCategoryController {

    private final MealCategoryService mealCategoryService;
    private final UserService userService;

    public MealCategoryController(MealCategoryService mealCategoryService, UserService userService) {
        this.mealCategoryService = mealCategoryService;
        this.userService = userService;
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @PostMapping
    public ResponseEntity<?> add(@RequestBody MealCategoryDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = mealCategoryService.add(dto, users);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @PutMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody MealCategoryDTO dto, HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
            StateMessage message = mealCategoryService.edit(dto, users, id);
            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Integer id) {
        try {
            return mealCategoryService.getOne(id);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            StateMessage message = mealCategoryService.delete(id);

            return ResponseEntity.status(message.getCode()).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','XODIMLAR_BO`LIMI','TEXNOLOG')")
    @GetMapping
    public ResponseEntity<?> get(HttpServletRequest request) {
        try {
            Users users = userService.parseToken(request);
           List<MealCategoryResDTO> list = mealCategoryService.getAll(users);

           list.sort(Comparator.comparing(MealCategoryResDTO::getName));
            return ResponseEntity.status(200).body(list);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
        }
    }
}
