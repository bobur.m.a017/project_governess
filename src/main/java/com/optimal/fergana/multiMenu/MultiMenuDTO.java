package com.optimal.fergana.multiMenu;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MultiMenuDTO {

    @NotNull
    private String name;

//    @Size(max = 31,min = 1)
    private Integer daily;

    @NotNull
    private List<Integer> mealTimeIdList;
}
