package com.optimal.fergana.multiMenu;

import com.optimal.fergana.message.StateMessage;
import com.optimal.fergana.product.ProductDTO;
import com.optimal.fergana.product.ProductResDTO;
import com.optimal.fergana.users.Users;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

public interface MultiMenuServiceInter {
    StateMessage add(Users users,MultiMenuDTO multiMenuDTO);
    StateMessage edit(UUID id, String name,Users users);
    ResponseEntity<?> getAll(Users users);
    ResponseEntity<?> getOne(UUID id,Users users);
    StateMessage delete(UUID id, Users users);
}
